import React, { Component } from 'react'
import {
  Text,
  Image,
  View,
  ActivityIndicator,
  StyleSheet
} from 'react-native'
import { connect } from 'react-redux'
import { Icon } from 'react-native-elements'
import { graphql, compose } from 'react-apollo'

import photoNotifications_QUERY from '../graphql/queries/getPhotoNotification.query'
import photoNotification_SUBSCRIPTION from '../graphql/subscriptions/photoNotification.subscription'
import styles from './componentStyle'

class PhotoIcon extends Component {
  render() {
    if (this.props.data.loading) {
      return <ActivityIndicator />
    }
    if (
      this.props.data.getPhotoNotifications.length === 0
    ) {
      return (
        <View style={styles.viewContainer}>
          <Icon
            name="photo-library"
            type="MaterialIcons"
            color={this.props.color}
          />
        </View>
      )
    }

    return (
      <View style={styles.viewContainer}>
        <Icon
          name="photo-library"
          type="MaterialIcons"
          color={this.props.color}
        />

        <View style={styles.badge}>
          <Text style={styles.text}>
            {this.props.data.getPhotoNotifications.length}
          </Text>
        </View>
      </View>
    )
  }
}

export default compose(
  connect(state => ({
    userId: state.user.userId
  })),
  graphql(photoNotifications_QUERY, {
    options: {
      pollInterval: 10000
    }
  })
)(PhotoIcon)
