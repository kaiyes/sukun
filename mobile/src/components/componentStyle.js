import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
  viewContainer: {
    zIndex: 0
  },
  badge: {
    position: 'absolute',
    left: Platform.OS === 'ios' ? 18 : 10,
    bottom: Platform.OS === 'ios' ? 18 : 10,
    height: 14,
    width: 14,
    borderRadius: 7,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 8,
    color: '#f0e3e4'
  }
})
