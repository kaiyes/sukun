import React, { Component } from 'react'
import {
  Text,
  Image,
  View,
  ActivityIndicator,
  StyleSheet
} from 'react-native'
import { connect } from 'react-redux'
import { Icon } from 'react-native-elements'
import { graphql, compose } from 'react-apollo'

import count_QUERY from '../graphql/queries/countOfunseenMatches.query'
import styles from './componentStyle'

class MatchIcon extends Component {
  render() {
    const { data } = this.props

    if (data.loading) {
      return <ActivityIndicator />
    }

    if (data.getUnseenMatches.length === 0) {
      return (
        <View style={styles.viewContainer}>
          <Icon
            name="heart"
            type="entypo"
            color={this.props.color}
          />
        </View>
      )
    }
    return (
      <View style={styles.viewContainer}>
        <Icon
          name="heart"
          type="entypo"
          color={this.props.color}
        />
        <View style={styles.badge}>
          <Text style={styles.text}>
            {data.getUnseenMatches.length}
          </Text>
        </View>
      </View>
    )
  }
}

export default compose(
  connect(state => ({
    userId: state.user.userId,
    gender: state.user.gender
  })),
  graphql(count_QUERY, {
    options: {
      pollInterval: 20000
    }
  })
)(MatchIcon)
