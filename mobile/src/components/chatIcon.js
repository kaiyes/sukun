import React, { Component } from 'react'
import {
  Text,
  Image,
  View,
  ActivityIndicator,
  StyleSheet
} from 'react-native'
import { connect } from 'react-redux'
import { Icon } from 'react-native-elements'
import { graphql, compose } from 'react-apollo'

import msgNoti_QUERY from '../graphql/queries/getMessageNotifications.query'
import msgNoti_subscription from '../graphql/subscriptions/msgNotification.subscription'
import styles from './componentStyle'

class ChatIcon extends Component {
  render() {
    if (this.props.data.loading) {
      return <ActivityIndicator />
    }
    if (
      this.props.data.getMessageNotifications.length === 0
    ) {
      return (
        <View style={styles.viewContainer}>
          <Icon
            name="chat"
            type="entypo"
            color={this.props.color}
          />
        </View>
      )
    }
    return (
      <View style={styles.viewContainer}>
        <Icon
          name="chat"
          type="Entypo"
          color={this.props.color}
        />

        <View style={styles.badge}>
          <Text style={styles.text}>
            {this.props.data.getMessageNotifications.length}
          </Text>
        </View>
      </View>
    )
  }
}

export default compose(
  connect(state => ({
    userId: state.user.userId
  })),
  graphql(msgNoti_QUERY, {
    options: {
      pollInterval: 5000
    }
  })
)(ChatIcon)
