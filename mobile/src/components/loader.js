import React, { Component } from 'react'
import {
  View,
  ActivityIndicator,
  StyleSheet
} from 'react-native'

class Loader extends Component {
  render() {
    return (
      <ActivityIndicator
        size="large"
        style={styles.loader}
      />
    )
  }
}

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Loader
