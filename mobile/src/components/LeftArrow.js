import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import { Icon } from 'react-native-elements'

class LeftArrow extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <Icon
          name="arrow-bold-left"
          type="entypo"
          color="#ffffff"
        />
      </TouchableOpacity>
    )
  }
}

export default LeftArrow
