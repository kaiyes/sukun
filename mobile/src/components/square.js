import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Icon } from 'react-native-elements'

class Square extends Component {
  render() {
    return (
      <View style={styles.navContainer}>
        <View style={styles.container}>
          <Icon
            size={50}
            name="ios-arrow-dropleft"
            type="ionicon"
            color="white"
            iconStyle={styles.icon}
            onPress={this.props.onPress}
          />
          <Text style={styles.text}>
            Frequently Asked Question
          </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#16CAFB',
    height: 200,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  navContainer: {
    flex: 1,
    top: 0,
    right: 0,
    left: 0,
    alignItems: 'stretch',
    position: 'absolute'
  },
  icon: {
    // marginTop: 40,
    marginLeft: 10
  },
  text: {
    fontWeight: '700',
    fontSize: 20,
    color: 'white',
    fontFamily: 'Helvetica Neue',
    marginRight: 45
  }
})

export default Square
