import React, { Component } from 'react'
import {
  Text,
  Image,
  View,
  StyleSheet,
  ActivityIndicator,
  Platform
} from 'react-native'
import { connect } from 'react-redux'
import { Icon } from 'react-native-elements'
import { graphql, compose } from 'react-apollo'
import getViewNotifications_QUERY from '../graphql/queries/getViewNotification.query'

import styles from './componentStyle'

class VisitorIcon extends Component {
  render() {
    if (this.props.data.loading) {
      return <ActivityIndicator />
    }

    if (this.props.data.getViewNotifications.length === 0) {
      return (
        <View style={styles.viewContainer}>
          <Icon
            name="people"
            type="simple-line-icon"
            color={this.props.color}
          />
        </View>
      )
    }
    return (
      <View style={styles.viewContainer}>
        <Icon
          name="people"
          type="simple-line-icon"
          color={this.props.color}
        />

        <View style={styles.badge}>
          <Text style={styles.text}>
            {this.props.data.getViewNotifications.length}
          </Text>
        </View>
      </View>
    )
  }
}

export default compose(
  connect(state => ({
    userId: state.user.userId
  })),
  graphql(getViewNotifications_QUERY, {
    options: ownProps => {
      return {
        pollInterval: 7000,
        variables: {
          receiversId: ownProps.userId
        }
      }
    }
  })
)(VisitorIcon)
