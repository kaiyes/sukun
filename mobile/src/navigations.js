import React, { Component } from 'react'
import {
  addNavigationHelpers,
  StackNavigator,
  TabNavigator,
  onNavigationStateChange
} from 'react-navigation'
import { connect } from 'react-redux'
import { withApollo, compose, graphql } from 'react-apollo'
import { Icon } from 'react-native-elements'
import { Platform } from 'react-native'
import {
  width,
  height,
  totalSize
} from 'react-native-dimension'

import ListScreen from './screens/List.Screen'
import ProfileViewsScreen from './screens/ProfileViews.Screen'
import PhotoReqScreen from './screens/PhotoReq.Screen'
import ChatScreen from './screens/Chat.Screen'
import SettingsScreen from './screens/Settings.Screen'
import EditProfileScreen from './screens/EditProfile.Screen'
import editFormScreen from './screens/editForm.Screen'
import FaqScreen from './screens/Faq.Screen'
import AdminChatScreen from './screens/AdminChat.Screen'
import PaymentScreen from './screens/Payment.Screen'
import FormScreen from './screens/Form.Screen'
import LogInScreen from './screens/LogIn.Screen'
import LandingScreen from './screens/Landing.Screen'
import SignUpScreen from './screens/SignUp.Screen'
import BanScreen from './screens/Banned.Screen'
import RejectedScreen from './screens/Rejected.Screen'
import DetailsScreen from './screens/Details.Screen'
import MessageScreen from './screens/Messages.Screen'
import NotApproved from './screens/NotApproved.Screen'
import MatchScreen from './screens/Match.Screen'
import ChatIcon from './components/chatIcon'
import VisitorIcon from './components/visitorIcon'
import PhotoIcon from './components/photoIcon'
import MatchIcon from './components/matchIcon'

import { colors } from './utils/constants'
import USER_QUERY from './graphql/queries/currentUser.query'
import MY_CHATS_QUERY from './graphql/queries/getMyChats.query'
import { userId, completedCv } from './actions/user.action'
import { subbed, unSubbed } from './actions/payment.action'
import { insertChatId } from './actions/chat.action'

import Square from '../src/components/square'

import UserListScreen from './matchMaker/screens/userList.screen'
import MatchDetailsScreen from './matchMaker/screens/MatchDetails.Screen'
import EditMMProfileScreen from './matchMaker/screens/EditMMProfile.Screen'
import QuestionScreen from './matchMaker/screens/Question.Screen'
import MyMatchesScreen from './matchMaker/screens/Matches.screen'
import CandidateScreen from './matchMaker/screens/Candidate.screen'
import ComparisonScreen from './matchMaker/screens/Comaparison.Screen'
// const TAB_ICON_SIZE = 20

const Tabs = TabNavigator(
  {
    List: {
      screen: ListScreen,
      navigationOptions: () => ({
        headerLeft: null,
        title: 'Home',
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="home"
            type="font-awesome"
            color={tintColor}
          />
        )
      })
    },
    Chat: {
      screen: ChatScreen,
      navigationOptions: () => ({
        title: 'Chat',
        headerLeft: null,
        tabBarIcon: ({ tintColor, screenProps }) => (
          <ChatIcon color={tintColor} />
        )
      })
    },
    Photo: {
      screen: PhotoReqScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Photo',
        headerLeft: null,
        tabBarIcon: ({ tintColor, screenProps }) => (
          <PhotoIcon color={tintColor} />
        )
      })
    },
    Visitors: {
      screen: ProfileViewsScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Visitors',
        headerLeft: null,
        tabBarIcon: ({ tintColor, screenProps }) => (
          <VisitorIcon color={tintColor} />
        )
      })
    }
    // Match: {
    //   screen: MatchScreen,
    //   navigationOptions: () => ({
    //     headerTitle: 'Settings',
    //     headerLeft: null,
    //     tabBarIcon: ({ tintColor, screenProps }) => (
    //       <MatchIcon color={tintColor} />
    //     )
    //   })
    // }
  },
  {
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    lazy: false,
    tabBarOptions: {
      showLabel: false,
      showIcon: true,
      activeTintColor: 'black',
      inactiveTintColor: '#4990E2',
      style: {
        backgroundColor: 'white',
        paddingVertical: 5
      }
    }
  }
)

const AppMainNav = StackNavigator(
  {
    Home: {
      screen: Tabs
    },
    EditProfile: {
      screen: editFormScreen,
      navigationOptions: () => ({
        headerTitle: 'Edit Profile'
      })
    },
    Faq: {
      screen: FaqScreen,
      navigationOptions: () => ({
        headerTitle: 'Faq'
      })
    },
    AdminChat: {
      screen: AdminChatScreen,
      navigationOptions: () => ({
        headerTitle: 'Chat with Admin'
      })
    },
    Payment: {
      screen: PaymentScreen,
      navigationOptions: () => ({
        headerTitle: 'Payment'
      })
    },
    Details: {
      screen: DetailsScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: `${navigation.state.params.username}`
      })
    },
    Messages: {
      screen: MessageScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: `chat with ${
          navigation.state.params.username
        }`
      })
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: 'Settings'
      })
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      headerRight: (
        <Icon
          name="settings"
          type="simple-line-icon"
          color="#ffffff"
          containerStyle={{ marginRight: 10 }}
          onPress={() => {
            navigation.navigate('Settings')
          }}
        />
      ),
      headerStyle: {
        backgroundColor: '#4d9fff'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      }
    })
  }
  // { headerMode: 'none' }
)

// ####################### matchMaker nav   #############################
const MatchNav = TabNavigator(
  {
    UserList: {
      screen: UserListScreen,
      navigationOptions: () => ({
        headerLeft: null,
        title: 'List',
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="home"
            type="font-awesome"
            color={tintColor}
          />
        )
      })
    },
    MyMatches: {
      screen: MyMatchesScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Matches',
        headerLeft: null,
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="heartbeat"
            type="font-awesome"
            color={tintColor}
          />
        )
      })
    },
    MMChatScreen: {
      screen: ChatScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Chat',
        headerLeft: null,
        tabBarIcon: ({ tintColor }) => (
          <ChatIcon color={tintColor} />
        )
      })
    },
    CandidateScreen: {
      screen: CandidateScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Chat',
        headerLeft: null,
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="nature-people"
            type="material-community"
            color={tintColor}
          />
        )
      })
    }
  },
  {
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    lazy: false,
    tabBarOptions: {
      showLabel: false,
      showIcon: true,
      activeTintColor: 'black',
      inactiveTintColor: '#4990E2',
      style: {
        backgroundColor: 'white',
        paddingVertical: 5
      }
    }
  }
)

const MatchStackNav = StackNavigator(
  {
    Home: {
      screen: MatchNav
    },
    MatchDetails: {
      screen: MatchDetailsScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: `${navigation.state.params.username}`
      })
    },
    Comparison: {
      screen: ComparisonScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: `${navigation.state.params.username}`
      })
    },
    Messages: {
      screen: MessageScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: `chat with ${
          navigation.state.params.username
        }`
      })
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: 'Settings'
      })
    },
    Faq: {
      screen: FaqScreen,
      navigationOptions: () => ({
        headerTitle: 'Faq'
      })
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      headerRight: (
        <Icon
          name="settings"
          type="simple-line-icon"
          color="#ffffff"
          containerStyle={{ marginRight: 10 }}
          onPress={() => {
            navigation.navigate('Settings')
          }}
        />
      ),
      headerStyle: {
        backgroundColor: '#4d9fff'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      }
    })
  }
)

// ###################### match Nav ends ##############################

class AppNavigator extends Component {
  state = {
    isBanned: false,
    isRejected: false,
    hasCompletedCv: false,
    isMatchMaker: false,
    isUser: false,
    userType: ''
  }
  componentWillMount() {
    this._getUserInfo()
    this._setUsersChats()
  }

  _setUsersChats = async () => {
    const { data } = await this.props.client.query({
      query: MY_CHATS_QUERY
    })

    if (data.getMyChats.length != 0) {
      await data.getMyChats.map(async item => {
        if (item.inviter._id === this.props.userId) {
          return await this.props.dispatch(
            insertChatId(item.responder._id, item._id)
          )
        } else {
          return await this.props.dispatch(
            insertChatId(item.inviter._id, item._id)
          )
        }
      })
    }
  }

  _getUserInfo = async () => {
    try {
      const { data } = await this.props.client.query({
        query: USER_QUERY
      })

      if (data.userInfo.isBanned) {
        this.setState({ isBanned: true })
      }

      if (data.userInfo.isRejected) {
        this.setState({ isRejected: true })
      }

      if (data.userInfo.hasPaid) {
        await this.props.dispatch(subbed())
      } else {
        await this.props.dispatch(unSubbed())
      }

      if (data.userInfo.hasCompletedCv) {
        await this.setState({ hasCompletedCv: true })
        await this.props.dispatch(completedCv())
      }

      return await this.props.dispatch(
        userId(
          data.userInfo._id,
          data.userInfo.gender,
          data.userInfo.userType,
          data.userInfo.isApproved
        )
      )
    } catch (e) {
      console.log(err)
    }
  }

  render() {
    if (!this.props.isAuthenticated) {
      return <LandingScreen />
    }

    if (!this.props.hasCompletedCv) {
      return <EditProfileScreen />
    }

    if (this.state.isBanned) {
      return <BanScreen />
    }

    if (this.state.isRejected) {
      return <RejectedScreen />
    }

    return <AppMainNav />
  }
}

export default withApollo(
  connect(state => ({
    // nav: state.nav,
    isAuthenticated: state.user.isAuthenticated,
    hasCompletedCv: state.user.hasCompletedCv,
    userId: state.user.userId
  }))(AppNavigator)
)
