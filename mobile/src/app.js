import React from 'react'
import { UIManager, AsyncStorage } from 'react-native'
import { ApolloProvider } from 'react-apollo'
import OneSignal from 'react-native-onesignal'

import { store, client } from './store'
import { colors } from './utils/constants'

import AppNavigation from './navigations'

if (UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true)
}
console.disableYellowBox = true

export default class mobile extends React.Component {
  async componentWillMount() {
    await OneSignal.init(
      'fb34d748-997a-492f-a017-117267053e07'
    )
    this._checkIfToken()
  }

  _checkIfToken = async () => {
    try {
      const token = await AsyncStorage.getItem(
        '@login:token'
      )

      // await AsyncStorage.removeItem('@cv:cv')
      // await AsyncStorage.removeItem('@login:token')
      if (token !== null) {
        await store.dispatch({ type: 'LOGIN' })
      }
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    return (
      <ApolloProvider store={store} client={client}>
        <AppNavigation />
      </ApolloProvider>
    )
  }
}
