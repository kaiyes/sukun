import { combineReducers } from 'redux'

import user from './user.reducer'
import notifications from './notification.reducer'
import payment from './payment.reducer'
import chat from './chat.reducer'

export default client =>
  combineReducers({
    apollo: client.reducer(),
    user,
    notifications,
    payment,
    chat
  })
