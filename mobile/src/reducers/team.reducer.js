const initialState = {
  teamNumber: '',
  partnerName: ''
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'TEAM':
      return {
        teamNumber: action.teamNumber,
        partnerName: action.partnerName
      }
    default:
      return state
  }
}
