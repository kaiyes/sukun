const initialState = {
  list: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'CHAT_LIST':
      state.list.push({
        userId: action.userId,
        chatRoomId: action.chatRoomId
      })
      return {
        list: [...state.list]
      }
    default:
      return state
  }
}
