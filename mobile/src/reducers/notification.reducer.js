const initialState = {
  viewNotification: 0,
  photoNotification: 0
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'INC_VIEW_NOTI':
      return {
        ...state,
        viewNotification: action.number
      }
    case 'DEC_VIEW_NOTI':
      return {
        ...state,
        viewNotification: 0
      }
    case 'INC_PHOTO_NOTI':
      return {
        ...state,
        photoNotification: action.number
      }
    case 'DEC_PHOTO_NOTI':
      return {
        ...state,
        photoNotification: 0
      }
    default:
      return state
  }
}
