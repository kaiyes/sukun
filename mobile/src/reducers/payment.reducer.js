const initialState = {
  hasPaid: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SUBBED':
      return {
        ...state,
        hasPaid: true
      }
    case 'UN_SUBBED':
      return {
        ...initialState
      }
    default:
      return state
  }
}
