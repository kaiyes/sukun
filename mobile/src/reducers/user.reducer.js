const initialState = {
  isAuthenticated: false,
  hasCompletedCv: false,
  approved: false,
  userId: '',
  gender: '',
  userType: ''
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        isAuthenticated: true
      }
    case 'LOGOUT':
      return {
        ...initialState
      }
    case 'CV':
      return {
        ...state,
        hasCompletedCv: true
      }
    case 'USERID':
      return {
        ...state,
        userId: action.id,
        gender: action.gender,
        userType: action.userType,
        approved: action.approved
      }
    default:
      return state
  }
}
