import { gql } from 'react-apollo'

export default gql`
  mutation createMatchMakerTeam(
    $team: String!
    $username: String!
    $avatar: String!
    $pic: String!
  ) {
    createMatchMakerTeam(
      team: $team
      username: $username
      avatar: $avatar
      pic: $pic
    ) {
      team
      brothersId
      brothersPic
      brothersAvatar
      brothersUsername

      sistersId
      sistersPic
      sistersAvatar
      sistersUsername
    }
  }
`
