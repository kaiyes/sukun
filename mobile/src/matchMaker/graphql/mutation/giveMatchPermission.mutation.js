import { gql } from 'react-apollo'

export default gql`
  mutation giveMatchPermission($_id: String) {
    giveMatchPermission(_id: $_id) {
      approved
    }
  }
`
