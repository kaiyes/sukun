import { gql } from 'react-apollo'

export default gql`
  mutation updateTeam(
    $team: String
    $username: String
    $pic: String
  ) {
    updateTeam(
      team: $team
      username: $username
      pic: $pic
    ) {
      team
      sistersUsername
      brothersUsername
    }
  }
`
