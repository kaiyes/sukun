import { gql } from 'react-apollo'

export default gql`
  mutation createMatch(
    $bride: String
    $groom: String
    $team: String
  ) {
    createMatch(bride: $bride, groom: $groom, team: $team) {
      bride {
        cv {
          username
        }
      }
      groom {
        cv {
          username
        }
      }
      team {
        team
      }
    }
  }
`
