import { gql } from 'react-apollo'

export default gql`
  mutation removeMatchReq($_id: String) {
    removeMatchReq(_id: $_id) {
      approved
    }
  }
`
