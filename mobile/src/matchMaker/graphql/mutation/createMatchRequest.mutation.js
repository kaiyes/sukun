import { gql } from 'react-apollo'

export default gql`
  mutation makeMatchReq(
    $team: String
    $_id: String
    $gender: String
  ) {
    makeMatchReq(team: $team, _id: $_id, gender: $gender) {
      approved
    }
  }
`
