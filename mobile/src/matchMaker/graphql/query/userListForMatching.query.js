import { gql } from 'react-apollo'

export default gql`
  query {
    userListForMatching {
      _id
      cv {
        username
        profession
        age
        beard
        gender
        photoApproved
        pic
        avatar
        avatar
      }
    }
  }
`
