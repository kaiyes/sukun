import { gql } from 'react-apollo'

export default gql`
  query {
    myTeam {
      team
      permissions
    }
  }
`
