import { gql } from 'react-apollo'

export default gql`
  query {
    getUnfinishedTeams {
      team
      brothersId
      brothersPic
      brothersAvatar
      brothersUsername

      sistersId
      sistersPic
      sistersAvatar
      sistersUsername
    }
  }
`
