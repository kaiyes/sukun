import { gql } from 'react-apollo'

export default gql`
  query {
    teamMalePermissions {
      _id
      approved
      candidate {
        _id
        cv {
          username
          age
          profession
          homeDistrict
          currentLocation
          practisingSince
          beard
          prayer
          education
          aboutMe
          height
          hifzLevel
          gender
          avatar
        }
      }
    }
  }
`
