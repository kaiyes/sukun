import { gql } from 'react-apollo'

export default gql`
  query {
    getTeamMatches {
      team {
        team
      }
      bride {
        _id
        cv {
          username
          avatar
        }
      }
      groom {
        _id
        cv {
          username
          avatar
        }
      }
    }
  }
`
