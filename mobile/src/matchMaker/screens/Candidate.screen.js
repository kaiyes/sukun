import React, { Component } from 'react'
import { graphql, compose } from 'react-apollo'
import {
  ActivityIndicator,
  FlatList,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  RefreshControl
} from 'react-native'
import {
  Card,
  ListItem,
  Button,
  Divider,
  Avatar,
  Badge
} from 'react-native-elements'
import { connect } from 'react-redux'
import ScrollableTabView from 'react-native-scrollable-tab-view'

import pendingMaleCandidate_QUERY from '../graphql/query/pendingMaleCandidates.query'
import pendingFemaleCandidate_QUERY from '../graphql/query/pendingFemaleCandidates.query'
import teamFemalePermissions_QUERY from '../graphql/query/approvedFemaleCandidates.query'
import teamMalePermissions_QUERY from '../graphql/query/approvedMaleCandidates.query'

class CandidateScreen extends Component {
  state = {
    refreshing: false
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.props.pendingMaleCandidates.refetch()
    await this.props.pendingFemaleCandidates.refetch()
    await this.props.approvedSisters.refetch()
    await this.props.approvedBrothers.refetch()
    this.setState({ refreshing: false })
  }

  render() {
    const {
      pendingMaleCandidates,
      pendingFemaleCandidates,
      approvedSisters,
      approvedBrothers,
      gender
    } = this.props

    if (
      pendingMaleCandidates.loading ||
      pendingFemaleCandidates.loading ||
      approvedSisters.loading ||
      approvedBrothers.loading
    ) {
      return (
        <View>
          <ActivityIndicator
            size="large"
            style={styles.loader}
          />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <ScrollableTabView>
          <ScrollView
            tabLabel="Sisters"
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            {approvedSisters.teamFemalePermissions.map(
              (item, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() =>
                      this.props.navigation.navigate(
                        'Comparison',
                        {
                          username: `${
                            item.candidate.cv.username
                          }`,
                          id: item.candidate._id,
                          approved: true,
                          matchReqId: item._id
                        }
                      )
                    }
                  >
                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.candidate.cv.avatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{
                          marginLeft: 10
                        }}
                      />
                      <View style={styles.textContainer}>
                        <Text style={styles.username}>
                          {item.candidate.cv.username}
                        </Text>
                        <Text style={{ color: '#696456' }}>
                          {item.candidate.cv.profession}
                        </Text>
                        <Text style={{ color: '#696456' }}>
                          {item.candidate.cv.age}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                )
              }
            )}
          </ScrollView>

          <ScrollView
            tabLabel="Brothers"
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            {approvedBrothers.teamMalePermissions.map(
              (item, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() =>
                      this.props.navigation.navigate(
                        'Comparison',
                        {
                          username: `${
                            item.candidate.cv.username
                          }`,
                          id: item.candidate._id,
                          approved: true,
                          matchReqId: item._id
                        }
                      )
                    }
                  >
                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.candidate.cv.avatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{
                          marginLeft: 10
                        }}
                      />
                      <View style={styles.textContainer}>
                        <Text style={styles.username}>
                          {item.candidate.cv.username}
                        </Text>
                        <Text style={{ color: '#696456' }}>
                          {item.candidate.cv.profession}
                        </Text>
                        <Text style={{ color: '#696456' }}>
                          {item.candidate.cv.age}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                )
              }
            )}
          </ScrollView>

          <ScrollView
            tabLabel="Candidates"
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            {gender === 'female'
              ? pendingFemaleCandidates.teamPendingFemalePermissions.map(
                  (item, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        onPress={() =>
                          this.props.navigation.navigate(
                            'Comparison',
                            {
                              username: `${
                                item.candidate.cv.username
                              }`,
                              id: item.candidate._id,
                              approved: false,
                              matchReqId: item._id
                            }
                          )
                        }
                      >
                        <View style={styles.card}>
                          <Avatar
                            medium
                            rounded
                            source={{
                              uri: item.candidate.cv.avatar
                            }}
                            activeOpacity={0.5}
                            containerStyle={{
                              marginLeft: 10
                            }}
                          />
                          <View
                            style={styles.textContainer}
                          >
                            <Text style={styles.username}>
                              {item.candidate.cv.username}
                            </Text>
                            <Text
                              style={{ color: '#696456' }}
                            >
                              {item.candidate.cv.profession}
                            </Text>
                            <Text
                              style={{ color: '#696456' }}
                            >
                              {item.candidate.cv.age}
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    )
                  }
                )
              : pendingMaleCandidates.teamPendingMalePermissions.map(
                  (item, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        onPress={() =>
                          this.props.navigation.navigate(
                            'Comparison',
                            {
                              username: `${
                                item.candidate.cv.username
                              }`,
                              id: item.candidate._id,
                              approved: false,
                              matchReqId: item._id
                            }
                          )
                        }
                      >
                        <View style={styles.card}>
                          <Avatar
                            medium
                            rounded
                            source={{
                              uri: item.candidate.cv.avatar
                            }}
                            activeOpacity={0.5}
                            containerStyle={{
                              marginLeft: 10
                            }}
                          />
                          <View
                            style={styles.textContainer}
                          >
                            <Text style={styles.username}>
                              {item.candidate.cv.username}
                            </Text>
                            <Text
                              style={{ color: '#696456' }}
                            >
                              {item.candidate.cv.profession}
                            </Text>
                            <Text
                              style={{ color: '#696456' }}
                            >
                              {item.candidate.cv.age}
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    )
                  }
                )}
          </ScrollView>
        </ScrollableTabView>

        {this.props.isApproved ? null : (
          <View style={styles.absolute}>
            <Text style={styles.blurredText}>
              Please Wait for approval
            </Text>
            <View style={{ marginTop: 10 }} />
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  card: {
    // backgroundColor: '#E3D5FC',
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 5,
    marginTop: 5
  },
  textContainer: {
    margin: 20
  },
  username: {
    fontWeight: '900',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue'
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(211, 233, 240, 0.78)'
  },
  blurredText: {
    color: '#314e79',
    fontSize: 26,
    fontWeight: '700'
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#356490'
  }
})

export default compose(
  connect(state => ({
    userId: state.user.userId,
    isApproved: state.user.approved,
    gender: state.user.gender
  })),
  graphql(pendingMaleCandidate_QUERY, {
    name: 'pendingMaleCandidates'
  }),
  graphql(pendingFemaleCandidate_QUERY, {
    name: 'pendingFemaleCandidates'
  }),
  graphql(teamFemalePermissions_QUERY, {
    name: 'approvedSisters'
  }),
  graphql(teamMalePermissions_QUERY, {
    name: 'approvedBrothers'
  })
)(CandidateScreen)
