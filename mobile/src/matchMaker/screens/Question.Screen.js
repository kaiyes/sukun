import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  TextInput,
  ScrollView
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {
  Divider,
  Button,
  Icon,
  Avatar,
  Header
} from 'react-native-elements'
import {
  width,
  height,
  totalSize
} from 'react-native-dimension'
import { graphql, withApollo, compose } from 'react-apollo'
import { connect } from 'react-redux'

import USER_QUERY from '../../graphql/queries/currentUser.query'
import { completedCv } from '../../actions/user.action'

import stateObject from '../../utils/stateObject'
import LeftArrow from '../../components/LeftArrow'
import FormOnly from './FormOnly.Screen'
import TeamChoosing from './Teams.Screen'

class Questions extends Component {
  state = {
    partnerSignedup: false,
    showForm: false
  }

  _onNoPress = () => {
    this.setState({
      showForm: true
    })
  }

  _onYesPress = () => {
    this.setState({
      showForm: false,
      partnerSignedup: true
    })
  }

  render() {
    const { gender, data } = this.props
    const {
      loading,
      partnerSignedup,
      firstLoad,
      showForm
    } = this.state

    if (loading || data.loading) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      )
    }
    if (showForm) {
      return <FormOnly />
    } else if (partnerSignedup) {
      return <TeamChoosing />
    }

    return (
      <View style={styles.loader}>
        <Text style={styles.bigText}>
          {'        '}has your{' '}
          {gender === 'female' ? 'husband' : 'wife'} already{' '}
          {'\n'}
          signed Up as a matchmaker ?
        </Text>
        <View style={{ flexDirection: 'row' }}>
          <Button
            buttonStyle={styles.secondButton}
            title="Yes"
            onPress={this._onYesPress}
          />
          <Button
            buttonStyle={styles.secondButton}
            title="No"
            onPress={this._onNoPress}
          />
        </View>
      </View>
    )
  }
}

export default compose(
  graphql(USER_QUERY),
  connect(
    state => ({
      gender: state.user.gender
    }),
    { completedCv }
  )
)(Questions)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  secondButton: {
    backgroundColor: '#291328',
    borderRadius: 7,
    marginTop: 20,
    width: 150
  },
  bigText: {
    color: '#404aa6',
    fontSize: 22,
    fontWeight: '700'
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
