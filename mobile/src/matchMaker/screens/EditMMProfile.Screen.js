import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
  Divider,
  Button,
  Icon,
  Avatar,
  Header,
} from 'react-native-elements';
import {
  width,
  height,
  totalSize,
} from 'react-native-dimension';
import { graphql, withApollo, compose } from 'react-apollo';
import { connect } from 'react-redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SearchBar from 'react-native-searchbar';
import DropdownAlert from 'react-native-dropdownalert';

import USER_QUERY from '../../graphql/queries/currentUser.query';
import CREATECV_MUTATION from '../../graphql/mutation/createCv.mutation';
import createMatchMaker_MUTATION from '../graphql/mutation/createMatchMaker.mutation';
import team_QUERY from '../graphql/query/team.query';
import { completedCv } from '../../actions/user.action';

import stateObject from '../../utils/stateObject';
import LeftArrow from '../../components/LeftArrow';

class EditMMProfileScreen extends Component {
  state = stateObject;

  _onPhotoPress = async (pic, uploaded) => {
    console.log(pic);
  };

  _onChangeText = async (text, type, nameOfState) => {
    await this.setState({
      [type]: text,
      [nameOfState]: true,
    });
  };

  _onSubmitPress = async () => {
    const {
      uploadedPic1,
      pic1,
      username,
      pics,
      nidCopy,
      avatar,
      team,
    } = this.state;

    this.setState({ loading: true });

    const { data } = await this.props
      .createMatchMaker({
        variables: {
          team: this.state.team,
          pic: pic1,
          avatar:
            this.props.gender === 'female'
              ? 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/mm_ods1ne.png'
              : 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/fm_ypnv4d.png',
          username,
        },
      })
      .catch(e => {
        this.setState({ loading: false });
        this.dropdown.alertWithType(
          'error',
          'Error',
          'টিম নাম্বার অলরেডি আছে ডাটাবেজে, অন্য একটা দিন'
        );
      });
    if (data.createMatchMakerTeam.team != null) {
      console.log(data);
      await this.props
        .mutate({
          variables: {
            age: 60,
            height: 'x',
            education: 'x',
            prayer: 'x',
            practisingSince: 'x',
            beard: 'x',
            homeDistrict: 'x',
            currentLocation: 'x',
            hifzLevel: 'x',
            profession: 'x',
            username,
            avatar:
              this.props.gender === 'female'
                ? 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/mm_ods1ne.png'
                : 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/fm_ypnv4d.png',
            pic: pics,
            aboutMe: 'x',
            nid: nidCopy,
          },
        })
        .catch(err => {
          console.log(`${err}`);
        });
      await AsyncStorage.setItem('@cv:cv', 'true');
      await this.props.completedCv();
      this.setState({ loading: false });
    }
    await this.setState({ loading: false });
  };

  _showSearchBar = () => {
    this.searchBar.show();
  };

  _onNoPress = () => {
    this.setState({
      firstLoad: false,
      partnerSignedup: false,
      showForm: false,
    });
  };

  _onYesPress = () => {
    this.setState({
      firstLoad: false,
      partnerSignedup: true,
      showForm: false,
    });
  };

  _handleResults = async results => {
    await this.setState({ result: results });
  };

  _renderForm = () => {
    const { gender } = this.props;
    const {
      nid,
      uploadedPic1,
      pic1,
      team,
      username,
      showForm,
      firstLoad,
    } = this.state;
    return (
      <View
        style={{
          alignItems: 'center',
          marginTop: height(5),
        }}
      >
        <Header
          backgroundColor="#2479c7"
          leftComponent={
            <LeftArrow
              onPress={() => {
                this.setState({
                  firstLoad: true,
                  showForm: false,
                });
              }}
            />
          }
          centerComponent={{
            text: 'Back to selection Page',
            style: {
              color: '#14141a',
              fontSize: 20,
              fontWeight: '700',
            },
          }}
        />
        <View style={{ marginTop: height(10) }} />
        <View style={styles.iconContainer}>
          <TouchableOpacity
            onPress={(pic, uploadedPic) =>
              this._onPhotoPress('pic1', 'uploadedPic1')
            }
          >
            <Image
              source={
                uploadedPic1
                  ? { uri: pic1 }
                  : require('../../../assets/icons/photo1.png')
              }
              style={styles.icon}
            />
          </TouchableOpacity>
        </View>
        <TextInput
          style={styles.form}
          placeholder="username"
          value={username}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          onChangeText={text =>
            this._onChangeText(text, 'username')
          }
        />

        <TextInput
          style={styles.form}
          placeholder="4 digit Team Number"
          value={team}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          keyboardType="numeric"
          maxLength={4}
          onChangeText={text =>
            this._onChangeText(text, 'team')
          }
        />
        <Text style={styles.text}>
          {' '}
          mistake this, your matches won't work properly
        </Text>
        <Button
          buttonStyle={styles.button}
          title="Submit"
          titleStyle={{ color: 'black' }}
          onPress={this._onSubmitPress}
          disabled={
            username != '' &&
            team != '' &&
            uploadedPic1 === true
              ? false
              : true
          }
        />
        {showForm ? (
          <View style={styles.avatarContainer}>
            <Avatar
              large
              rounded
              source={{
                uri:
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/mm_ods1ne.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/fm_ypnv4d.png',
              }}
              activeOpacity={0.5}
            />

            <Text style={styles.bigText}>
              {this.state.partnerName}
            </Text>

            <Text style={styles.bigText}>
              {this.state.team}
            </Text>
          </View>
        ) : null}
      </View>
    );
  };

  render() {
    const { gender, data, teamQuery } = this.props;
    const {
      loading,
      nid,
      uploadedPic1,
      pic1,
      team,
      username,
      partnerSignedup,
      firstLoad,
      results,
      showForm,
    } = this.state;

    if (loading || data.loading || teamQuery.loading) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      );
    }

    return (
      <KeyboardAvoidingView
        behavior="padding"
        style={styles.container}
      >
        {firstLoad ? (
          <View style={styles.loader}>
            <Text style={styles.bigText}>
              {'        '}has your{' '}
              {gender === 'female' ? 'husband' : 'wife'}{' '}
              already {'\n'}
              signed Up as a matchmaker ?
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <Button
                buttonStyle={styles.secondButton}
                title="Yes"
                onPress={this._onYesPress}
              />
              <Button
                buttonStyle={styles.secondButton}
                title="No"
                onPress={this._onNoPress}
              />
            </View>
          </View>
        ) : partnerSignedup ? (
          <View style={styles.container}>
            <View style={styles.seceondView}>
              {showForm ? (
                this._renderForm()
              ) : (
                <View style={styles.seceondView}>
                  <View style={{ marginTop: height(10) }} />
                  <SearchBar
                    ref={ref => (this.searchBar = ref)}
                    data={teamQuery.getUnfinishedTeams}
                    handleResults={results => {
                      this.setState({ results });
                    }}
                    showOnLoad
                  />

                  <Button
                    buttonStyle={styles.button}
                    title="show search bar"
                    titleStyle={{ color: 'black' }}
                    onPress={this._showSearchBar}
                  />

                  <ScrollView>
                    {results.map((item, index, gender) => {
                      return (
                        <View
                          key={index}
                          style={styles.avatarContainer}
                        >
                          <Avatar
                            large
                            rounded
                            source={{
                              uri:
                                gender === 'male'
                                  ? item.brothersAvatar
                                  : item.sistersAvatar,
                            }}
                            activeOpacity={0.5}
                          />

                          <Text style={styles.bigText}>
                            {gender === 'male'
                              ? item.sistersUsername
                              : item.brothersUsername}
                          </Text>
                          <Text style={styles.bigText}>
                            {item.team}
                          </Text>
                          <TouchableOpacity
                            style={styles.tickCircle}
                            onPress={async () => {
                              this.setState({
                                showForm: true,
                                team: item.team,
                                partnerName:
                                  gender === 'female'
                                    ? item.brothersUsername
                                    : item.sistersUsername,
                              });
                            }}
                          >
                            <Icon
                              name="check"
                              type="entypo"
                              color="white"
                            />
                          </TouchableOpacity>
                        </View>
                      );
                    })}
                  </ScrollView>
                </View>
              )}
            </View>
          </View>
        ) : (
          this._renderForm()
        )}

        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </KeyboardAvoidingView>
    );
  }
}

export default compose(
  graphql(CREATECV_MUTATION),
  graphql(createMatchMaker_MUTATION, {
    name: 'createMatchMaker',
  }),
  graphql(USER_QUERY),
  graphql(team_QUERY, {
    name: 'teamQuery',
  }),
  connect(
    state => ({
      gender: state.user.gender,
    }),
    { completedCv }
  )
)(EditMMProfileScreen);

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  form: {
    height: 50,
    width: 300,
    borderColor: '#62a2ec',
    borderWidth: 1,
    borderRadius: 22,
    margin: 5,
    textAlign: 'center',
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  icon: {
    width: 90,
    height: 90,
    borderRadius: 45,
  },
  button: {
    backgroundColor: '#291328',
    borderRadius: 7,
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 20,
    width: 150,
  },
  secondButton: {
    backgroundColor: '#291328',
    borderRadius: 7,
    marginTop: 20,
    width: 150,
  },
  text: {
    color: '#404aa6',
    fontSize: 12,
    fontStyle: 'italic',
    textAlign: 'center',
    fontWeight: '400',
    width: 250,
    margin: 10,
  },
  bigText: {
    color: '#404aa6',
    fontSize: 22,
    fontWeight: '700',
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15,
    backgroundColor: '#fbc3ff',
    height: height(14),
    width: width(90),
    borderRadius: 10,
  },
  tickCircle: {
    width: 60,
    height: 60,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#852c87',
  },
  seceondView: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
