import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  AsyncStorage,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
  Button,
  Icon,
  Header,
} from 'react-native-elements';
import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';
import DropdownAlert from 'react-native-dropdownalert';

import USER_QUERY from '../../graphql/queries/currentUser.query';
import CREATECV_MUTATION from '../../graphql/mutation/createCv.mutation';
import createMatchMaker_MUTATION from '../graphql/mutation/createMatchMaker.mutation';
import { completedCv } from '../../actions/user.action';

import LeftArrow from '../../components/LeftArrow';
import Question from './Question.Screen';

class FormOnly extends Component {
  state = {
    firstLoad: false,
    pic1: '',
    uploadedPic: '',
    username: '',
    team: '',
    pics: [],
    nidCopy: '',
    loading: false,
    avatar: '',
  };
  _onChangeText = async (text, type) => {
    await this.setState({
      [type]: text,
    });
  };

  _onPhotoPress = async (pic, uploaded) => {
    console.log(pic);
  };

  _onSubmitPress = async () => {
    const {
      uploadedPic1,
      pic1,
      username,
      pics,
      nidCopy,
      avatar,
      team,
    } = this.state;

    this.setState({ loading: true });

    const { data } = await this.props
      .createMatchMaker({
        variables: {
          team: team,
          pic: pic1,
          username,
          avatar:
            this.props.gender === 'female'
              ? 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/mm_ods1ne.png'
              : 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/fm_ypnv4d.png',
        },
      })
      .catch(e => {
        this.setState({ loading: false });
        this.dropdown.alertWithType(
          'error',
          'Error',
          'টিম নাম্বার অলরেডি আছে ডাটাবেজে, অন্য একটা দিন'
        );
      });
    if (data.createMatchMakerTeam.team != null) {
      console.log(data);
      await this.props
        .mutate({
          variables: {
            age: 60,
            height: 'x',
            education: 'x',
            prayer: 'x',
            practisingSince: 'x',
            beard: 'x',
            homeDistrict: 'x',
            currentLocation: 'x',
            hifzLevel: 'x',
            profession: 'x',
            username,
            avatar:
              this.props.gender === 'female'
                ? 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/mm_ods1ne.png'
                : 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/fm_ypnv4d.png',
            pic: pics,
            aboutMe: 'x',
            nid: nidCopy,
          },
        })
        .catch(err => {
          console.log(`${err}`);
        });
      await AsyncStorage.setItem('@cv:cv', 'true');
      await this.props.completedCv();
      this.setState({ loading: false });
    }
    await this.setState({ loading: false });
  };

  render() {
    const { gender, data } = this.props;
    const {
      loading,
      nid,
      pic1,
      uploadedPic1,
      firstLoad,
      username,
      team,
    } = this.state;

    if (loading || data.loading) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      );
    }
    if (firstLoad) {
      return <Question />;
    }

    return (
      <View style={styles.loader}>
        <Header
          backgroundColor="#2479c7"
          leftComponent={
            <LeftArrow
              onPress={() => {
                this.setState({
                  firstLoad: true,
                });
              }}
            />
          }
          centerComponent={{
            text: 'Form',
            style: styles.headerText,
          }}
        />

        <View style={styles.iconContainer}>
          <TouchableOpacity
            onPress={(pic, uploadedPic) =>
              this._onPhotoPress('pic1', 'uploadedPic1')
            }
          >
            <Image
              source={
                uploadedPic1
                  ? { uri: pic1 }
                  : require('../../../assets/icons/photo1.png')
              }
              style={styles.icon}
            />
          </TouchableOpacity>
        </View>

        <TextInput
          style={styles.form}
          placeholder="4 digit Team Number"
          value={team}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          keyboardType="numeric"
          maxLength={4}
          onChangeText={text =>
            this._onChangeText(text, 'team')
          }
        />
        <TextInput
          style={styles.form}
          placeholder="username"
          value={username}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          onChangeText={text =>
            this._onChangeText(text, 'username')
          }
        />
        <Text style={styles.text}>
          {' '}
          your team Number is Very important. Pass this team
          number to your wife/husband when they sign up.
          This is how your other half will find you.If you
          mistake this, your matches won't work properly
        </Text>
        <Button
          buttonStyle={styles.button}
          title="Submit"
          titleStyle={{ color: 'black' }}
          onPress={this._onSubmitPress}
          disabled={
            username != '' &&
            team != '' &&
            uploadedPic1 === true
              ? false
              : true
          }
        />
      </View>
    );
  }
}

export default compose(
  graphql(USER_QUERY),
  graphql(CREATECV_MUTATION),
  graphql(createMatchMaker_MUTATION, {
    name: 'createMatchMaker',
  }),
  connect(
    state => ({
      gender: state.user.gender,
    }),
    { completedCv }
  )
)(FormOnly);

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#291328',
    borderRadius: 7,
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 20,
    width: 150,
  },
  bigText: {
    color: '#404aa6',
    fontSize: 22,
    fontWeight: '700',
  },
  text: {
    color: '#404aa6',
    fontSize: 12,
    fontStyle: 'italic',
    textAlign: 'center',
    fontWeight: '400',
    width: 250,
    margin: 10,
  },
  headerText: {
    color: '#ffffff',
    fontSize: 20,
    fontWeight: '700',
  },
  form: {
    height: 50,
    width: 300,
    borderColor: '#62a2ec',
    borderWidth: 1,
    borderRadius: 22,
    margin: 5,
    textAlign: 'center',
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  icon: {
    width: 90,
    height: 90,
    borderRadius: 45,
  },
});
