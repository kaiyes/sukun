import React, { Component } from 'react'
import { graphql, compose } from 'react-apollo'
import {
  ActivityIndicator,
  FlatList,
  View,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native'
import {
  Card,
  ListItem,
  Button,
  Divider,
  Avatar,
  Badge
} from 'react-native-elements'
import { connect } from 'react-redux'
import { width, height } from 'react-native-dimension'

import matches_QUERY from '../graphql/query/teamMatches.query'

class MyMatchesScreen extends Component {
  render() {
    const { data } = this.props

    if (data.loading) {
      return (
        <View>
          <ActivityIndicator size="large" />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        {data.getTeamMatches.map((item, index) => {
          return (
            <View style={styles.card} key={index}>
              <TouchableOpacity
                style={styles.avatarContainer}
                onPress={() =>
                  this.props.navigation.navigate(
                    'MatchDetails',
                    {
                      username: `${item.groom.cv.username}`,
                      id: item.groom._id
                    }
                  )
                }
              >
                <Avatar
                  large
                  rounded
                  source={{ uri: item.groom.cv.avatar }}
                  activeOpacity={0.5}
                />
                <Text style={styles.name}>
                  {item.groom.cv.username}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.avatarContainer}
                onPress={() =>
                  this.props.navigation.navigate(
                    'MatchDetails',
                    {
                      username: `${item.bride.cv.username}`,
                      id: item.bride._id
                    }
                  )
                }
              >
                <Avatar
                  large
                  rounded
                  source={{ uri: item.bride.cv.avatar }}
                  activeOpacity={0.5}
                />
                <Text style={styles.name}>
                  {item.bride.cv.username}
                </Text>
              </TouchableOpacity>
            </View>
          )
        })}

        {this.props.isApproved ? null : (
          <View style={styles.absolute}>
            <Text style={styles.blurredText}>
              Please Wait for approval
            </Text>
            <View style={{ marginTop: 10 }} />
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  card: {
    backgroundColor: '#E3D5FC',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 5,
    marginTop: 5,
    borderRadius: 7,
    height: height(18)
  },
  avatarContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  textContainer: {
    margin: 20
  },
  username: {
    fontWeight: '900',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue'
  },
  name: {
    fontWeight: '700',
    fontSize: 20,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginTop: 5
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(211, 233, 240, 0.78)'
  },
  blurredText: {
    color: '#314e79',
    fontSize: 26,
    fontWeight: '700'
  }
})

export default compose(
  connect(state => ({
    userId: state.user.userId,
    isApproved: state.user.approved
  })),
  graphql(matches_QUERY)
)(MyMatchesScreen)
