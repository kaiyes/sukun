import React, { Component } from 'react'
import { graphql, compose } from 'react-apollo'
import {
  ActivityIndicator,
  FlatList,
  View,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native'
import {
  Card,
  ListItem,
  Button,
  Divider,
  Avatar,
  Badge
} from 'react-native-elements'
import { connect } from 'react-redux'
import _ from 'lodash'

import USER_LIST_QUERY from '../../matchMaker/graphql/query/userListForMatching.query'

class UserListScreen extends Component {
  render() {
    const { data } = this.props

    if (data.loading) {
      return (
        <View>
          <ActivityIndicator size="large" />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <FlatList
          data={data.userListForMatching}
          keyExtractor={item => item._id}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate(
                  'MatchDetails',
                  {
                    username: `${item.cv.username}`,
                    id: item._id
                  }
                )
              }
            >
              <View style={styles.card}>
                <Avatar
                  medium
                  rounded
                  source={{ uri: item.cv.avatar }}
                  activeOpacity={0.5}
                  containerStyle={{ marginLeft: 10 }}
                />
                <View style={styles.textContainer}>
                  <Text style={styles.username}>
                    {item.cv.username}
                  </Text>
                  <Text style={{ color: '#696456' }}>
                    {item.cv.profession}
                  </Text>
                  <Text style={{ color: '#696456' }}>
                    {item.cv.age}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
        {this.props.isApproved ? null : (
          <View style={styles.absolute}>
            <Text style={styles.blurredText}>
              Please Wait for approval
            </Text>
            <View style={{ marginTop: 10 }} />
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  card: {
    // backgroundColor: '#E3D5FC',
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 5,
    marginTop: 5
  },
  textContainer: {
    margin: 20
  },
  username: {
    fontWeight: '900',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue'
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(211, 233, 240, 0.78)'
  },
  blurredText: {
    color: '#314e79',
    fontSize: 26,
    fontWeight: '700'
  }
})

export default compose(
  connect(state => ({
    userId: state.user.userId,
    isApproved: state.user.approved
  })),
  graphql(USER_LIST_QUERY)
)(UserListScreen)
