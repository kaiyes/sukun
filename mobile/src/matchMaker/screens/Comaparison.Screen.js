import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  Image,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
  Dimensions
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import { connect } from 'react-redux'
import {
  Button,
  Badge,
  Divider,
  Icon,
  Card,
  Avatar
} from 'react-native-elements'
import { width, height } from 'react-native-dimension'
import DropdownAlert from 'react-native-dropdownalert'
import _ from 'lodash'
import ScrollableTabView from 'react-native-scrollable-tab-view'

import getOneCv_QUERY from '../../graphql/queries/getOneCv.query'
import myChats_QUERY from '../../graphql/queries/getMyChats.query'
import teamFemalePermissions_QUERY from '../graphql/query/approvedFemaleCandidates.query'
import teamMalePermissions_QUERY from '../graphql/query/approvedMaleCandidates.query'
import pendingMaleCandidate_QUERY from '../graphql/query/pendingMaleCandidates.query'
import pendingFemaleCandidate_QUERY from '../graphql/query/pendingFemaleCandidates.query'
import myTeam_QUERY from '../graphql/query/myTeam.query'
import CREATECHATROOM_MUTATION from '../../graphql/mutation/createChatRoom.mutation'
import GIVE_PERMISSION_MUTATION from '../graphql/mutation/giveMatchPermission.mutation'
import TAKE_PERMISSION_MUTATION from '../graphql/mutation/removeMatchPermission.mutation'
import CREATE_MATCH_MUTATION from '../graphql/mutation/createMatch.mutation'
import { insertChatId } from '../../actions/chat.action'

class ComparisonScreen extends Component {
  state = {
    me: false,
    refreshing: false
  }

  _onMaleMatchPress = async (groom, bride) => {
    await this.props.match({
      variables: {
        bride,
        groom,
        team: this.props.team.myTeam.team
      }
    })
    this.dropdown.alertWithType(
      'Success',
      'Success',
      'Match Made'
    )
  }

  _onFemaleMatchPress = async (bride, groom) => {
    await this.props.match({
      variables: {
        bride,
        groom,
        team: this.props.team.myTeam.team
      }
    })
    this.dropdown.alertWithType(
      'Success',
      'Success',
      'Match Made'
    )
  }
  _onMatchPlease = async (id, userId) => {
    await this.props.givePermission({
      variables: {
        _id: id
      },
      refetchQueries: [
        { query: teamFemalePermissions_QUERY },
        { query: teamMalePermissions_QUERY },
        { query: pendingMaleCandidate_QUERY },
        { query: pendingFemaleCandidate_QUERY },
        { query: myTeam_QUERY }
      ]
    })
  }
  _onMatchRemove = async userId => {
    await this.props.takePermission({
      variables: {
        _id: this.props.navigation.state.params.matchReqId
      },
      refetchQueries: [
        { query: teamFemalePermissions_QUERY },
        { query: teamMalePermissions_QUERY },
        { query: pendingMaleCandidate_QUERY },
        { query: pendingFemaleCandidate_QUERY },
        { query: myTeam_QUERY }
      ]
    })
  }

  _onChatPress = async () => {
    const {
      id,
      username
    } = this.props.navigation.state.params
    const ifExists = _.find(this.props.chatList, [
      'userId',
      id
    ])
    if (ifExists === undefined) {
      const { data } = await this.props.createChatRoom({
        variables: {
          _id: id
        },
        refetchQueries: [{ query: myChats_QUERY }]
      })
      await this.props.insertChatId(
        id,
        data.createChatRoom._id
      )
      this.props.navigation.navigate('Messages', {
        conversationId: data.createChatRoom._id,
        username: username
      })
    } else {
      this.props.navigation.navigate('Messages', {
        conversationId: ifExists.chatRoomId,
        username: username
      })
    }
  }

  renderAvatar = () => {
    const {
      pic,
      currentLocation,
      avatar,
      aboutMe
    } = this.props.data.getOneCv
    return (
      <View style={{ flexDirection: 'row', marginTop: 10 }}>
        <View style={styles.photoContainer}>
          <Image
            style={{
              width: 50,
              height: 50,
              marginTop: 10,
              marginLeft: 10
            }}
            source={{ uri: avatar }}
          />
          <View style={styles.locationRow}>
            <Icon
              name="dot-single"
              type="entypo"
              color="#42f125"
            />
            <Text>{currentLocation}</Text>
          </View>
        </View>

        <Text style={styles.text}>{aboutMe}</Text>
      </View>
    )
  }

  renderIconContainer = () => {
    const {
      photoPermission,
      navigation,
      userId,
      gender
    } = this.props

    return (
      <View style={styles.iconsContainer}>
        {gender === this.props.data.getOneCv.gender ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._onChatPress}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../../assets/icons/Chat.png')}
            />
          </TouchableOpacity>
        ) : null}

        {navigation.state.params.approved ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={() => {
              this._onMatchRemove(
                navigation.state.params.id
              )
            }}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../../assets/icons/removeRequest.png')}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.icon}
            onPress={() => {
              this._onMatchPlease(
                navigation.state.params.matchReqId,
                navigation.state.params.id
              )
            }}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../../assets/icons/matchPlease.png')}
            />
          </TouchableOpacity>
        )}
      </View>
    )
  }

  _renderApprovedMales = () => {
    const { femaleCandidates, navigation } = this.props

    return (
      <ScrollView horizontal={true}>
        {femaleCandidates.teamFemalePermissions.map(
          (item, index) => {
            return (
              <View style={styles.card} key={index}>
                <View style={styles.avatar}>
                  <Avatar
                    medium
                    rounded
                    source={{
                      uri: item.candidate.cv.avatar
                    }}
                    activeOpacity={0.5}
                  />
                </View>
                <View style={styles.name}>
                  <Text style={styles.username}>
                    {item.candidate.cv.username}
                  </Text>
                </View>

                <ScrollableTabView>
                  <View tabLabel="basic">
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.age}
                        </Text>
                      </Badge>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                        wrapperStyle={{
                          marginLeft: 5
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.height}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.education}
                        </Text>
                      </Badge>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                        wrapperStyle={{
                          marginLeft: 5
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.profession}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.homeDistrict}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.buttonContainer}>
                      <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                          this._onFemaleMatchPress(
                            item.candidate._id,
                            navigation.state.params.id
                          )
                        }}
                      >
                        <Icon
                          name="heart"
                          type="font-awesome"
                          color="#ffffff"
                          size={18}
                        />
                        <Text
                          style={{
                            color: 'white',
                            marginLeft: 5,
                            fontWeight: '600'
                          }}
                        >
                          Match
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View tabLabel="religion">
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.beard}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                        wrapperStyle={{
                          marginLeft: 5
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {
                            item.candidate.cv
                              .practisingSince
                          }
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.prayer}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.hifzLevel}
                        </Text>
                      </Badge>
                    </View>
                  </View>

                  <View tabLabel="About Me">
                    <Text style={{ textAlign: 'left' }}>
                      {item.candidate.cv.aboutMe}
                    </Text>
                  </View>
                </ScrollableTabView>
              </View>
            )
          }
        )}
      </ScrollView>
    )
  }

  _renderApprovedFemales = () => {
    const { maleCandidates, navigation } = this.props
    return (
      <ScrollView horizontal={true}>
        {maleCandidates.teamMalePermissions.map(
          (item, index) => {
            return (
              <View style={styles.card} key={index}>
                <View style={styles.avatar}>
                  <Avatar
                    medium
                    rounded
                    source={{
                      uri: item.candidate.cv.avatar
                    }}
                    activeOpacity={0.5}
                  />
                </View>
                <View style={styles.name}>
                  <Text style={styles.username}>
                    {item.candidate.cv.username}
                  </Text>
                </View>

                <ScrollableTabView>
                  <View tabLabel="basic">
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.age}
                        </Text>
                      </Badge>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                        wrapperStyle={{
                          marginLeft: 5
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.height}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.education}
                        </Text>
                      </Badge>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                        wrapperStyle={{
                          marginLeft: 5
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.profession}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.homeDistrict}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.buttonContainer}>
                      <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                          this._onMaleMatchPress(
                            item.candidate._id,
                            navigation.state.params.id
                          )
                        }}
                      >
                        <Icon
                          name="heart"
                          type="font-awesome"
                          color="#ffffff"
                          size={18}
                        />
                        <Text
                          style={{
                            color: 'white',
                            marginLeft: 5,
                            fontWeight: '600'
                          }}
                        >
                          Match
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View tabLabel="religion">
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.beard}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                        wrapperStyle={{
                          marginLeft: 5
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {
                            item.candidate.cv
                              .practisingSince
                          }
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.prayer}
                        </Text>
                      </Badge>
                    </View>
                    <View style={styles.badgeRow}>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F'
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.candidate.cv.hifzLevel}
                        </Text>
                      </Badge>
                    </View>
                  </View>

                  <View tabLabel="About Me">
                    <Text style={{ textAlign: 'left' }}>
                      {item.candidate.cv.aboutMe}
                    </Text>
                  </View>
                </ScrollableTabView>
              </View>
            )
          }
        )}
      </ScrollView>
    )
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.props.data.refetch()
    this.setState({ refreshing: false })
  }

  render() {
    const {
      data,
      maleCandidates,
      femaleCandidates,
      navigation,
      team
    } = this.props

    if (
      data.loading ||
      maleCandidates.loading ||
      femaleCandidates.loading ||
      team.loading
    ) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      )
    }

    const {
      age,
      pic,
      profession,
      education,
      homeDistrict,
      currentLocation,
      beard,
      height,
      hifzLevel,
      aboutMe,
      prayer,
      practisingSince,
      avatar,
      photoApproved,
      username,
      gender
    } = data.getOneCv

    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          {this.renderAvatar()}

          <View style={styles.badgeContainer}>
            <View style={styles.badgeRow}>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
              >
                <Text style={{ color: 'white' }}>
                  {age}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {profession}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {education}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {homeDistrict}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {height}
                </Text>
              </Badge>
            </View>

            <View style={styles.badgeRow}>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {beard}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {practisingSince}
                </Text>
              </Badge>
            </View>
            <View style={styles.badgeRow}>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {hifzLevel}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {prayer}
                </Text>
              </Badge>
            </View>
          </View>
          {this.renderIconContainer()}

          {navigation.state.params.approved
            ? gender === 'male'
              ? this._renderApprovedMales()
              : this._renderApprovedFemales()
            : null}
        </ScrollView>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    )
  }
}

export default compose(
  connect(
    state => ({
      userId: state.user.userId,
      chatList: state.chat.list,
      gender: state.user.gender
    }),
    { insertChatId }
  ),
  graphql(getOneCv_QUERY, {
    options: ownProps => {
      return {
        variables: {
          _id: ownProps.navigation.state.params.id
        }
      }
    }
  }),
  graphql(teamFemalePermissions_QUERY, {
    name: 'femaleCandidates'
  }),
  graphql(teamMalePermissions_QUERY, {
    name: 'maleCandidates'
  }),
  graphql(CREATECHATROOM_MUTATION, {
    name: 'createChatRoom'
  }),
  graphql(GIVE_PERMISSION_MUTATION, {
    name: 'givePermission'
  }),
  graphql(TAKE_PERMISSION_MUTATION, {
    name: 'takePermission'
  }),
  graphql(CREATE_MATCH_MUTATION, {
    name: 'match'
  }),
  graphql(myTeam_QUERY, {
    name: 'team'
  })
)(ComparisonScreen)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  photoContainer: {
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#356490'
  },
  text: {
    color: 'black',
    marginLeft: 10,
    textAlign: 'justify'
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  iconsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },
  badgeContainer: {
    marginBottom: 10,
    marginTop: 5
  },
  badgeRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 5
  },
  locationRow: {
    flexDirection: 'row'
  },
  card: {
    backgroundColor: '#E3D5FC',
    width: width(65),
    height: height(42),
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 5,
    marginTop: 5,
    borderRadius: 10
  },
  avatar: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10
  },
  name: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  username: {
    fontWeight: '700',
    fontSize: 16,
    color: '#231647'
  },
  button: {
    marginTop: 8,
    backgroundColor: '#ff1313',
    width: width(30),
    height: height(6),
    borderRadius: 18,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  }
})
