import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
  Image,
  TextInput,
  Platform,
  AsyncStorage,
} from 'react-native';
import {
  Icon,
  Header,
  Avatar,
  Button,
} from 'react-native-elements';
import { width, height } from 'react-native-dimension';
import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';

import team_QUERY from '../graphql/query/team.query';
import updateMatchMaker_MUTATION from '../graphql/mutation/updateMatchMaker.mutation';

import { completedCv } from '../../actions/user.action';

import SearchBar from 'react-native-searchbar';
import LeftArrow from '../../components/LeftArrow';

import Question from './Question.Screen';
import Form from './FormOnly.Screen';

class TeamChoosing extends Component {
  state = {
    firstLoad: false,
    loading: false,
    results: ['your search results will appear here'],
    showForm: false,
    team: '',
    partnerName: '',
    nid: '',
    uploadedPic1: '',
    pic1: '',
    username: '',
    nidCopy: '',
    pics: [],
  };

  _handleResults = async results => {
    await this.setState({ result: results });
  };

  _onPhotoPress = async (pic, uploaded) => {
    console.log(pic);
  };

  _onChangeText = async (text, type, nameOfState) => {
    await this.setState({
      [type]: text,
      [nameOfState]: true,
    });
  };

  _onSubmitPress = async () => {
    const {
      uploadedPic1,
      pic1,
      username,
      pics,
      nidCopy,
      avatar,
      team,
    } = this.state;

    this.setState({ loading: true });

    const { data } = await this.props
      .mutate({
        variables: {
          team,
          pic: pic1,
          username,
        },
      })
      .catch(e => {
        this.setState({ loading: false });
        console.log(e);
      });
    if (data.updateTeam.team != null) {
      console.log(data);
      await this.props
        .mutate({
          variables: {
            age: 60,
            height: 'x',
            education: 'x',
            prayer: 'x',
            practisingSince: 'x',
            beard: 'x',
            homeDistrict: 'x',
            currentLocation: 'x',
            hifzLevel: 'x',
            profession: 'x',
            username,
            avatar:
              this.props.gender === 'female'
                ? 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/mm_ods1ne.png'
                : 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/fm_ypnv4d.png',
            pic: pics,
            aboutMe: 'x',
            nid: nidCopy,
          },
        })
        .catch(err => {
          console.log(`${err}`);
        });
      await AsyncStorage.setItem('@cv:cv', 'true');
      await this.props.completedCv();
      this.setState({ loading: false });
    }
    await this.setState({ loading: false });
  };

  _renderList = () => {
    const { teamQuery, gender } = this.props;
    const { results, showForm } = this.state;
    return (
      <View style={styles.seceondView}>
        <SearchBar
          ref={ref => (this.searchBar = ref)}
          data={teamQuery.getUnfinishedTeams}
          handleResults={results => {
            this.setState({ results });
          }}
          showOnLoad
          hideBack
        />

        <View style={{ marginTop: height(13) }} />
        <ScrollView>
          {results.map((item, index, gender) => {
            return (
              <View
                key={index}
                style={styles.avatarContainer}
              >
                <Avatar
                  large
                  rounded
                  source={{
                    uri:
                      gender === 'male'
                        ? item.sistersAvatar
                        : item.brothersAvatar,
                  }}
                  activeOpacity={0.5}
                />
                <View style={{ justifyContent: 'center' }}>
                  <Text style={styles.name}>
                    {gender === 'male'
                      ? item.sistersUsername
                      : item.brothersUsername}
                  </Text>
                  <Text style={styles.bigText}>
                    {item.team}
                  </Text>
                </View>

                <TouchableOpacity
                  style={styles.tickCircle}
                  onPress={() => {
                    this.setState({
                      showForm: true,
                      team: item.team,
                      partnerName:
                        gender === 'male'
                          ? item.sistersUsername
                          : item.brothersUsername,
                    });
                  }}
                >
                  <Icon
                    name="check"
                    type="entypo"
                    color="white"
                  />
                </TouchableOpacity>
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  };

  _renderForm = () => {
    const { gender } = this.props;
    const {
      nid,
      uploadedPic1,
      pic1,
      team,
      username,
      showForm,
      firstLoad,
      partnerName,
    } = this.state;

    return (
      <View
        style={{
          alignItems: 'center',
          marginTop: height(5),
        }}
      >
        <View style={{ marginTop: height(10) }} />
        <View style={styles.iconContainer}>
          <TouchableOpacity
            onPress={(pic, uploadedPic) =>
              this._onPhotoPress('pic1', 'uploadedPic1')
            }
          >
            <Image
              source={
                uploadedPic1
                  ? { uri: pic1 }
                  : require('../../../assets/icons/photo1.png')
              }
              style={styles.icon}
            />
          </TouchableOpacity>
        </View>
        <TextInput
          style={styles.form}
          placeholder="username"
          value={username}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          onChangeText={text =>
            this._onChangeText(text, 'username')
          }
        />

        <TextInput
          style={styles.form}
          placeholder="4 digit Team Number"
          value={team}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          keyboardType="numeric"
          maxLength={4}
          onChangeText={text =>
            this._onChangeText(text, 'team')
          }
        />
        <Text style={styles.text}>
          {' '}
          your team Number is Very important. Pass this team
          number to your wife/husband when they sign up.
          This is how your other half will find you.If you
          mistake this, your matches won't work properly
        </Text>
        <Button
          buttonStyle={styles.button}
          title="Submit"
          titleStyle={{ color: 'black' }}
          onPress={this._onSubmitPress}
          disabled={
            username != '' &&
            team != '' &&
            uploadedPic1 === true
              ? false
              : true
          }
        />

        <View style={styles.avatarContainer}>
          <Avatar
            large
            rounded
            source={{
              uri:
                gender === 'male'
                  ? 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/mm_ods1ne.png'
                  : 'https://res.cloudinary.com/kaiyes/image/upload/v1524390247/fm_ypnv4d.png',
            }}
            activeOpacity={0.5}
          />

          <Text style={styles.bigText}>{partnerName}</Text>

          <Text style={styles.bigText}>{team}</Text>
        </View>
      </View>
    );
  };

  render() {
    const { teamQuery, gender } = this.props;
    const {
      loading,
      firstLoad,
      results,
      showForm,
    } = this.state;

    if (loading || teamQuery.loading) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      );
    }
    if (firstLoad) {
      return <Question />;
    }
    return (
      <View style={styles.container}>
        <Header
          backgroundColor="#2479c7"
          leftComponent={
            <LeftArrow
              onPress={() => {
                this.setState({
                  showForm: false,
                });
              }}
            />
          }
          centerComponent={{
            text: 'Selection Page',
            style: styles.headerText,
          }}
        />
        {showForm ? null : (
          <View
            style={{
              marginTop: height(10),
              backgroundColor: '#62a2ec',
            }}
          />
        )}
        {showForm ? this._renderForm() : this._renderList()}
      </View>
    );
  }
}

export default compose(
  graphql(team_QUERY, {
    name: 'teamQuery',
  }),
  graphql(updateMatchMaker_MUTATION),
  connect(
    state => ({
      gender: state.user.gender,
    }),
    { completedCv }
  )
)(TeamChoosing);

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bigText: {
    color: '#404aa6',
    fontSize: 22,
    fontWeight: '700',
  },
  text: {
    color: '#404aa6',
    fontSize: 12,
    fontStyle: 'italic',
    textAlign: 'center',
    fontWeight: '400',
    width: 250,
    margin: 10,
  },
  name: {
    color: '#404aa6',
    fontSize: 12,
    fontStyle: 'italic',
    textAlign: 'center',
    fontWeight: '400',
  },
  headerText: {
    color: '#ffffff',
    fontSize: 20,
    fontWeight: '700',
  },
  avatarContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15,
    backgroundColor: '#fbc3ff',
    height: height(14),
    width: width(90),
    borderRadius: 10,
  },
  seceondView: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  tickCircle: {
    width: 60,
    height: 60,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#852c87',
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  form: {
    height: 50,
    width: 300,
    borderColor: '#62a2ec',
    borderWidth: 1,
    borderRadius: 22,
    margin: 5,
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#291328',
    borderRadius: 7,
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 20,
    width: 150,
  },
  icon: {
    width: 90,
    height: 90,
    borderRadius: 45,
  },
});
