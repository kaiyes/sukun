import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  Image,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
  Dimensions
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import { connect } from 'react-redux'
import {
  Button,
  Badge,
  Divider,
  Icon,
  Card,
  Avatar
} from 'react-native-elements'
import { width, height } from 'react-native-dimension'
import DropdownAlert from 'react-native-dropdownalert'
import _ from 'lodash'
import ScrollableTabView from 'react-native-scrollable-tab-view'

import getOneCv_QUERY from '../../graphql/queries/getOneCv.query'
import myChats_QUERY from '../../graphql/queries/getMyChats.query'
import myTeam_QUERY from '../graphql/query/myTeam.query'
import pendingMaleCandidate_QUERY from '../graphql/query/pendingMaleCandidates.query'
import pendingFemaleCandidate_QUERY from '../graphql/query/pendingFemaleCandidates.query'
import CREATECHATROOM_MUTATION from '../../graphql/mutation/createChatRoom.mutation'
import REQ_TO_MATCH_MUTATION from '../graphql/mutation/createMatchRequest.mutation'
import { insertChatId } from '../../actions/chat.action'

class MatchDetailsScreen extends Component {
  state = {
    me: false,
    refreshing: false
  }

  _onMatchPress = async () => {
    await this.props.reqToMatch({
      variables: {
        _id: this.props.navigation.state.params.id,
        team: this.props.myTeam.myTeam.team,
        gender: this.props.data.getOneCv.gender
      },
      refetchQueries: [
        { query: pendingMaleCandidate_QUERY },
        { query: pendingFemaleCandidate_QUERY }
      ]
    })
  }
  _onChatPress = async () => {
    const {
      id,
      username
    } = this.props.navigation.state.params
    const ifExists = _.find(this.props.chatList, [
      'userId',
      id
    ])
    if (ifExists === undefined) {
      const { data } = await this.props.createChatRoom({
        variables: {
          _id: id
        },
        refetchQueries: [{ query: myChats_QUERY }]
      })
      await this.props.insertChatId(
        id,
        data.createChatRoom._id
      )
      this.props.navigation.navigate('Messages', {
        conversationId: data.createChatRoom._id,
        username: username
      })
    } else {
      this.props.navigation.navigate('Messages', {
        conversationId: ifExists.chatRoomId,
        username: username
      })
    }
  }

  renderAvatar = () => {
    const {
      pic,
      currentLocation,
      avatar
    } = this.props.data.getOneCv
    return (
      <View style={styles.photoContainer}>
        <Image
          style={{
            width: 250,
            height: 200,
            marginTop: 10,
            marginLeft: 10
          }}
          source={{ uri: avatar }}
        />
        <View style={styles.locationRow}>
          <Icon
            name="dot-single"
            type="entypo"
            color="#42f125"
          />
          <Text>{currentLocation}</Text>
        </View>
      </View>
    )
  }

  renderIconContainer = () => {
    const {
      photoPermission,
      navigation,
      userId,
      gender,
      myTeam
    } = this.props

    return (
      <View style={styles.iconsContainer}>
        {gender === this.props.data.getOneCv.gender ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._onChatPress}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../../assets/icons/Chat.png')}
            />
          </TouchableOpacity>
        ) : null}

        {_.includes(
          myTeam.myTeam.permissions,
          navigation.state.params.id
        ) ? null : (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._onMatchPress}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../../assets/icons/matchPermission.png')}
            />
          </TouchableOpacity>
        )}
      </View>
    )
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.props.data.refetch()
    this.setState({ refreshing: false })
  }

  render() {
    if (
      this.props.data.loading ||
      this.props.myTeam.loading
    ) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      )
    }

    const {
      age,
      pic,
      profession,
      education,
      homeDistrict,
      currentLocation,
      beard,
      height,
      hifzLevel,
      aboutMe,
      prayer,
      gender,
      practisingSince,
      avatar,
      photoApproved,
      username
    } = this.props.data.getOneCv

    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          {this.renderAvatar()}

          {this.renderIconContainer()}

          <View style={styles.badgeContainer}>
            <View style={styles.badgeRow}>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
              >
                <Text style={{ color: 'white' }}>
                  {age}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {profession}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {education}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {homeDistrict}
                </Text>
              </Badge>
            </View>

            <View style={styles.badgeRow}>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {beard}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {practisingSince}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {height}
                </Text>
              </Badge>
            </View>
            <View style={styles.badgeRow}>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {hifzLevel}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {prayer}
                </Text>
              </Badge>
            </View>
            <Text style={styles.heading}>About Me</Text>

            <Text style={styles.aboutMe}>{aboutMe}</Text>
          </View>
        </ScrollView>

        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    )
  }
}

export default compose(
  connect(
    state => ({
      userId: state.user.userId,
      chatList: state.chat.list,
      gender: state.user.gender
    }),
    { insertChatId }
  ),
  graphql(getOneCv_QUERY, {
    options: ownProps => {
      return {
        variables: {
          _id: ownProps.navigation.state.params.id
        }
      }
    }
  }),
  graphql(CREATECHATROOM_MUTATION, {
    name: 'createChatRoom'
  }),
  graphql(REQ_TO_MATCH_MUTATION, {
    name: 'reqToMatch'
  }),
  graphql(myTeam_QUERY, {
    name: 'myTeam'
  })
)(MatchDetailsScreen)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  scrollView: {
    flex: 1,
    backgroundColor: 'white'
  },
  photoContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  sliderContainer: {
    flexDirection: 'column',
    justifyContent: 'center'
    // alignItems: 'stretch',
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#356490'
  },
  text: {
    color: 'black'
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20
  },
  iconsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  badgeContainer: {
    marginTop: 5,
    marginBottom: 10
  },
  badgeRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 5
  },
  aboutMe: {
    color: 'black',
    fontWeight: '300',
    fontSize: 14,
    fontFamily: 'Helvetica Neue',
    marginTop: 3,
    marginLeft: 5,
    marginLeft: 15,
    marginRight: 15
  },
  heading: {
    color: 'black',
    fontWeight: '500',
    fontSize: 14,
    fontFamily: 'Helvetica Neue',
    marginTop: 15,
    marginLeft: 5,
    marginLeft: 15,
    marginRight: 15
  },
  locationRow: {
    flexDirection: 'row'
  }
})
