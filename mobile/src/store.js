import { createStore, applyMiddleware } from 'redux'
import { AsyncStorage, Platform } from 'react-native'
import { composeWithDevTools } from 'redux-devtools-extension'
import ApolloClient, {
  createNetworkInterface
} from 'apollo-client'
import thunk from 'redux-thunk'
import {
  SubscriptionClient,
  addGraphQLSubscriptions
} from 'subscriptions-transport-ws'

import reducers from './reducers/index.reducer'

// const host =
//   Platform === 'ios' ? 'localhost' : '192.168.0.100'

const networkInterface = createNetworkInterface({
  uri: `https://sukun.herokuapp.com/graphql`
})

const wsClient = new SubscriptionClient(
  `ws://https://sukun.herokuapp.com/subscriptions`,
  {
    reconnect: true
  }
)

// const networkInterface = createNetworkInterface({
//   uri: `http://localhost:3000/graphql`
// })
//
// const wsClient = new SubscriptionClient(
//   `ws://http://localhost:3000/subscriptions`,
//   {
//     reconnect: true
//   }
// )

networkInterface.use([
  {
    async applyMiddleware(req, next) {
      if (!req.options.headers) {
        req.options.headers = {}
      }
      try {
        const token = await AsyncStorage.getItem(
          '@login:token'
        )
        if (token !== null) {
          req.options.headers.authorization = token || null
        }
      } catch (error) {
        throw error
      }
      next()
    }
  }
])

const networkInterfaceWithSubs = addGraphQLSubscriptions(
  networkInterface,
  wsClient
)
export const client = new ApolloClient({
  networkInterface: networkInterfaceWithSubs
})

const middlewares = [client.middleware(), thunk]

export const store = createStore(
  reducers(client),
  undefined,
  composeWithDevTools(applyMiddleware(...middlewares))
)
