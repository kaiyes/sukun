import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  ScrollView,
  FlatList,
  Dimensions,
  Image,
  Platform
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import {
  Avatar,
  Button,
  Divider
} from 'react-native-elements'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

import Header from '../components/header'
import Loader from '../components/loader'

import getMessages_QUERY from '../graphql/queries/getMessages.query'
import sendMessage_MUTATION from '../graphql/mutation/sendMessage.mutation'
import makeChatSeen_MUTATION from '../graphql/mutation/makeChatNotiSeen.mutation'
import getMsgNoti_QUERY from '../graphql/queries/getMessageNotifications.query'

class MessageScreen extends Component {
  state = {
    text: ''
  }

  async componentWillMount() {
    this.props.data.startPolling(5000)
    await this.props
      .makeChatSeen({
        variables: {
          conversationId: this.props.navigation.state.params
            .conversationId
        },
        refetchQueries: [
          {
            query: getMsgNoti_QUERY
          }
        ]
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  componentWillUnMount() {
    this.props.data.stopPolling()
  }

  _onSendMessage = async () => {
    await this.props
      .mutate({
        variables: {
          conversationId: this.props.navigation.state.params
            .conversationId,
          body: this.state.text
        },
        refetchQueries: [
          {
            query: getMessages_QUERY,
            variables: {
              conversationId: this.props.navigation.state
                .params.conversationId
            }
          }
        ]
      })
      .then(this.setState({ text: '' }))
      .catch(err => {
        console.log(`${err}`)
      })
  }

  render() {
    if (this.props.data.loading) {
      return <Loader />
    }
    return (
      <View style={styles.container}>
        <Header />
        <ScrollView
          style={styles.container}
          ref={ref => (this.scrollView = ref)}
          onContentSizeChange={(
            contentWidth,
            contentHeight
          ) =>
            this.scrollView.scrollToEnd({ animated: true })
          }
        >
          <FlatList
            data={this.props.data.getMessages}
            keyExtractor={item => item._id}
            renderItem={({ item }) => (
              <View style={styles.card}>
                <Image
                  style={styles.avatar}
                  onPress={() =>
                    this.props.navigation.navigate(
                      'Details',
                      {
                        username: `${
                          item.sender.cv.username
                        }`,
                        id: item.sender._id
                      }
                    )
                  }
                  source={
                    item.sender._id === this.props.userId
                      ? {
                          uri: item.sender.cv.pic[0]
                        }
                      : _.includes(
                          item.sender.cv.photoApproved,
                          this.props.userId
                        )
                        ? {
                            uri: item.sender.cv.pic[0]
                          }
                        : {
                            uri: item.sender.cv.avatar
                          }
                  }
                />
                <View style={styles.chatTextContainer}>
                  <Text style={styles.username}>
                    {item.sender.cv.username}
                  </Text>
                  <Text style={styles.chatBody}>
                    {item.body}
                  </Text>
                </View>
              </View>
            )}
          />
        </ScrollView>
        <TextInput
          style={styles.form}
          placeholder="write here"
          value={this.state.text}
          underlineColorAndroid="transparent"
          onChangeText={text => this.setState({ text })}
          onSubmitEditing={this._onSendMessage}
        />
      </View>
    )
  }
}

export default compose(
  connect(state => ({
    userId: state.user.userId
  })),
  graphql(getMessages_QUERY, {
    options: ownProps => {
      return {
        variables: {
          conversationId:
            ownProps.navigation.state.params.conversationId
        }
      }
    }
  }),
  graphql(sendMessage_MUTATION),
  graphql(makeChatSeen_MUTATION, { name: 'makeChatSeen' })
)(MessageScreen)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  form: {
    height: hp('7.5%'),
    borderColor: '#959595',
    borderWidth: wp('.8%'),
    margin: wp('2.6%'),
    textAlign: 'left'
  },
  card: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginHorizontal: wp('3.2%'),
    marginVertical: hp('1.5%')
  },
  chatTextContainer: {
    marginBottom: hp('.75%'),
    marginLeft: wp('4%')
  },
  chatBody: {
    fontWeight: '400',
    fontSize: hp('2.4%'),
    fontFamily: 'Avenir-Medium',
    marginBottom: hp('.2%'),
    textAlign: 'auto',
    width: wp('60%')
  },
  username: {
    fontWeight: '900',
    fontSize: hp('2.5%'),
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginBottom: hp('.2%')
  },
  avatar: {
    marginLeft: wp('2.6%'),
    width: Platform.OS === 'ios' ? wp('18%') : wp('18%'),
    height: Platform.OS === 'ios' ? hp('10%') : hp('11%')
  }
})
