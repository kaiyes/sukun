import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native'

import SignUpScreen from '../screens/SignUp.Screen'
import LogInScreen from '../screens/LogIn.Screen'

const initialState = {
  signup: false,
  login: false
}

class LandingScreen extends Component {
  state = initialState

  // componentWillMount() {
  //   this.setState(initialState)
  // }

  _onLoginPress = () => this.setState({ login: true })

  _onSignupPress = () => this.setState({ signup: true })

  // _onBackPress = () => {
  //   this.setState(initialState);
  // };

  render() {
    if (this.state.signup) {
      return <SignUpScreen />
    } else if (this.state.login) {
      return <LogInScreen />
    }

    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image
            source={require('../../assets/icons/Heart.png')}
          />
          <Text style={styles.text}> Sukun </Text>
          <Text style={styles.subTitle}>
            An Islamic MatchMaking Platform
          </Text>
        </View>
        <View>
          <TouchableOpacity onPress={this._onLoginPress}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._onSignupPress}>
            <Text style={styles.signUpText}>SignUp</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#170736'
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 150
  },
  text: {
    color: 'white',
    fontFamily: 'Helvetica Neue',
    fontSize: 64,
    fontWeight: '200'
  },
  subTitle: {
    color: 'white',
    fontFamily: 'Helvetica Neue'
  },
  loginText: {
    color: 'black',
    fontFamily: 'Helvetica Neue',
    fontSize: 22,
    padding: 16,
    textAlign: 'center',
    backgroundColor: '#f1c40f'
  },
  signUpText: {
    color: 'white',
    fontFamily: 'Helvetica Neue',
    fontSize: 22,
    padding: 16,
    textAlign: 'center',
    backgroundColor: '#2980b9'
  }
})

export default LandingScreen
