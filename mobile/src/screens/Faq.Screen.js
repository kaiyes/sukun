import React, { Component } from 'react'
import {
  Text,
  Image,
  View,
  StyleSheet,
  ScrollView
} from 'react-native'
import { Icon } from 'react-native-elements'
import {
  width,
  height,
  totalSize
} from 'react-native-dimension'

class FaqScreen extends Component {
  render() {
    return (
      <ScrollView style={styles.viewContainer}>
        <View style={styles.card1}>
          <Image
            style={styles.pic}
            source={require('../../assets/icons/why.png')}
          />
          <View style={styles.textContainer}>
            <Text style={styles.heading}>
              আপনারা কিভাবে কাজ করেন ?
            </Text>
            <Text style={styles.text}>
              সুকুন পুরোপুরি স্বয়ংক্রিয় একটি অ্যাপ/সার্ভিস।
               আমরা ফেসবুক ভিত্তিক ম্যাচমেকিং সার্ভিস নই ।
              পাত্র-পাত্রী-অভিভাবক যে কেউ অ্যাপে তাঁদের
              ইমেইল দিয়ে ফ্রিতে রেজিস্ট্রেশন করে সুকুনের
              সদস্য হতে পারবেন । এরপর নিজেরাই পাত্র-পাত্রী
              খুঁজে নেবেন আমাদের ডাটাবেস এ থাকা প্রোফাইল
              গুলো দেখে ।
            </Text>
          </View>
        </View>

        <View style={styles.card2}>
          <Image
            style={styles.pic2}
            source={require('../../assets/icons/connection.png')}
          />
          <View style={styles.textContainer}>
            <Text style={styles.heading}>
              পাত্র-পাত্রীর সাথে যোগাযোগ করার উপায় কি ?
            </Text>
            <Text style={styles.text}>
              অ্যাপ এ ম্যাসেজিং এর ব্যবস্থা আছে । সীমিত এবং
              মার্জিত টেক্সট ম্যাসেজিং এর মাধ্যমে যদি
              প্রাথমিক ভাবে পছন্দ হয় তাহলে পারিবারিক ভাবে
              আগাবেন । এছাড়া অ্যাপ এ ছবি পারমিশন সাপেক্ষে
              দেখার ব্যবস্থা আছে । অনুমতি ছাড়া ছবি দেখা যায়
              না । সকল চ্যাট এডমিন মনিটর করেন । অশালীনতার
              শাস্তি অ্যাপ থেকে ব্যান হওয়া ।
            </Text>
          </View>
        </View>

        <View style={styles.card3}>
          <Image
            style={styles.pic3}
            source={require('../../assets/icons/taka2.png')}
          />
          <View style={styles.textContainer}>
            <Text style={styles.heading}>
              আপনাদের ফি কত ?
            </Text>
            <Text style={styles.text}>
              সুকুন এ মাসিক সাবস্ক্রিপশন ফি ১০০০ টাকা। তবে
              রেজিস্ট্রেশন ফ্রি। রেজিস্ট্রেশন করে আপনারা যে
              কারো প্রোফাইল দেখতে পারবেন, তবে ম্যাসেজ করতে
              পারবেন না এবং ছবিও দেখতে পারবেন না । মাসিক
              ১০০০ টাকা দিলে এক মাসের জন্যে যে কারো সাথে
              কথা বলতে পারবেন ওঁ ছবি দেখতে চাইতে পারবেন ।
              কিন্তু অপর পক্ষ ছবি দেখার অনুমতি না দিলে বা
              মেসেজের উত্তর না দিলে সে দায়িত্ব আমাদের নয় ।
            </Text>
          </View>
        </View>

        <View style={styles.card4}>
          <View style={styles.iconContainer}>
            <Icon
              name="magnifying-glass"
              type="entypo"
              color="white"
              size={50}
            />
          </View>

          <View style={styles.textContainer}>
            <Text style={styles.heading}>
              আপনারা কি যাচাই বাছাই করে পাত্র/পাত্রী
              রেজিস্ট্রেশন করেন ?
            </Text>
            <Text style={styles.text}>
              আমরা শুধুমাত্র দাড়ি/নিকাব-বোরখা এগুলো দেখে
              রেজিস্ট্রেশনের অনুমতি দেই । এছাড়া যিনি একাউণ্ট
              খুলছেন তিনিই আসলে ঐ ব্যাক্তি কিনা তা আইডি
              কার্ড দেখে নিশ্চিত করা হয় । কিন্তু তারা
              নিজেদের সম্পর্কে যে তথ্য প্রদান করে তা সঠিক
              কিনা তা যাচাই বাছাই করা হয় না কারণ এই কস্টীং এ
              সেটি করার সুযোগ নেই । আপনারা নিজ দায়িত্বে Due
              Dilligence করে নিবেন ।
            </Text>
          </View>
        </View>
      </ScrollView>
    )
  }
}

export default FaqScreen

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: '#dff2f7'
  },
  card1: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-around'
  },
  pic: {
    height: 145,
    width: 85,
    marginTop: 25,
    marginLeft: 10
  },
  card2: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-around'
  },
  pic2: {
    width: width(20),
    marginTop: height(20),
    marginLeft: 30
  },
  card3: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  pic3: {
    height: height(12),
    width: width(23),
    marginLeft: 30,
    borderRadius: 5
  },
  card4: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  heading: {
    fontWeight: '900',
    fontSize: 16,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue'
  },
  text: {
    fontWeight: '300',
    fontSize: 14,
    color: 'black',
    fontFamily: 'Helvetica Neue',
    marginRight: 20,
    textAlign: 'justify'
  },
  textContainer: {
    alignItems: 'flex-start',
    marginTop: 10,
    marginLeft: 60,
    marginRight: 20,
    marginBottom: 15
  },
  iconContainer: {
    height: 70,
    width: 70,
    borderRadius: 35,
    backgroundColor: '#469cd4',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 30
  }
})
