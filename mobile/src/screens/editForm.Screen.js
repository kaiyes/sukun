import React, { Component } from 'react';
import ModalDropdown from 'react-native-modal-dropdown';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  ScrollView,
  RefreshControl,
} from 'react-native';
import Picker from 'react-native-picker';
import {
  Divider,
  Button,
  Icon,
} from 'react-native-elements';
import DropdownAlert from 'react-native-dropdownalert';
import { graphql, withApollo, compose } from 'react-apollo';
import { connect } from 'react-redux';
import {
  DocumentPicker,
  DocumentPickerUtil,
} from 'react-native-document-picker';
import _ from 'lodash';

import UPDATECV_MUTATION from '../graphql/mutation/updateCv.mutation';
import { completedCv } from '../actions/user.action';
import GETONECV_QUERY from '../graphql/queries/getOneCv.query';

import Header from '../components/header';
import styles from '../utils/styles/editProfile.style';
import eduList from '../utils/eduList';
import heightList from '../utils/height';
import Districts from '../utils/districts';

class EditFormScreen extends Component {
  state = {
    ageState: null,
    ageChanged: false,
    heightState: null,
    heightChanged: false,
    educationState: 'শিক্ষা',
    eduChanged: false,
    prayerState: null,
    prayerChanged: false,
    practisingSinceState: null,
    practisingChanged: false,
    beardState: null,
    bearChanged: false,
    homeDistrictState: null,
    homeChanged: false,
    currentLocationState: null,
    locationChanged: false,
    hifzLevelState: null,
    hifzChanged: false,
    professionState: 'student',
    profChanged: false,
    usernameState: null,
    usernameChanged: false,
    aboutMeState: null,
    aboutMeChanged: false,
    picState: [],
    picChanged: false,
    nidCopyState: null,
    nidCopyChanged: false,
    avatarState: null,
    avatarChanged: false,
    pic1: '',
    pic2: '',
    pic3: '',
    uploadedPic1: false,
    uploadedPic2: false,
    uploadedPic3: false,
    loading: false,
  };

  async componentDidMount() {
    await this.props.data.refetch();
    const {
      age,
      height,
      education,
      prayer,
      practisingSince,
      beard,
      homeDistrict,
      currentLocation,
      hifzLevel,
      profession,
      username,
      aboutMe,
      pic,
      nidCopy,
      avatar,
      gender,
    } = await this.props.data.getOneCv;

    await this.setState({
      ageState: age,
      heightState: height,
      educationState: education,
      prayerState: prayer,
      practisingSinceState: practisingSince,
      beardState: beard,
      homeDistrictState: homeDistrict,
      currentLocationState: currentLocation,
      hifzLevelState: hifzLevel,
      professionState: profession,
      usernameState: username,
      aboutMeState: aboutMe,
      picState: pic,
      nidCopyState: nidCopy,
      avatarState: avatar,
    });
  }
  _onChangeText = async (text, state, changeOfState) => {
    await this.setState({
      [state]: text,
      [changeOfState]: true,
    });
  };

  _onPhotoPress = async (pic, uploaded) => {
    console.log(pic);
  };

  _onEduPress = async () => {
    const data = eduList;
    Picker.init({
      pickerData: data,
      pickerTitleText: 'আপনার শিক্ষাগত যোগ্যতা',
      onPickerConfirm: async data => {
        await this.setState({
          educationState: data[0],
          eduChanged: true,
        });
      },
    });
    Picker.show();
  };

  _onHeightPress = async () => {
    let data = heightList;
    Picker.init({
      pickerData: data,
      pickerTitleText: 'আপনার উচ্চতা কত ?',
      onPickerConfirm: async data => {
        await this.setState({
          heightState: data[0],
          heightChanged: true,
        });
      },
    });
    Picker.show();
  };

  _onAgePress = async () => {
    let data = [];
    for (var i = 18; i < 60; i++) {
      data.push(i);
    }

    Picker.init({
      pickerData: data,
      pickerTitleText: 'আপনার বয়স কত ?',
      onPickerConfirm: async data => {
        await this.setState({
          ageState: data[0],
          ageChanged: true,
        });
      },
    });
    Picker.show();
  };

  _onBeardPress = async () => {
    let maleData = [
      'সুন্নাহ মোতাবেক পূর্ণ দাড়ি',
      'এক মুঠোর উপরে কেটে ফেলা',
      'দাড়ি ট্রিম করা ট্রিমার দিয়ে',
      'প্রাকৃতিক ভাবে উঠে না',
      'শেভ করা',
    ];
    Picker.init({
      pickerData: maleData,
      pickerTitleText: 'দাড়ি কি ভাবে রাখেন ?',
      onPickerConfirm: async maleData => {
        await this.setState({
          beardState: maleData[0],
          beardChanged: true,
        });
      },
    });

    Picker.show();
  };

  _onHijabPress = async () => {
    let femaleData = [
      'নিকাব-আবায়া-বোরখা',
      'আবায়া/বোরখা মুখ খোলা',
      'শুধু স্কার্ফ',
      'হিজাব ব্যতীত সাধারণ পোশাক',
    ];
    Picker.init({
      pickerData: femaleData,
      pickerTitleText: 'হিজাবের বিবরণ',
      onPickerConfirm: async femaleData => {
        await this.setState({
          beardState: femaleData[0],
          beardChanged: true,
        });
      },
    });
    Picker.show();
  };

  _onPrayerPress = async () => {
    let data = [
      '৫ ওয়াক্ত',
      'মাঝে মাঝে ফজর মিস হয়',
      'শুধু জুমুআ',
      'মাঝে মাঝে সালাহ পড়ি',
    ];

    Picker.init({
      pickerData: data,
      pickerTitleText: 'সালাহ কয় ওয়াক্ত পড়েন ?',
      onPickerConfirm: async data => {
        await this.setState({
          prayerState: data[0],
          prayerChanged: true,
        });
      },
    });
    Picker.show();
  };

  _onPractisingPress = async () => {
    let data = [
      '৫ বছর এর উপরে',
      '৩ বছর ধরে',
      '১ বছর ধরে',
      'কয়েক মাস ধরে',
    ];

    Picker.init({
      pickerData: data,
      onPickerConfirm: async data => {
        await this.setState({
          practisingSinceState: data[0],
          practisingChanged: true,
        });
      },
    });
    Picker.show();
  };

  _onCurrentLocationPress = async () => {
    let data = Districts;
    Picker.init({
      pickerData: data,
      onPickerConfirm: async data => {
        await this.setState({
          currentLocationState: data[0],
          locationChanged: true,
        });
      },
    });
    Picker.show();
  };

  _onHomeDistrictPress = async () => {
    let data = Districts;
    Picker.init({
      pickerData: data,
      onPickerConfirm: async data => {
        await this.setState({
          homeDistrictState: data[0],
          homeChanged: true,
        });
      },
    });
    Picker.show();
  };

  _onQuranPress = async () => {
    let data = memorizationList;
    Picker.init({
      pickerData: data,
      pickerTitleText: 'কতটুকু কুরআন মুখস্থ আছে ?',
      onPickerConfirm: async data => {
        await this.setState({
          hifzLevelState: data[0],
          hifzChanged: true,
        });
      },
    });
    Picker.show();
  };

  _onSubmitPress = async () => {
    const {
      ageState,
      ageChanged,
      heightState,
      heightChanged,
      educationState,
      eduChanged,
      prayerState,
      prayerChanged,
      practisingSinceState,
      practisingChanged,
      beardState,
      beardChanged,
      homeDistrictState,
      homeChanged,
      currentLocationState,
      locationChanged,
      hifzLevelState,
      hifzChanged,
      professionState,
      profChanged,
      usernameState,
      usernameChanged,
      aboutMeState,
      aboutMeChanged,
      picState,
      picChanged,
      nidCopyState,
      nidCopyChanged,
      avatarState,
      avatarChanged,
      uploadedPic1,
      uploadedPic2,
      uploadedPic3,
      pic1,
      pic2,
      pic3,
    } = this.state;

    await this.setState({ loading: true });
    await this.props
      .mutate({
        variables: {
          age: ageState,
          height: heightState,
          education: educationState,
          prayer: prayerState,
          practisingSince: practisingSinceState,
          beard: beardState,
          homeDistrict: homeDistrictState,
          currentLocation: currentLocationState,
          hifzLevel: hifzLevelState,
          profession: professionState,
          username: usernameState,
          aboutMe: aboutMeState,
          pic: picState,
          nid: nidCopyState,
          avatar: avatarState,
        },
        refetchQueries: [
          {
            query: GETONECV_QUERY,
            variables: {
              _id: this.props.userId,
            },
          },
        ],
      })
      .catch(err => {
        console.log(`${err}`);
      });

    await AsyncStorage.setItem('@cv:cv', 'true');
    await this.props.completedCv();

    await this.setState({ loading: false });
  };

  _onRefresh = async () => {
    this.setState({ loading: true });
    await this.props.data.refetch();
    this.setState({ loading: false });
  };

  render() {
    if (this.props.data.loading || this.state.loading) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      );
    }

    const {
      age,
      height,
      education,
      prayer,
      practisingSince,
      beard,
      homeDistrict,
      currentLocation,
      hifzLevel,
      profession,
      username,
      aboutMe,
      pic,
      nidCopy,
      avatar,
      gender,
    } = this.props.data.getOneCv;

    const {
      ageState,
      ageChanged,
      heightState,
      heightChanged,
      educationState,
      eduChanged,
      prayerState,
      prayerChanged,
      practisingSinceState,
      practisingChanged,
      beardState,
      beardChanged,
      homeDistrictState,
      homeChanged,
      currentLocationState,
      locationChanged,
      hifzLevelState,
      hifzChanged,
      professionState,
      profChanged,
      usernameState,
      usernameChanged,
      aboutMeState,
      aboutMeChanged,
      picState,
      picChanged,
      nidCopyState,
      nidCopyChanged,
      avatarState,
      avatarChanged,
      uploadedPic1,
      uploadedPic2,
      uploadedPic3,
      pic1,
      pic2,
      pic3,
    } = this.state;

    return (
      <View style={styles.container}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              onRefresh={this._onRefresh}
            />
          }
        >
          <View style={styles.iconContainer}>
            <TouchableOpacity
              onPress={(pic, uploadedPic) =>
                this._onPhotoPress('pic3', 'uploadedPic3')
              }
            >
              <Image
                source={
                  uploadedPic3
                    ? { uri: pic3 }
                    : pic[2]
                    ? { uri: pic[2] }
                    : require('../../assets/icons/photo3.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={(pic, uploadedPic) =>
                this._onPhotoPress('pic2', 'uploadedPic2')
              }
            >
              <Image
                source={
                  uploadedPic2
                    ? { uri: pic2 }
                    : pic[1]
                    ? { uri: pic[1] }
                    : require('../../assets/icons/photo2.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={(pic, uploadedPic) =>
                this._onPhotoPress('pic1', 'uploadedPic1')
              }
            >
              <Image
                source={
                  uploadedPic1
                    ? { uri: pic1 }
                    : { uri: pic[0] }
                }
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>ইউজারনেম</Text>
            <TextInput
              value={
                usernameChanged ? usernameState : username
              }
              textAlign={'center'}
              style={styles.form}
              underlineColorAndroid="transparent"
              onChangeText={text =>
                this._onChangeText(
                  text,
                  'usernameState',
                  'usernameChanged'
                )
              }
            />
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>পেশা</Text>
            <TextInput
              value={
                profChanged ? professionState : profession
              }
              textAlign={'center'}
              underlineColorAndroid="transparent"
              style={styles.form}
              onChangeText={text =>
                this._onChangeText(
                  text,
                  'professionState',
                  'profChanged'
                )
              }
            />
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>বয়স</Text>
            <Button
              title={ageChanged ? ageState : age}
              buttonStyle={styles.button}
              iconRight
              onPress={this._onAgePress}
            />
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>শিক্ষা</Text>
            <Button
              title={
                eduChanged ? educationState : education
              }
              buttonStyle={styles.button}
              iconRight
              onPress={this._onEduPress}
            />
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>উচ্চতা</Text>
            <Button
              title={heightChanged ? heightState : height}
              buttonStyle={styles.button}
              iconRight
              onPress={this._onHeightPress}
            />
          </View>
          {gender === 'male' ? (
            <View style={styles.formContainer2}>
              <Text style={styles.captionText}>দাড়ি</Text>
              <Button
                title={beardChanged ? beardState : beard}
                buttonStyle={styles.button}
                iconRight
                onPress={this._onBeardPress}
              />
            </View>
          ) : (
            <View style={styles.formContainer2}>
              <Text style={styles.captionText}>হিজাব</Text>
              <Button
                title={beardChanged ? beardState : beard}
                buttonStyle={styles.button}
                iconRight
                onPress={this._onHijabPress}
              />
            </View>
          )}
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>সালাত</Text>
            <Button
              title={prayerChanged ? prayerState : prayer}
              buttonStyle={styles.button}
              iconRight
              onPress={this._onPrayerPress}
            />
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>
              প্র্যাকটিস {'\n'} এর সময়{' '}
            </Text>
            <Button
              title={
                practisingChanged
                  ? practisingSinceState
                  : practisingSince
              }
              buttonStyle={styles.button}
              iconRight
              onPress={this._onPractisingPress}
            />
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>
              দেশের {'\n'} বাড়ী
            </Text>
            <Button
              title={
                homeChanged
                  ? homeDistrictState
                  : homeDistrict
              }
              buttonStyle={styles.button}
              iconRight
              onPress={this._onHomeDistrictPress}
            />
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>
              বর্তমান {'\n'} ঠিকানা
            </Text>
            <Button
              title={
                locationChanged
                  ? currentLocationState
                  : currentLocation
              }
              buttonStyle={styles.button}
              iconRight
              onPress={this._onCurrentLocationPress}
            />
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>
              কুরআন {'\n'} যেটুকু {'\n'} মুখস্থ আছে
            </Text>
            <Button
              title={
                hifzChanged ? hifzLevelState : hifzLevel
              }
              buttonStyle={styles.button}
              iconRight
              onPress={this._onQuranPress}
            />
          </View>
          <View style={styles.formContainer2}>
            <Text style={styles.captionText}>
              আপনার কথা
            </Text>
            <TextInput
              multiline
              maxLength={2400}
              style={styles.textInput}
              underlineColorAndroid="transparent"
              value={
                aboutMeChanged ? aboutMeState : aboutMe
              }
              onChangeText={text =>
                this._onChangeText(
                  text,
                  'aboutMeState',
                  'aboutMeChanged'
                )
              }
            />
          </View>
        </ScrollView>
        <TouchableOpacity onPress={this._onSubmitPress}>
          <View style={styles.submitButton}>
            <Text style={styles.buttonText}>Submit</Text>
          </View>
        </TouchableOpacity>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    );
  }
}

// ************************************************
//                     Export                     *
// ************************************************

export default compose(
  connect(
    state => ({
      userId: state.user.userId,
    }),
    { completedCv }
  ),
  graphql(GETONECV_QUERY, {
    options: ownProps => {
      return {
        variables: {
          _id: ownProps.userId,
        },
      };
    },
  }),
  graphql(UPDATECV_MUTATION)
)(EditFormScreen);
