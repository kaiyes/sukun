import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import { connect } from 'react-redux'
import {
  Avatar,
  Icon,
  Divider,
  Button,
  Badge
} from 'react-native-elements'
import DropdownAlert from 'react-native-dropdownalert'
import _ from 'lodash'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import GridView from 'react-native-super-grid'
import { width, height } from 'react-native-dimension'

import myReqs_QUERY from '../graphql/queries/getMyphotReqs.query'
import list_QUERY from '../graphql/queries/list.query'
import photoNotifications_QUERY from '../graphql/queries/getPhotoNotification.query'
import whoCanSeeMe_QUERY from '../graphql/queries/whoCanSeeMe.query'
import myFavs_QUERY from '../graphql/queries/myFavs.query'
import user_QUERY from '../graphql/queries/currentUser.query'

import ACCEPT_MUTATION from '../graphql/mutation/acceptPhotoReq.mutation'
import DELETE_MUTATION from '../graphql/mutation/deletePhotReq.mutation'
import MAKE_SEEN_MUTATION from '../graphql/mutation/makePhotoNotiSeen.mutation'

import { subbed, unSubbed } from '../actions/payment.action'
import Header from '../components/header'

class PhotoReqScreen extends Component {
  componentWillMount() {
    this.props.navigation.addListener('didFocus', () => {
      this.props.canSeeMe.refetch()
      this.props.myReqs.refetch()
      this.props
        .makeSeen({
          refetchQueries: [
            { query: photoNotifications_QUERY }
          ]
        })
        .catch(err => {
          console.log(`${err}`)
        })
    })
  }
  _onPaymentPress = () => {
    this.props.navigation.navigate('Payment')
  }

  _refresh = async () => {
    let { data } = await this.props.userQuery.refetch()
    if (data.userInfo.hasPaid) {
      await this.props.dispatch(subbed())
    } else {
      await this.props.dispatch(unSubbed())
    }
  }

  render() {
    if (
      this.props.myReqs.loading ||
      this.props.canSeeMe.loading ||
      this.props.myFavs.loading
    ) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      )
    }
    return (
      <View style={styles.container}>
        <ScrollableTabView>
          <ScrollView tabLabel="Requests">
            {this.props.myReqs.getMyPhotoReqs.map(
              (item, index) => {
                return (
                  <View
                    key={index}
                    style={styles.avatarContainer}
                  >
                    <Avatar
                      medium
                      rounded
                      onPress={() =>
                        this.props.navigation.navigate(
                          'Details',
                          {
                            username: `${
                              item.requester.cv.username
                            }`,
                            id: item.requester._id
                          }
                        )
                      }
                      source={
                        _.includes(
                          item.requester.cv.photoApproved,
                          this.props.userId
                        )
                          ? {
                              uri: item.requester.cv.pic[0]
                            }
                          : {
                              uri: item.requester.cv.avatar
                            }
                      }
                      activeOpacity={0.5}
                    />

                    <Text
                      style={{
                        fontWeight: 'normal',
                        fontFamily: 'Helvetica Neue',
                        fontSize: 15
                      }}
                    >
                      {item.requester.cv.username} wants to{
                        '\n'
                      }
                      view your photo
                    </Text>
                    <View style={styles.circleContainer}>
                      <TouchableOpacity
                        style={styles.tickCircle}
                        onPress={async () => {
                          await this.props
                            .acceptPhotoReq({
                              variables: {
                                _id: item._id
                              },
                              refetchQueries: [
                                { query: myReqs_QUERY },
                                { query: list_QUERY },
                                { query: whoCanSeeMe_QUERY }
                              ]
                            })
                            .catch(err => {
                              console.log(`${err}`)
                            })
                          this.dropdown.alertWithType(
                            'success',
                            'Success',
                            'Photo Request Accepted'
                          )
                        }}
                      >
                        <Icon
                          name="check"
                          type="entypo"
                          color="white"
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.crossCircle}
                        onPress={async () => {
                          await this.props
                            .deletePhotoReq({
                              variables: {
                                _id: item._id
                              },
                              refetchQueries: [
                                { query: myReqs_QUERY }
                              ]
                            })
                            .catch(err => {
                              console.log(`${err}`)
                            })
                          this.dropdown.alertWithType(
                            'error',
                            'Error',
                            'Photo Request deleted'
                          )
                        }}
                      >
                        <Icon
                          name="cross"
                          type="entypo"
                          color="white"
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                )
              }
            )}
          </ScrollView>

          <ScrollView
            tabLabel="Requests"
            style={styles.scrollContainer}
            tabLabel="Permissions"
          >
            {this.props.canSeeMe.whoCanSeeMe.map(
              (item, index) => {
                return (
                  <View
                    key={index}
                    style={styles.avatarContainer}
                  >
                    <Avatar
                      medium
                      rounded
                      onPress={() =>
                        this.props.navigation.navigate(
                          'Details',
                          {
                            username: `${
                              item.requester._id ===
                              this.props.userId
                                ? item.receiver.cv.username
                                : item.requester.cv.username
                            }`,
                            id:
                              item.requester._id ===
                              this.props.userId
                                ? item.receiver._id
                                : item.requester._id
                          }
                        )
                      }
                      source={
                        item.requester._id ===
                        this.props.userId
                          ? _.includes(
                              item.receiver.cv
                                .photoApproved,
                              this.props.userId
                            )
                            ? {
                                uri: item.receiver.cv.pic[0]
                              }
                            : require('../../assets/icons/sister.png')
                          : _.includes(
                              item.requester.cv
                                .photoApproved,
                              this.props.userId
                            )
                            ? {
                                uri:
                                  item.requester.cv.pic[0]
                              }
                            : require('../../assets/icons/sister.png')
                      }
                      activeOpacity={0.5}
                    />

                    <Text
                      style={{
                        fontWeight: 'normal',
                        fontFamily: 'Helvetica Neue',
                        fontSize: 15
                      }}
                    >
                      {item.requester._id ===
                      this.props.userId
                        ? item.receiver.cv.username
                        : item.requester.cv.username}{' '}
                      gave you {'\n'}
                      photo permission
                    </Text>
                    <View style={styles.circleContainer}>
                      <TouchableOpacity
                        style={styles.crossCircle}
                      >
                        <Icon
                          name="cross"
                          type="entypo"
                          color="white"
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                )
              }
            )}
          </ScrollView>

          {/* <ScrollView > */}
          <GridView
            tabLabel="Favourites"
            itemDimension={130}
            items={this.props.myFavs.getMyFavs}
            style={styles.scrollContainer}
            renderItem={item => (
              <TouchableOpacity
                style={styles.card}
                // key={index}
                onPress={() =>
                  this.props.navigation.navigate(
                    'Details',
                    {
                      username: `${
                        item.likeReceiver.cv.username
                      }`,
                      id: item.likeReceiver._id
                    }
                  )
                }
              >
                <Avatar
                  medium
                  rounded
                  source={
                    _.includes(
                      item.likeReceiver.cv.photoApproved,
                      this.props.userId
                    )
                      ? {
                          uri: item.likeReceiver.cv.pic[0]
                        }
                      : require('../../assets/icons/sister.png')
                  }
                  activeOpacity={0.5}
                  containerStyle={{ marginTop: 10 }}
                />
                <Text style={styles.heading}>
                  {item.likeReceiver.cv.username}
                </Text>
                <Text style={styles.subtitle}>
                  {item.likeReceiver.cv.profession}
                </Text>
                <View style={styles.badgeContainer}>
                  <Badge
                    containerStyle={{
                      backgroundColor: '#5D4A8F'
                    }}
                  >
                    <Text style={{ color: 'white' }}>
                      {item.likeReceiver.cv.age}
                    </Text>
                  </Badge>
                  <Badge
                    containerStyle={{
                      backgroundColor: '#5D4A8F'
                    }}
                  >
                    <Text style={{ color: 'white' }}>
                      {item.likeReceiver.cv.education}
                    </Text>
                  </Badge>
                </View>
              </TouchableOpacity>
            )}
          />
          {/* </ScrollView> */}
        </ScrollableTabView>
        {this.props.hasPaid ? null : (
          <View style={styles.absolute}>
            <Text style={styles.blurredText}>
              For subscribers only
            </Text>
            <View style={{ marginTop: 10 }} />

            <Button
              backgroundColor="#314e79"
              rounded={true}
              title="Start Asking Photo permissions"
              onPress={this._onPaymentPress}
            />
            <Button
              backgroundColor="#314e79"
              title="Check if your payment has gone through"
              rounded={true}
              buttonStyle={{ marginTop: height(2) }}
              onPress={this._refresh}
            />
          </View>
        )}
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={1500}
        />
      </View>
    )
  }
}

export default compose(
  connect(state => ({
    userId: state.user.userId,
    hasPaid: state.payment.hasPaid
  })),
  graphql(ACCEPT_MUTATION, { name: 'acceptPhotoReq' }),
  graphql(DELETE_MUTATION, { name: 'deletePhotoReq' }),
  graphql(MAKE_SEEN_MUTATION, { name: 'makeSeen' }),
  graphql(user_QUERY, {
    name: 'userQuery'
  }),
  graphql(myReqs_QUERY, { name: 'myReqs' }),
  graphql(myFavs_QUERY, { name: 'myFavs' }),
  graphql(whoCanSeeMe_QUERY, { name: 'canSeeMe' })
)(PhotoReqScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e0f3f6'
  },
  scrollContainer: {
    flex: 1,
    paddingTop: 10
  },
  textInvisible: {
    color: '#ecf0f1'
  },
  avatarContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15
  },
  textContainer: {
    flex: 1,
    backgroundColor: '#ecf0f1'
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#356490'
  },
  tickCircle: {
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#25ba22'
  },
  crossCircle: {
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#691212',
    marginLeft: 5
  },
  circleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(211, 233, 240, 0.78)'
  },
  blurredText: {
    color: '#314e79',
    fontSize: 26,
    fontWeight: '700'
  },
  card: {
    height: 150,
    width: 150,
    backgroundColor: 'white',
    alignItems: 'center',
    marginLeft: 10,
    borderRadius: 5
  },
  heading: {
    fontWeight: '700',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginTop: 5
  },
  subtitle: {
    fontWeight: '300',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue'
  },
  badgeContainer: {
    flexDirection: 'row',
    marginTop: 5
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150
  }
})
