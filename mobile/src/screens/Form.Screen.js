import React, { Component } from 'react';
import ModalDropdown from 'react-native-modal-dropdown';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  ScrollView,
  RefreshControl,
} from 'react-native';
import Picker from 'react-native-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Select, Option } from 'react-native-select-list';
import { Divider } from 'react-native-elements';
import DropdownAlert from 'react-native-dropdownalert';
import { graphql, withApollo, compose } from 'react-apollo';
import { connect } from 'react-redux';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {
  DocumentPicker,
  DocumentPickerUtil,
} from 'react-native-document-picker';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {
  Makiko,
  Kaede,
  Fumi,
} from 'react-native-textinput-effects';
import _ from 'lodash';

import UPDATECV_MUTATION from '../graphql/mutation/updateCv.mutation';
import { completedCv } from '../actions/user.action';
import GETONECV_QUERY from '../graphql/queries/getOneCv.query';

import Header from '../components/header';
import Districts from '../utils/districts';
import heightList from '../utils/height';
import eduSwitchFunction from '../utils/education';
import beardSwitchFunction from '../utils/beard';
import hijabSwitchFunction from '../utils/hijab';
import deenFunction from '../utils/deen';
import prayerSwitchFunction from '../utils/prayer';
import femalePrayerSwitchFunction from '../utils/femalePrayer';
import femaleAgeSwitchFunc from '../utils/femaleAge';
import ageSwitchFunction from '../utils/age';
import styles from '../utils/styles/form.style';
import stateObject from '../utils/stateObject';

class FormScreen extends Component {
  state = stateObject;

  async componentDidMount() {
    await this.props.data.refetch();
    const {
      age,
      height,
      education,
      prayer,
      practisingSince,
      beard,
      homeDistrict,
      currentLocation,
      hifzLevel,
      profession,
      username,
      aboutMe,
      pic,
      nidCopy,
      avatar,
      gender,
    } = await this.props.data.getOneCv;

    await this.setState({
      education,
      eduPic: eduSwitchFunction(education),
      beard,
      beardPic: beardSwitchFunction(beard),
      hijabPic: hijabSwitchFunction(beard),
      practisingSince,
      deenPic: deenFunction(practisingSince),
      prayer,
      prayerPic: prayerSwitchFunction(prayer),
      femalePrayerPic: femalePrayerSwitchFunction(prayer),
      age,
      agePic: ageSwitchFunction(age),
      agePicFemale: femaleAgeSwitchFunc(age),
      hifzLevel,
      currentLocation,
      homeDistrict,
      height,
      pic1:
        pic.length > 0
          ? { uri: pic[0] }
          : require('../../assets/icons/photo1.png'),
      pic2:
        pic.length > 1
          ? { uri: pic[1] }
          : require('../../assets/icons/photo2.png'),
      pic3:
        pic.length > 2
          ? { uri: pic[2] }
          : require('../../assets/icons/photo3.png'),
      pic4:
        pic.length > 3
          ? { uri: pic[3] }
          : require('../../assets/icons/photo4.png'),
      username,
      profession,
      aboutMe,
      avatar,
    });
  }

  _onPhotoPress = async (pic, uploaded) => {
    console.log(pic);
  };

  _onCvPress = async () => {
    // try {
    //   const pdf = await DocumentPicker.show({
    //     filetype: [DocumentPickerUtil.pdf()]
    //   })
    // } catch (e) {
    //   console.log(e)
    // }
    // for (var i = 0; i < 100; i++) {
    //   data.push(i)
    // }
    console.log(this);
  };

  _onQuranPress = async () => {
    let data = [
      'অল্প কিছু সুরা',
      'জুজ আম্মা',
      '২-৫ পারা',
      '১০ পারা',
      'অর্ধেক কুরআন',
      '২০ পারা',
      'হাফেজ',
      'হাফেজা',
    ];

    Picker.init({
      pickerData: data,
      selectedValue: ['অল্প কিছু সুরা'],
      pickerTitleText: 'কতটুকু কুরআন মুখস্থ আছে ?',
      onPickerConfirm: async data => {
        await this.setState({
          hifzLevel: data[0],
          hifzSelected: true,
          quranPic: require('../../assets/icons/accept.png'),
        });
      },
    });
    Picker.show();
  };

  _onAgePress = async () => {
    let data = [];
    for (var i = 18; i < 60; i++) {
      data.push(i);
    }

    Picker.init({
      pickerData: data,
      selectedValue: ['25'],
      pickerTitleText: 'আপনার বয়স কত ?',
      onPickerConfirm: async data => {
        await this.setState({
          age: data[0],
          ageSelected: true,
        });
        if (this.props.data.getOneCv.gender === 'male') {
          this.setState({
            agePic: require('../../assets/icons/accept.png'),
          });
        } else if (this.props.gender === 'female') {
          this.setState({
            agePicFemale: require('../../assets/icons/accept.png'),
          });
        }
      },
      onPickerSelect: async data => {
        let clientAge = data[0];
        if (this.props.data.getOneCv.gender === 'male') {
          if (clientAge < 38) {
            this.setState({
              agePic: require('../../assets/icons/young.png'),
            });
          } else if (clientAge > 39 && clientAge < 60) {
            this.setState({
              agePic: require('../../assets/icons/old.png'),
            });
          }
        } else if (
          this.props.data.getOneCv.gender === 'female'
        ) {
          if (clientAge < 38) {
            this.setState({
              agePicFemale: require('../../assets/icons/youngGirl.png'),
            });
          } else if (clientAge > 39 && clientAge < 60) {
            this.setState({
              agePicFemale: require('../../assets/icons/agedGirl.png'),
            });
          }
        }
      },
    });
    Picker.show();
  };

  _onHeightPress = async () => {
    let data = heightList;
    Picker.init({
      pickerData: data,
      selectedValue: ['৫ ফিট'],
      pickerTitleText: 'আপনার উচ্চতা কত ?',
      onPickerConfirm: async data => {
        await this.setState({
          height: data[0],
          heightSelected: true,
        });
        if (this.props.data.getOneCv.gender === 'male') {
          this.setState({
            heightPic: require('../../assets/icons/accept.png'),
          });
        } else if (
          this.props.data.getOneCv.gender === 'female'
        ) {
          this.setState({
            heightPicFemale: require('../../assets/icons/accept.png'),
          });
        }
      },
      onPickerSelect: async data => {
        let clientHeight = data[0];
        if (clientHeight == 'তালগাছ') {
          this.setState({
            heightPic: require('../../assets/icons/heightTree.png'),
          });
        }
      },
    });
    Picker.show();
  };

  _onBeardPress = async () => {
    let maleData = [
      'সুন্নাহ মোতাবেক পূর্ণ দাড়ি',
      'এক মুঠোর উপরে কেটে ফেলা',
      'দাড়ি ট্রিম করা ট্রিমার দিয়ে',
      'প্রাকৃতিক ভাবে উঠে না',
      'শেভ করা',
    ];
    Picker.init({
      pickerData: maleData,
      selectedValue: ['সুন্নাহ মোতাবেক পূর্ণ দাড়ি'],
      pickerTitleText: 'দাড়ি কি ভাবে রাখেন ?',
      onPickerConfirm: async maleData => {
        await this.setState({
          beard: maleData[0],
          beardSelected: true,
          beardPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async maleData => {
        this.setState({
          beardPic: beardSwitchFunction(maleData[0]),
        });
      },
    });

    Picker.show();
  };
  _onHijabPress = async () => {
    let femaleData = [
      'নিকাব-আবায়া-বোরখা',
      'আবায়া/বোরখা মুখ খোলা',
      'শুধু স্কার্ফ',
      'হিজাব ব্যতীত সাধারণ পোশাক',
    ];
    Picker.init({
      pickerData: femaleData,
      selectedValue: ['আবায়া/বোরখা মুখ খোলা'],
      pickerTitleText: 'হিজাবের বিবরণ',
      onPickerConfirm: async femaleData => {
        await this.setState({
          beard: femaleData[0],
          beardSelected: true,
          hijabPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async femaleData => {
        this.setState({
          hijabPic: hijabSwitchFunction(femaleData[0]),
        });
      },
    });

    Picker.show();
  };

  _onPrayerPress = async () => {
    let data = [
      '৫ ওয়াক্ত সালাহ পড়ি',
      'মাঝে মাঝে ফজর মিস হয়',
      'শুধু জুমুআ',
      'মাঝে মাঝে সালাহ পড়ি',
    ];

    Picker.init({
      pickerData: data,
      selectedValue: ['৫ ওয়াক্ত সালাহ পড়ি'],
      pickerTitleText: 'সালাহ কয় ওয়াক্ত পড়েন ?',
      onPickerConfirm: async data => {
        await this.setState({
          prayer: data[0],
          prayerSelected: true,
          prayerPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async data => {
        await this.setState({
          prayerPic: prayerSwitchFunction(data[0]),
        });
      },
    });
    Picker.show();
  };
  _onFemalePrayerPress = async () => {
    let data = [
      '৫ ওয়াক্ত সালাহ পড়ি',
      'মাঝে মাঝে ফজর মিস হয়',
      'মাঝে মাঝে সালাহ পড়ি',
    ];

    Picker.init({
      pickerData: data,
      selectedValue: ['৫ ওয়াক্ত সালাহ পড়ি'],
      pickerTitleText: 'সালাহ কয় ওয়াক্ত পড়েন ?',
      onPickerConfirm: async data => {
        await this.setState({
          prayer: data[0],
          prayerSelected: true,
          femalePrayerPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async data => {
        await this.setState({
          femalePrayerPic: femalePrayerSwitchFunction(
            data[0]
          ),
        });
      },
    });
    Picker.show();
  };

  _onPractisingPress = async () => {
    let data = [
      '৫ বছর এর উপরে',
      '৩ বছর ধরে',
      '১ বছর ধরে',
      'কয়েক মাস ধরে',
    ];

    Picker.init({
      pickerData: data,
      selectedValue: ['৫ বছর এর উপরে'],
      onPickerConfirm: async data => {
        await this.setState({
          practisingSince: data[0],
          practisingSinceSelected: true,
          deenPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async data => {
        await this.setState({
          deenPic: deenFunction(data[0]),
        });
      },
    });
    Picker.show();
  };

  _onEduPress = async () => {
    const data = eduList;
    Picker.init({
      pickerData: data,
      selectedValue: ['Hons'],
      onPickerConfirm: async data => {
        await this.setState({
          education: data[0],
          educationSelected: true,
          eduPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async data => {
        await this.setState({
          eduPic: eduSwitchFunction(data[0]),
        });
      },
    });
    Picker.show();
  };

  _onCurrentLocationPress = async () => {
    let data = Districts;

    Picker.init({
      pickerData: data,
      selectedValue: ['Dhaka'],
      onPickerConfirm: async data => {
        await this.setState({
          currentLocation: data[0],
          currentLocationSelected: true,
          currentLocationPic: require('../../assets/icons/accept.png'),
        });
      },
    });
    Picker.show();
  };

  _onHomeDistrictPress = async () => {
    let data = Districts;
    Picker.init({
      pickerData: data,
      selectedValue: ['Dhaka'],
      onPickerConfirm: async data => {
        await this.setState({
          homeDistrict: data[0],
          homeDistrictSelected: true,
          homePic: require('../../assets/icons/accept.png'),
        });
      },
    });
    Picker.show();
  };

  _onChangeText = async (text, type, nameOfState) => {
    await this.setState({
      [type]: text,
      [nameOfState]: true,
    });
  };

  _onSubmitPress = async () => {
    const {
      age,
      height,
      education,
      prayer,
      practisingSince,
      beard,
      homeDistrict,
      currentLocation,
      hifzLevel,
      profession,
      username,
      aboutMe,
      pics,
      pic1,
      pic2,
      pic3,
      pic4,
      nidCopy,
      avatar,
    } = this.state;

    await pics.push(pic1.uri, pic2.uri, pic3.uri, pic4.uri);
    const newPics = pics.filter(Boolean);
    console.log(newPics);

    this.setState({ loading: true });
    const { data } = this.props.mutate({
      variables: {
        age,
        height,
        education,
        prayer,
        practisingSince,
        beard,
        homeDistrict,
        currentLocation,
        hifzLevel,
        profession,
        username,
        aboutMe,
        pic: newPics,
        nid: nidCopy,
        avatar,
      },
      refetchQueries: [
        {
          query: GETONECV_QUERY,
          variables: {
            _id: this.props.navigation.state.params.id,
          },
        },
      ],
    });
    // const newPics = this.props.completedCv()
    this.setState({
      loading: false,
      loadSvg: true,
    });
  };

  _onRefresh = async () => {
    this.setState({ loading: true });
    await this.props.data.refetch();
    this.setState({ loading: false });
  };

  render() {
    if (this.state.loading || this.props.data.loading) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      );
    }

    if (this.state.loadSvg) {
      return (
        <Image
          source={require('../../assets/cvUpdated.png')}
          style={styles.bigSvg}
        />
      );
    }

    const {
      age,
      height,
      education,
      prayer,
      practisingSince,
      beard,
      homeDistrict,
      currentLocation,
      hifzLevel,
      profession,
      username,
      aboutMe,
      pic,
      nidCopy,
      avatar,
      gender,
    } = this.props.data.getOneCv;

    return (
      <View style={styles.container}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              onRefresh={this._onRefresh}
            />
          }
        >
          <Divider
            style={{
              backgroundColor: '#2980b9',
              marginBottom: 5,
            }}
          />

          <View style={styles.iconContainer3}>
            <TouchableOpacity
              style={{
                marginRight: 20,
              }}
              onPress={this._onQuranPress}
            >
              {this.state.hifzSelected ? (
                <Image
                  source={this.state.quranPic}
                  style={styles.icon}
                />
              ) : (
                <View style={styles.emptyIcon}>
                  <Text style={styles.iconText}>
                    {hifzLevel}
                  </Text>
                </View>
              )}
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer3}>
            <TouchableOpacity
              style={{
                marginRight: 20,
              }}
              onPress={this._onAgePress}
            >
              <Image
                source={
                  gender === 'female'
                    ? this.state.agePicFemale
                    : this.state.agePic
                }
                style={styles.icon}
              />
            </TouchableOpacity>

            <TouchableOpacity onPress={this._onHeightPress}>
              {this.state.heightSelected ? (
                <Image
                  source={this.state.heightPic}
                  style={styles.icon}
                />
              ) : (
                <View style={styles.emptyIcon}>
                  <Text style={styles.iconText}>
                    {height}
                  </Text>
                </View>
              )}
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer2}>
            {gender === 'female' ? (
              <TouchableOpacity
                onPress={this._onHijabPress}
              >
                <Image
                  source={this.state.hijabPic}
                  style={styles.icon}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={this._onBeardPress}
              >
                <Image
                  source={this.state.beardPic}
                  style={styles.icon}
                />
              </TouchableOpacity>
            )}

            {gender === 'female' ? (
              <TouchableOpacity
                onPress={this._onFemalePrayerPress}
              >
                <Image
                  source={this.state.femalePrayerPic}
                  style={styles.icon}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={this._onPrayerPress}
              >
                <Image
                  source={this.state.prayerPic}
                  style={styles.icon}
                />
              </TouchableOpacity>
            )}
            <TouchableOpacity
              onPress={this._onPractisingPress}
            >
              <Image
                source={this.state.deenPic}
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer3}>
            <TouchableOpacity
              onPress={this._onHomeDistrictPress}
              style={{
                marginRight: 20,
              }}
            >
              {this.state.homeDistrictSelected ? (
                <Image
                  source={this.state.homePic}
                  style={styles.icon}
                />
              ) : (
                <View style={styles.emptyIcon}>
                  <Text style={styles.iconText}>
                    {homeDistrict}
                  </Text>
                </View>
              )}
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this._onCurrentLocationPress}
            >
              {this.state.currentLocationSelected ? (
                <Image
                  source={this.state.currentLocationPic}
                  style={styles.icon}
                />
              ) : (
                <View style={styles.emptyIcon}>
                  <Text style={styles.iconText}>
                    {currentLocation}
                  </Text>
                </View>
              )}
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer3}>
            <TouchableOpacity
              style={{
                marginRight: 20,
              }}
              onPress={this._onEduPress}
            >
              <Image
                source={this.state.eduPic}
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <Divider
            style={{
              backgroundColor: '#2980b9',
              marginBottom: 10,
            }}
          />

          {/* #   Select starts here  */}
          {/* #   Select starts here  */}
          {/* #   Select starts here  */}

          <View style={styles.iconContainer}>
            <TouchableOpacity
              onPress={(pic, uploadedPic) =>
                this._onPhotoPress('nid', 'uploadedNid')
              }
            >
              <Image
                source={
                  this.state.uploadedNid
                    ? { uri: this.state.nid }
                    : this.state.nid
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={this._onCvPress}>
              <Image
                source={require('../../assets/icons/cv.png')}
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={(pic, uploadedPic) =>
                this._onPhotoPress('pic1', 'uploadedPic1')
              }
            >
              <Image
                source={this.state.pic1}
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <View>
            <Divider
              style={{
                backgroundColor: '#2980b9',
                marginBottom: 5,
              }}
            />
            <View style={styles.iconContainer}>
              <TouchableOpacity
                onPress={(pic, uploadedPic) =>
                  this._onPhotoPress('pic2', 'uploadedPic2')
                }
              >
                <Image
                  source={this.state.pic2}
                  style={styles.icon}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={(pic, uploadedPic) =>
                  this._onPhotoPress('pic3', 'uploadedPic3')
                }
              >
                <Image
                  source={this.state.pic3}
                  style={styles.icon}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={(pic, uploadedPic) =>
                  this._onPhotoPress('pic4', 'uploadedPic4')
                }
              >
                <Image
                  source={this.state.pic4}
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>
          </View>

          {/* #   Select ends here  */}
          {/* #   Select ends here  */}
          {/* #   Select ends here  */}

          <Divider
            style={{
              backgroundColor: '#2980b9',
              marginBottom: 10,
            }}
          />

          <View style={styles.form2}>
            <Kaede
              label={profession}
              inputStyle={{ color: 'black' }}
              labelStyle={{ color: '#ac83c4' }}
              defaultValue={profession}
              onChangeText={text =>
                this._onChangeText(
                  text,
                  'profession',
                  'professionSelected'
                )
              }
            />
            <Divider
              style={{
                backgroundColor: '#2980b9',
                marginBottom: 5,
              }}
            />
            <Kaede
              label="আপনার ইউজারনেম"
              defaultValue={username}
              inputStyle={{ color: 'black' }}
              labelStyle={{ color: '#ac83c4' }}
              onChangeText={text =>
                this._onChangeText(
                  text,
                  'username',
                  'usernameSelected'
                )
              }
            />
          </View>
          <TextInput
            multiline
            maxLength={1400}
            style={styles.textInput}
            placeholder="আপনার নিজের কথা"
            underlineColorAndroid="transparent"
            value={
              this.state.aboutMe == ''
                ? aboutMe
                : this.state.aboutMe
            }
            onChangeText={text =>
              this._onChangeText(text, 'aboutMe')
            }
          />
        </ScrollView>
        <TouchableOpacity onPress={this._onSubmitPress}>
          <View style={styles.buttonContainer}>
            <Text style={styles.buttonText}>Submit</Text>
          </View>
        </TouchableOpacity>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    );
  }
}

// ************************************************
//                     Export                     *
// ************************************************

export default compose(
  connect(
    undefined,
    { completedCv }
  ),
  graphql(GETONECV_QUERY, {
    options: ownProps => {
      return {
        variables: {
          _id: ownProps.navigation.state.params.id,
        },
      };
    },
  }),
  graphql(UPDATECV_MUTATION)
)(FormScreen);
