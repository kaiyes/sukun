import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  AsyncStorage,
  ActivityIndicator,
  KeyboardAvoidingView,
  Keyboard
} from 'react-native'
import { graphql, withApollo, compose } from 'react-apollo'
import { connect } from 'react-redux'
import DropdownAlert from 'react-native-dropdownalert'
import { Header } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {
  width,
  height,
  totalSize
} from 'react-native-dimension'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

import { colors } from '../utils/constants'
import LOGIN_MUTATION from '../graphql/mutation/login.mutaion'
import { logIn, completedCv } from '../actions/user.action'

import NavigationService from '../navigationService'
import LeftArrow from '../components/LeftArrow'
import LandingScreen from '../screens/Landing.Screen'

class LogInScreen extends Component {
  state = {
    email: '',
    password: '',
    loading: false,
    goToLanding: false,
    smallImage: false
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        this.setState({ smallImage: true })
      }
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        this.setState({ smallImage: false })
      }
    )
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  _onChangeText = (text, type) =>
    this.setState({
      [type]: text
    })

  _onLoginPress = async () => {
    this.setState({ loading: true })
    const { email, password } = this.state

    await this.props
      .mutate({
        variables: { email, password }
      })
      .then(({ data }) => {
        AsyncStorage.setItem(
          '@login:token',
          data.logIn.token
        )
        if (data !== null) {
          this.props.logIn(data.logIn.token)
          if ((data.logIn.user.hasCompletedCv = true)) {
            this.props.completedCv()
            NavigationService.navigate('List')
          }
        }
      })
      .catch(e => {
        this.setState({ loading: false })
        this.dropdown.alertWithType(
          'error',
          'Error',
          `${e.graphQLErrors[0].message}`
        )
      })
    this.setState({ loading: false })
  }

  render() {
    if (this.state.loading) {
      return <ActivityIndicator size="large" />
    }
    if (this.state.goToLanding) {
      return <LandingScreen />
    }

    return (
      <View style={styles.container}>
        <Header
          backgroundColor="#16bbcf"
          leftComponent={
            <LeftArrow
              onPress={() => {
                this.setState({ goToLanding: true })
              }}
            />
          }
          centerComponent={{
            text: 'Log In',
            style: {
              color: '#14141a',
              fontSize: 20,
              fontWeight: '700'
            }
          }}
        />
        <View style={styles.imageContainer}>
          {this.state.smallImage ? (
            <Image
              source={require('../../assets/icons/bigChat.png')}
              style={{
                width: 25,
                height: 25
              }}
            />
          ) : (
            <Image
              source={require('../../assets/icons/bigChat.png')}
            />
          )}
        </View>

        <View style={styles.formContainer}>
          <TextInput
            style={styles.form}
            placeholder="Email"
            value={this.state.email}
            keyBoardType="email-address"
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            onChangeText={text =>
              this._onChangeText(text, 'email')
            }
          />
          <TextInput
            style={styles.form}
            placeholder="Password"
            value={this.state.password}
            secureTextEntry
            underlineColorAndroid="transparent"
            onChangeText={text =>
              this._onChangeText(text, 'password')
            }
          />
        </View>

        <TouchableOpacity onPress={this._onLoginPress}>
          <View style={styles.signUpContainer}>
            <Text style={styles.signUpText}>LogIn</Text>
          </View>
        </TouchableOpacity>

        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    )
  }
}

export default compose(
  graphql(LOGIN_MUTATION),
  connect(undefined, { logIn, completedCv })
)(LogInScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column'
  },
  imageContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: hp('15%')
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  formContainer: {
    flex: 2,
    justifyContent: 'center',
    paddingHorizontal: wp('5.3%')
  },
  signUpContainer: {
    justifyContent: 'center',
    backgroundColor: '#2980b9'
  },
  subTitle: {
    fontFamily: 'Helvetica Neue',
    justifyContent: 'flex-start'
  },
  form: {
    height: hp('7.5%'),
    borderColor: '#2980b9',
    borderWidth: 1,
    borderRadius: wp('5.8%'),
    marginHorizontal: wp('2.7%'),
    marginVertical: hp('.5%'),
    textAlign: 'center',
    color: 'black'
  },
  signUpText: {
    color: 'white',
    fontFamily: 'Helvetica Neue',
    fontSize: 22,
    paddingVertical: hp('2.4%'),
    textAlign: 'center',
    fontWeight: '600'
  }
})
