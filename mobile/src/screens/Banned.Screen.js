import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native'
import { connect } from 'react-redux'

class BanScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('../../assets/icons/Banned.png')}
          style={styles.image}
        />
        <Text style={styles.heading}>
          ব্যান হওয়ার কারণ সমূহ:
        </Text>
        <View style={styles.textContainer}>
          <Text style={styles.text}>
            ১। অসংখ্য মুসলিমাকে মেসেজ করা । একসাথে ৩ জনের
            বেশি কারোর সাথে কথা বলা বা ছবির রিকোয়েস্ট পাঠান
            যাবে না ।
          </Text>
          <Text style={styles.text}>
            ২। ম্যাসেজিং এ কারো ফেসবুকের আইডি চাওয়া বা ইমেইল
            চাওয়া । সব ব্যবস্থা অ্যাপেই করা আছে ।
          </Text>
          <Text style={styles.text}>
            ৩। ম্যাসেজিং এ অশালীন আচরণ করা ।
          </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  image: {
    marginTop: 50
  },
  heading: {
    fontWeight: '900',
    fontSize: 22,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    textDecorationLine: 'underline',
    marginTop: 5
  },
  text: {
    fontWeight: '300',
    fontSize: 18,
    color: 'black',
    fontFamily: 'Helvetica Neue',
    textAlign: 'justify',
    marginTop: 10
  },
  textContainer: {
    alignItems: 'stretch',
    marginLeft: 20,
    marginRight: 20
  }
})

export default BanScreen
