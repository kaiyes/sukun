import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  AsyncStorage,
  ActivityIndicator,
  Keyboard
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DropdownAlert from 'react-native-dropdownalert'
import { graphql, compose } from 'react-apollo'
import { connect } from 'react-redux'
import { Header, Divider } from 'react-native-elements'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

import SIGNUP_MUTATION from '../graphql/mutation/signup.mutation'
import { logIn } from '../actions/user.action'

import LandingScreen from '../screens/Landing.Screen'
import LeftArrow from '../components/LeftArrow'

class SignUpScreen extends Component {
  state = {
    behavior: 'padding',
    email: '',
    password: '',
    loading: false,
    selected: false,
    gender: '',
    goToLanding: false,
    userType: ''
  }
  // async componentWillMount() {
  //   await AsyncStorage.removeItem('@login:token')
  //   await AsyncStorage.removeItem('@cv:cv')
  //   await AsyncStorage.removeItem('@gender:token')
  // }
  _onChangeText = (text, type) =>
    this.setState({
      [type]: text
    })

  _onSignupPress = () => {
    if (this.state.selected) {
      this.setState({ loading: true })
      const {
        email,
        password,
        gender,
        userType
      } = this.state
      this.props
        .mutate({
          variables: { email, password, gender, userType }
        })
        .then(({ data }) => {
          AsyncStorage.setItem(
            '@login:token',
            data.signUp.token
          )
          AsyncStorage.setItem(
            '@gender:token',
            data.signUp.user.gender
          )
          this.props.logIn(data.signUp.token)
        })
        .catch(e => {
          this.dropdown.alertWithType(
            'error',
            'Error',
            `${e.graphQLErrors[0].message}`
          )
        })
      this.setState({ loading: false })
    } else {
      this.dropdown.alertWithType(
        'error',
        'Error',
        'Please select your gender'
      )
    }
  }

  _onBrother = async () => {
    await this.setState({
      gender: 'male',
      userType: 'user',
      selected: true
    })
  }

  _onSister = async () => {
    await this.setState({
      gender: 'female',
      userType: 'user',
      selected: true
    })
  }

  _onFm = async () => {
    await this.setState({
      gender: 'female',
      userType: 'matchMaker',
      selected: true
    })
  }

  _onMm = async () => {
    await this.setState({
      gender: 'male',
      userType: 'matchMaker',
      selected: true
    })
  }

  render() {
    if (this.state.loading) {
      return <ActivityIndicator size="large" />
    }
    if (this.state.goToLanding) {
      return <LandingScreen />
    }
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView>
          <Header
            backgroundColor="#2479c7"
            leftComponent={
              <LeftArrow
                onPress={() => {
                  this.setState({ goToLanding: true })
                }}
              />
            }
            centerComponent={{
              text: 'Sign Up',
              style: {
                color: '#14141a',
                fontSize: 20,
                fontWeight: '700'
              }
            }}
          />
          <View style={styles.imageContainerTop}>
            <TouchableOpacity onPress={this._onSister}>
              <Image
                source={require('../../assets/icons/sister.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={this._onBrother}>
              <Image
                source={require('../../assets/icons/brother2.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.text}> Female</Text>
            <Text style={styles.text}> Male</Text>
          </View>
          <View style={styles.imageContainer}>
            <TouchableOpacity onPress={this._onSister}>
              <Image
                source={require('../../assets/icons/guardian2.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={this._onBrother}>
              <Image
                source={require('../../assets/icons/guardian2.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.text}>
              {'      '}Sister's {'\n'}
              {'    '} Guardian
            </Text>
            <Text style={styles.text}>
              {'      '}Brother's {'\n'}
              {'    '} Guardian
            </Text>
          </View>
          {/* <View style={styles.imageContainer}>
            <TouchableOpacity onPress={this._onFm}>
              <Image
                source={require('../../assets/icons/mm.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={this._onMm}>
              <Image
                source={require('../../assets/icons/fm.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.text}>
              {'         '}Female {'\n'}
              {'    '} Match Maker
            </Text>
            <Text style={styles.text}>
              {'            '}Male {'\n'}
              {'    '} Match Maker
            </Text>
          </View>
 */}
          <Divider style={styles.divider} />

          <View style={styles.formContainer}>
            <TextInput
              style={styles.form}
              placeholder="Email"
              value={this.state.email}
              keyBoardType="email-address"
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              onChangeText={text =>
                this._onChangeText(text, 'email')
              }
            />
            <TextInput
              style={styles.form}
              placeholder="Password"
              value={this.state.password}
              underlineColorAndroid="transparent"
              secureTextEntry
              onChangeText={text =>
                this._onChangeText(text, 'password')
              }
            />
          </View>
        </KeyboardAwareScrollView>

        <TouchableOpacity onPress={this._onSignupPress}>
          <View style={styles.signUpContainer}>
            <Text style={styles.signUpText}>SignUp</Text>
          </View>
        </TouchableOpacity>

        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    )
  }
}

export default compose(
  graphql(SIGNUP_MUTATION),
  connect(undefined, { logIn })
)(SignUpScreen)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  imageContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: hp('2.2%')
  },
  imageContainerTop: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: hp('15%')
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  text: {
    fontFamily: 'Helvetica Neue',
    fontWeight: '900',
    fontSize: 16,
    color: '#62a2ec',
    fontFamily: 'Helvetica Neue'
  },
  formContainer: {
    flex: 2,
    justifyContent: 'center',
    paddingHorizontal: hp('3%')
  },
  form: {
    height: hp('7.5%'),
    borderColor: '#62a2ec',
    borderWidth: 1,
    borderRadius: 22,
    margin: hp('1%'),
    textAlign: 'center'
  },
  signUpContainer: {
    justifyContent: 'center',
    backgroundColor: '#62a2ec',
    height: hp('9%')
  },
  signUpText: {
    color: 'white',
    fontFamily: 'Helvetica Neue',
    fontSize: 22,
    padding: wp('3%'),
    textAlign: 'center',
    fontWeight: '800'
  },
  divider: {
    backgroundColor: '#fff',
    marginBottom: hp('6%')
  }
})
