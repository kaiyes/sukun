import React, { Component } from 'react';
import ModalDropdown from 'react-native-modal-dropdown';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  RefreshControl,
} from 'react-native';
import Picker from 'react-native-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Select, Option } from 'react-native-select-list';
import { Divider } from 'react-native-elements';
import DropdownAlert from 'react-native-dropdownalert';
import { graphql, withApollo, compose } from 'react-apollo';
import { connect } from 'react-redux';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {
  DocumentPicker,
  DocumentPickerUtil,
} from 'react-native-document-picker';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Madoka } from 'react-native-textinput-effects';
import { width, height } from 'react-native-dimension';

import USER_QUERY from '../graphql/queries/currentUser.query';
import CREATECV_MUTATION from '../graphql/mutation/createCv.mutation';
import {
  userId,
  completedCv,
} from '../actions/user.action';
import NavigationService from '../navigationService';

import Header from '../components/header';
import heightList from '../utils/height';
import memorizationList from '../utils/memorizationList';
import eduList from '../utils/eduList';
import Districts from '../utils/districts';
import beardSwitchFunction from '../utils/beard';
import eduSwitchFunction from '../utils/education';
import deenFunction from '../utils/deen';
import prayerSwitchFunction from '../utils/prayer';
import femalePrayerSwitchFunction from '../utils/femalePrayer';
import ageSwitchFunction from '../utils/age';
import hijabSwitchFunction from '../utils/hijab';
import styles from '../utils/styles/editProfile.style';
import stateObject from '../utils/stateObject';

class EditProfileScreen extends Component {
  state = stateObject;

  async componentWillMount() {
    const token = await AsyncStorage.getItem(
      '@gender:token'
    );
    console.log(token);
    this.setState({
      gender: token,
    });
  }

  _onPhotoPress = async (pic, uploaded) => {
    console.log(pic);
  };

  _onCvPress = async () => {
    // try {
    //   const pdf = await DocumentPicker.show({
    //     filetype: [DocumentPickerUtil.pdf()]
    //   })
    // } catch (e) {
    //   console.log(e)
    // }
    // for (var i = 0; i < 100; i++) {
    //   data.push(i)
    // }
    console.log(this);
  };

  _onQuranPress = async () => {
    let data = memorizationList;

    Picker.init({
      pickerData: data,
      selectedValue: ['অল্প কিছু সুরা'],
      pickerTitleText: 'কতটুকু কুরআন মুখস্থ আছে ?',
      onPickerConfirm: async data => {
        await this.setState({
          hifzLevel: data[0],
          quranPic: require('../../assets/icons/accept.png'),
        });
      },
    });
    Picker.show();
  };

  _onAgePress = async () => {
    const { gender } = this.state;
    let data = [];
    for (var i = 18; i < 60; i++) {
      data.push(i);
    }

    Picker.init({
      pickerData: data,
      selectedValue: ['25'],
      pickerTitleText: 'আপনার বয়স কত ?',
      onPickerConfirm: async data => {
        await this.setState({
          age: data[0],
          ageSelected: true,
        });
        if (gender === 'male') {
          this.setState({
            agePic: require('../../assets/icons/accept.png'),
          });
        } else if (gender === 'female') {
          this.setState({
            agePicFemale: require('../../assets/icons/accept.png'),
          });
        }
      },
      onPickerSelect: async data => {
        let clientAge = data[0];
        if (gender === 'male') {
          if (clientAge < 38) {
            this.setState({
              agePic: require('../../assets/icons/young.png'),
            });
          } else if (clientAge > 39 && clientAge < 60) {
            this.setState({
              agePic: require('../../assets/icons/old.png'),
            });
          }
        } else if (gender === 'female') {
          if (clientAge < 38) {
            this.setState({
              agePicFemale: require('../../assets/icons/youngGirl.png'),
            });
          } else if (clientAge > 39 && clientAge < 60) {
            this.setState({
              agePicFemale: require('../../assets/icons/agedGirl.png'),
            });
          }
        }
      },
    });
    Picker.show();
  };

  _onHeightPress = async () => {
    const { gender } = this.state;
    let data = heightList;
    Picker.init({
      pickerData: data,
      selectedValue: ['৫ ফিট'],
      pickerTitleText: 'আপনার উচ্চতা কত ?',
      onPickerConfirm: async data => {
        await this.setState({
          height: data[0],
          heightSelected: true,
        });
        if (gender === 'male') {
          this.setState({
            heightPic: require('../../assets/icons/accept.png'),
          });
        } else if (gender === 'female') {
          this.setState({
            heightPicFemale: require('../../assets/icons/accept.png'),
          });
        }
      },
      onPickerSelect: async data => {
        let clientHeight = data[0];
        if (clientHeight == 'তালগাছ') {
          this.setState({
            heightPic: require('../../assets/icons/heightTree.png'),
          });
        }
      },
    });
    Picker.show();
  };

  _onBeardPress = async () => {
    let maleData = [
      'সুন্নাহ মোতাবেক পূর্ণ দাড়ি',
      'এক মুঠোর উপরে কেটে ফেলা',
      'দাড়ি ট্রিম করা ট্রিমার দিয়ে',
      'প্রাকৃতিক ভাবে উঠে না',
      'শেভ করা',
    ];
    Picker.init({
      pickerData: maleData,
      selectedValue: ['সুন্নাহ মোতাবেক পূর্ণ দাড়ি'],
      pickerTitleText: 'দাড়ি কি ভাবে রাখেন ?',
      onPickerConfirm: async maleData => {
        await this.setState({
          beard: maleData[0],
          beardSelected: true,
          beardPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async maleData => {
        this.setState({
          beardPic: beardSwitchFunction(maleData[0]),
        });
      },
    });

    Picker.show();
  };
  _onHijabPress = async () => {
    let femaleData = [
      'নিকাব-আবায়া-বোরখা',
      'আবায়া/বোরখা মুখ খোলা',
      'শুধু স্কার্ফ',
      'হিজাব ব্যতীত সাধারণ পোশাক',
    ];
    Picker.init({
      pickerData: femaleData,
      selectedValue: ['আবায়া/বোরখা মুখ খোলা'],
      pickerTitleText: 'হিজাবের বিবরণ',
      onPickerConfirm: async femaleData => {
        await this.setState({
          beard: femaleData[0],
          beardSelected: true,
          hijabPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async femaleData => {
        this.setState({
          hijabPic: hijabSwitchFunction(femaleData[0]),
        });
      },
    });

    Picker.show();
  };

  _onPrayerPress = async () => {
    let data = [
      '৫ ওয়াক্ত সালাহ পড়ি',
      'মাঝে মাঝে ফজর মিস হয়',
      'শুধু জুমুআ',
      'মাঝে মাঝে সালাহ পড়ি',
    ];

    Picker.init({
      pickerData: data,
      selectedValue: ['৫ ওয়াক্ত সালাহ পড়ি'],
      pickerTitleText: 'সালাহ কয় ওয়াক্ত পড়েন ?',
      onPickerConfirm: async data => {
        await this.setState({
          prayer: data[0],
          prayerSelected: true,
          prayerPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async data => {
        await this.setState({
          prayerPic: prayerSwitchFunction(data[0]),
        });
      },
    });
    Picker.show();
  };
  _onFemalePrayerPress = async () => {
    let data = [
      '৫ ওয়াক্ত সালাহ পড়ি',
      'মাঝে মাঝে ফজর মিস হয়',
      'মাঝে মাঝে সালাহ পড়ি',
    ];

    Picker.init({
      pickerData: data,
      selectedValue: ['৫ ওয়াক্ত সালাহ পড়ি'],
      pickerTitleText: 'সালাহ কয় ওয়াক্ত পড়েন ?',
      onPickerConfirm: async data => {
        await this.setState({
          prayer: data[0],
          prayerSelected: true,
          femalePrayerPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async data => {
        await this.setState({
          femalePrayerPic: femalePrayerSwitchFunction(
            data[0]
          ),
        });
      },
    });
    Picker.show();
  };

  _onPractisingPress = async () => {
    let data = [
      '৫ বছর এর উপরে',
      '৩ বছর ধরে',
      '১ বছর ধরে',
      'কয়েক মাস ধরে',
    ];

    Picker.init({
      pickerData: data,
      selectedValue: ['৫ বছর এর উপরে'],
      onPickerConfirm: async data => {
        await this.setState({
          practisingSince: data[0],
          practisingSinceSelected: true,
          deenPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async data => {
        await this.setState({
          deenPic: deenFunction(data[0]),
        });
      },
    });
    Picker.show();
  };

  _onEduPress = async () => {
    const data = eduList;
    Picker.init({
      pickerData: data,
      selectedValue: ['Hons'],
      onPickerConfirm: async data => {
        await this.setState({
          education: data[0],
          educationSelected: true,
          eduPic: require('../../assets/icons/accept.png'),
        });
      },
      onPickerSelect: async data => {
        await this.setState({
          eduPic: eduSwitchFunction(data[0]),
        });
      },
    });
    Picker.show();
  };

  _onCurrentLocationPress = async () => {
    let data = Districts;

    Picker.init({
      pickerData: data,
      selectedValue: ['Dhaka'],
      onPickerConfirm: async data => {
        await this.setState({
          currentLocation: data[0],
          currentLocationSelected: true,
          currentLocationPic: require('../../assets/icons/accept.png'),
        });
      },
    });
    Picker.show();
  };

  _onHomeDistrictPress = async () => {
    let data = Districts;
    Picker.init({
      pickerData: data,
      selectedValue: ['Dhaka'],
      onPickerConfirm: async data => {
        await this.setState({
          homeDistrict: data[0],
          homeDistrictSelected: true,
          homePic: require('../../assets/icons/accept.png'),
        });
      },
    });
    Picker.show();
  };

  _onAvatarPress = async picNumber => {
    console.log(picNumber);
    this.setState({
      avatarUploaded: true,
      avatar: picNumber,
    });
  };

  _onChangeText = async (text, type, nameOfState) => {
    await this.setState({
      [type]: text,
      [nameOfState]: true,
    });
  };

  _onRefresh = async () => {
    this.setState({ loading: true });
    await this.props.data.refetch();
    this.setState({ loading: false });
  };

  _onSubmitPress = async () => {
    const {
      ageSelected,
      heightSelected,
      professionSelected,
      educationSelected,
      prayerSelected,
      usernameSelected,
      practisingSinceSelected,
      avatarUploaded,
    } = this.state;

    if (
      ageSelected &&
      heightSelected &&
      professionSelected &&
      educationSelected &&
      prayerSelected &&
      usernameSelected &&
      practisingSinceSelected &&
      avatarUploaded
    ) {
      const {
        age,
        height,
        education,
        prayer,
        practisingSince,
        beard,
        homeDistrict,
        currentLocation,
        hifzLevel,
        profession,
        username,
        aboutMe,
        pics,
        nidCopy,
        avatar,
      } = this.state;

      this.setState({ loading: true });
      const { data } = await this.props
        .mutate({
          variables: {
            age,
            height,
            education,
            prayer,
            practisingSince,
            beard,
            homeDistrict,
            currentLocation,
            hifzLevel,
            profession,
            username,
            aboutMe,
            pic: pics,
            nid: nidCopy,
            avatar,
          },
        })
        .catch(err => {
          console.log(`${err}`);
        });

      await AsyncStorage.setItem('@cv:cv', 'true');
      await this.props.completedCv();

      await this.setState({ loading: false });
    } else {
      this.dropdown.alertWithType(
        'error',
        'Error',
        'সবকিছু পূরণ করুন'
      );
    }
  };

  render() {
    const { data } = this.props;
    const {
      girlHeight,
      agePicFemale,
      agePic,
      heightPic,
      heightPicFemale,
      loading,
      nid,
      uploadedNid,
      uploadedPic1,
      uploadedPic2,
      uploadedPic3,
      uploadedPic4,
      pic1,
      pic2,
      pic3,
      pic4,
      femalePrayerPic,
      prayerPic,
      gender,
    } = this.state;

    if (loading || data.loading) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      );
    }

    return (
      <View style={styles.container}>
        <Header />
        <KeyboardAwareScrollView
          behavior="padding"
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              onRefresh={this._onRefresh}
            />
          }
        >
          <Divider
            style={{
              backgroundColor: '#ffffff',
              marginBottom: 5,
            }}
          />

          <View style={styles.iconContainer3}>
            <TouchableOpacity
              style={{
                marginRight: 20,
              }}
              onPress={this._onQuranPress}
            >
              <Image
                source={this.state.quranPic}
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer3}>
            <TouchableOpacity
              style={{
                marginRight: 20,
              }}
              onPress={this._onAgePress}
            >
              <Image
                source={
                  gender === 'female'
                    ? agePicFemale
                    : agePic
                }
                style={styles.icon}
              />
            </TouchableOpacity>

            <TouchableOpacity onPress={this._onHeightPress}>
              <Image
                source={
                  gender === 'female'
                    ? heightPicFemale
                    : heightPic
                }
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer2}>
            {gender === 'female' ? (
              <TouchableOpacity
                onPress={this._onHijabPress}
              >
                <Image
                  source={this.state.hijabPic}
                  style={styles.icon}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={this._onBeardPress}
              >
                <Image
                  source={this.state.beardPic}
                  style={styles.icon}
                />
              </TouchableOpacity>
            )}

            {gender === 'female' ? (
              <TouchableOpacity
                onPress={this._onFemalePrayerPress}
              >
                <Image
                  source={femalePrayerPic}
                  style={styles.icon}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={this._onPrayerPress}
                style={{
                  marginLeft: width(4),
                  marginRight: width(4),
                }}
              >
                <Image
                  source={prayerPic}
                  style={styles.icon}
                />
              </TouchableOpacity>
            )}

            <TouchableOpacity
              onPress={this._onPractisingPress}
            >
              <Image
                source={this.state.deenPic}
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer3}>
            <TouchableOpacity
              onPress={this._onHomeDistrictPress}
              style={{
                marginRight: 20,
              }}
            >
              <Image
                source={this.state.homePic}
                style={styles.icon}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this._onCurrentLocationPress}
            >
              <Image
                source={this.state.currentLocationPic}
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer3}>
            <TouchableOpacity
              style={{
                marginRight: 20,
              }}
              onPress={this._onEduPress}
            >
              <Image
                source={this.state.eduPic}
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <Divider
            style={{
              backgroundColor: '#ffffff',
              marginBottom: 10,
            }}
          />
          <View style={styles.form2}>
            <Madoka
              label={'আপনার পেশা'}
              borderColor={'#a5d1cc'}
              labelStyle={{ color: '#ac83c4' }}
              onChangeText={text =>
                this._onChangeText(
                  text,
                  'profession',
                  'professionSelected'
                )
              }
            />

            <Divider
              style={{
                backgroundColor: '#ffffff',
                marginBottom: 5,
              }}
            />
            <Madoka
              label={'আপনার ইউজারনেম'}
              borderColor={'#a5d1cc'}
              labelStyle={{ color: '#ac83c4' }}
              onChangeText={text =>
                this._onChangeText(
                  text,
                  'username',
                  'usernameSelected'
                )
              }
            />

            <TextInput
              multiline
              maxLength={1400}
              style={styles.textInput}
              placeholder="আপনার নিজের কিছু কথা"
              underlineColorAndroid="transparent"
              value={this.state.aboutMe}
              onChangeText={text =>
                this._onChangeText(text, 'aboutMe')
              }
            />
          </View>

          <View style={styles.iconContainer}>
            <TouchableOpacity
              onPress={(pic, uploadedPic) =>
                this._onPhotoPress('pic3', 'uploadedPic3')
              }
            >
              <Image
                source={
                  uploadedPic3
                    ? { uri: pic3 }
                    : require('../../assets/icons/photo3.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={(pic, uploadedPic) =>
                this._onPhotoPress('pic2', 'uploadedPic2')
              }
            >
              <Image
                source={
                  uploadedPic2
                    ? { uri: pic2 }
                    : require('../../assets/icons/photo2.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={(pic, uploadedPic) =>
                this._onPhotoPress('pic1', 'uploadedPic1')
              }
            >
              <Image
                source={
                  uploadedPic1
                    ? { uri: pic1 }
                    : require('../../assets/icons/photo1.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text style={styles.heading}>
              Choose your avatar
            </Text>
          </View>

          {this.state.avatarUploaded ? (
            <View style={styles.iconContainer}>
              <Image
                source={{ uri: this.state.avatar }}
                style={styles.icon}
              />
            </View>
          ) : null}

          <View style={styles.iconContainer}>
            <TouchableOpacity
              onPress={pic =>
                this._onAvatarPress(
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1510133895/brother5_wgghlj.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1510132967/sister_srunav.png'
                )
              }
            >
              <Image
                source={
                  gender === 'female'
                    ? require('../../assets/icons/sister.png')
                    : require('../../assets/icons/brother5.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={pic =>
                this._onAvatarPress(
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1531376949/fullBeard3_kltlvc.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1510133935/sister1_fpk5mc.png'
                )
              }
            >
              <Image
                source={
                  gender === 'female'
                    ? require('../../assets/icons/sister1.png')
                    : require('../../assets/icons/fullBeard3.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={pic =>
                this._onAvatarPress(
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1531376949/fullBeard2_f76jq3.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1510133935/sister2_fablab.png'
                )
              }
            >
              <Image
                source={
                  gender === 'female'
                    ? require('../../assets/icons/sister2.png')
                    : require('../../assets/icons/fullBeard2.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer}>
            <TouchableOpacity
              onPress={pic =>
                this._onAvatarPress(
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1510133895/brother4_efiq2h.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1510133935/sister3_niws6i.png'
                )
              }
            >
              <Image
                source={
                  gender === 'female'
                    ? require('../../assets/icons/sister3.png')
                    : require('../../assets/icons/brother4.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={pic =>
                this._onAvatarPress(
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1510133895/brother3_towyck.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1532747893/sister4_ugixnx.png'
                )
              }
            >
              <Image
                source={
                  gender === 'female'
                    ? require('../../assets/icons/sister4.png')
                    : require('../../assets/icons/brother3.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={pic =>
                this._onAvatarPress(
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1510133895/brother2_kapva4.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1532751995/sister7_cczuou.png'
                )
              }
            >
              <Image
                source={
                  gender === 'female'
                    ? require('../../assets/icons/sister7.png')
                    : require('../../assets/icons/brother2.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.iconContainer}>
            <TouchableOpacity
              onPress={pic =>
                this._onAvatarPress(
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1531376949/noBeard2_aym202.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1532750834/sister6_hhvdda.png'
                )
              }
            >
              <Image
                source={
                  gender === 'female'
                    ? require('../../assets/icons/sister6.png')
                    : require('../../assets/icons/noBeard2.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={pic =>
                this._onAvatarPress(
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1531376949/trimmed2_abdlqs.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1532750297/sister5_diqvnh.png'
                )
              }
            >
              <Image
                source={
                  gender === 'female'
                    ? require('../../assets/icons/sister5.png')
                    : require('../../assets/icons/trimmed2.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={pic =>
                this._onAvatarPress(
                  gender === 'male'
                    ? 'https://res.cloudinary.com/kaiyes/image/upload/v1531376949/shaved2_kwxxt5.png'
                    : 'https://res.cloudinary.com/kaiyes/image/upload/v1532752662/sister8_ct82k2.png'
                )
              }
            >
              <Image
                source={
                  gender === 'female'
                    ? require('../../assets/icons/sister8.png')
                    : require('../../assets/icons/shaved2.png')
                }
                style={styles.icon}
              />
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>

        <TouchableOpacity onPress={this._onSubmitPress}>
          <View style={styles.submitButton}>
            <Text style={styles.buttonText}>Submit</Text>
          </View>
        </TouchableOpacity>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    );
  }
}

export default compose(
  graphql(CREATECV_MUTATION),
  graphql(USER_QUERY),
  connect(
    state => ({
      gender: state.user.gender,
    }),
    { completedCv }
  )
)(EditProfileScreen);
