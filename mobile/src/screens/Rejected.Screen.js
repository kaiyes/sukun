import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView
} from 'react-native'

class RejectedScreen extends Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <Image
          source={require('../../assets/icons/reject.png')}
          style={styles.image}
        />
        <Text style={styles.heading}>
          দুঃখিত আপনাকে সুকুনে এ আপাতত নেয়া যাচ্ছে না।
        </Text>
        <Text style={styles.heading2}>
          যে কারণে এটি হতে পারে:
        </Text>
        <View style={styles.textContainer}>
          <Text style={styles.text}>
            ১। সুকুন ইসলামী মূল্যবোধের উপরে সংসার গড়তে
            প্র্যাকটিসিং মুসলিমদের সাহায্য চায় এমন একটি
            প্রতিষ্ঠান । আপনার বাহ্যিক অবস্থা দেখে আপাত
            দৃষ্টিতে আপনি ইসলামী মূল্যবোধের অনুসারী বলে মনে
            হচ্ছে না বলে আমরা আন্তরিকভাবে দুঃখিত । আশা করি,
            আপনি পুরোপুরি ইসলামের ভিতরে প্রবেশ করবেন, দাড়ি
            রাখবেন বা বোরখা পরা শুরু করবেন এবং আবার আমাদের
            এখানে ফিরে আসবেন ।
          </Text>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  image: {
    marginTop: 50
  },
  heading: {
    fontWeight: '900',
    fontSize: 16,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    // textDecorationLine: 'underline',
    marginTop: 10
  },
  heading2: {
    fontWeight: '900',
    fontSize: 16,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    textDecorationLine: 'underline',
    marginTop: 5
  },
  text: {
    fontWeight: '300',
    fontSize: 18,
    color: 'black',
    fontFamily: 'Helvetica Neue',
    textAlign: 'justify',
    marginTop: 10
  },
  textContainer: {
    alignItems: 'stretch',
    marginLeft: 20,
    marginRight: 20
  }
})

export default RejectedScreen
