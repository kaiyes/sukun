import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  ScrollView,
  FlatList,
  Dimensions
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import {
  Avatar,
  Button,
  Divider
} from 'react-native-elements'
import KeyboardSpacer from 'react-native-keyboard-spacer'
import { connect } from 'react-redux'
import _ from 'lodash'

import Header from '../components/header'
import getMessages_QUERY from '../graphql/queries/getMessages.query'
import messages_SUBSCRIPTION from '../graphql/subscriptions/messages.subscription'
import sendMessage_MUTATION from '../graphql/mutation/sendMessage.mutation'
import makeChatSeen_MUTATION from '../graphql/mutation/makeChatNotiSeen.mutation'
import getMsgNoti_QUERY from '../graphql/queries/getMessageNotifications.query'

class AdminChatScreen extends Component {
  state = {
    text: ''
  }

  async componentWillMount() {
    this.props.data.subscribeToMore({
      document: messages_SUBSCRIPTION,
      variables: {
        conversationId: this.props.navigation.state.params
          .conversationId
      },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) {
          console.log(prev)
          return prev
        }
        const newMessage =
          subscriptionData.data.messageAdded
        if (
          !prev.getMessages.find(
            item => item._id === newMessage._id
          )
        ) {
          return {
            ...prev,
            getMessages: [
              ...prev.getMessages,
              { ...newMessage }
            ]
          }
        }
        return prev
      }
    })
    // console.log(Dimensions.get('window'))
    await this.props
      .makeChatSeen({
        variables: {
          conversationId: this.props.navigation.state.params
            .conversationId
        },
        refetchQueries: [
          {
            query: getMsgNoti_QUERY
          }
        ]
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  _onSendMessage = async () => {
    await this.props
      .mutate({
        variables: {
          conversationId: this.props.navigation.state.params
            .conversationId,
          body: this.state.text
        }
      })
      .then(this.setState({ text: '' }))
      .catch(err => {
        console.log(`${err}`)
      })
  }

  render() {
    if (this.props.data.loading) {
      return (
        <View>
          <ActivityIndicator size="large" />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <Header />

        <FlatList
          contentContainerStyle={{ alignSelf: 'stretch' }}
          data={this.props.data.getMessages}
          keyExtractor={item => item._id}
          renderItem={({ item }) =>
            <View style={styles.card}>
              <Avatar
                medium
                rounded
                activeOpacity={0.5}
                source={
                  item.sender._id === this.props.userId
                    ? {
                        uri: item.sender.cv.pic[0]
                      }
                    : _.includes(
                        item.sender.cv.photoApproved,
                        this.props.userId
                      )
                      ? {
                          uri: item.sender.cv.pic[0]
                        }
                      : item.sender.cv.gender === 'female'
                        ? require('../../assets/icons/sister.png')
                        : require('../../assets/icons/brother2.png')
                }
              />
              <View style={styles.chatTextContainer}>
                <Text style={styles.username}>
                  {item.sender.cv.username}
                </Text>
                <Text style={styles.chatBody}>
                  {item.body}
                </Text>
              </View>
            </View>}
        />

        <TextInput
          style={styles.form}
          placeholder="write here"
          underlineColorAndroid="transparent"
          value={this.state.text}
          onChangeText={text => this.setState({ text })}
          onSubmitEditing={this._onSendMessage}
        />
        <KeyboardSpacer />
      </View>
    )
  }
}

export default compose(
  connect(state => ({
    userId: state.user.userId
  })),
  graphql(getMessages_QUERY, {
    options: ownProps => {
      return {
        variables: {
          conversationId:
            ownProps.navigation.state.params.conversationId
        }
      }
    }
  }),
  graphql(sendMessage_MUTATION),
  graphql(makeChatSeen_MUTATION, { name: 'makeChatSeen' })
)(AdminChatScreen)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  form: {
    height: 50,
    borderColor: '#959595',
    borderWidth: 3,
    margin: 10,
    textAlign: 'left'
  },
  card: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 10,
    marginTop: 10
  },
  chatTextContainer: {
    marginBottom: 5,
    marginLeft: 15
  },
  chatBody: {
    fontWeight: '400',
    fontSize: 16,
    fontFamily: 'Avenir-Medium',
    marginBottom: 2
  },
  username: {
    fontWeight: '900',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginBottom: 2
  }
})
