import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TextInput
} from 'react-native'
import { Button } from 'react-native-elements'
import KeyboardSpacer from 'react-native-keyboard-spacer'
import { graphql } from 'react-apollo'
import DropdownAlert from 'react-native-dropdownalert'

import Header from '../components/header'

import payment_MUTATION from '../graphql/mutation/createPayment.mutation'
class PaymentScreen extends Component {
  state = {
    bikash: '',
    amount: ''
  }
  _onChangeText = (text, type) =>
    this.setState({
      [type]: text
    })

  _onSubmitPress = () => {
    const { bikash, amount } = this.state

    this.props
      .mutate({
        variables: {
          amount,
          bikash
        }
      })
      .then(({ data }) => {
        console.log(data)
        this.dropdown.alertWithType(
          'success',
          'Success',
          'Your payment went through'
        )
      })
      .catch(err => {
        this.dropdown.alertWithType(
          'error',
          'Error',
          `${err.graphQLErrors[0].message}`
        )
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <View style={styles.circle}>
            <Text style={styles.text}>
              Sukun's Bikash Number
            </Text>
            <Text style={styles.number}>01703305288</Text>
            <Text style={styles.smallText}>Personal</Text>
          </View>

          <TextInput
            style={styles.form}
            placeholder="Amount"
            autoCapitalize="none"
            value={this.state.amount}
            underlineColorAndroid="transparent"
            onChangeText={text =>
              this._onChangeText(text, 'amount')}
          />

          <TextInput
            style={styles.form}
            placeholder="Last 4 Digits of your Bikash"
            keyBoardType="numeric"
            value={this.state.bikash}
            underlineColorAndroid="transparent"
            onChangeText={text =>
              this._onChangeText(text, 'bikash')}
          />
        </View>
        <KeyboardSpacer />
        <Button
          large
          title="Submit Information"
          buttonStyle={{
            backgroundColor: '#4990E2',
            borderRadius: 15,
            marginBottom: 20
          }}
          onPress={this._onSubmitPress}
        />
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  circle: {
    height: 300,
    width: 300,
    borderRadius: 150,
    backgroundColor: '#4990E2',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },
  text: {
    fontWeight: '400',
    fontSize: 22,
    color: 'white',
    fontFamily: 'Helvetica Neue',
    marginTop: -40,
    marginBottom: 35
  },
  number: {
    fontWeight: '900',
    fontSize: 30,
    color: 'white',
    fontFamily: 'Helvetica Neue'
  },
  smallText: {
    fontWeight: '300',
    fontSize: 16,
    color: 'white',
    fontFamily: 'Helvetica Neue'
  },
  form: {
    height: 50,
    borderColor: '#62a2ec',
    borderWidth: 1,
    borderRadius: 22,
    marginTop: 10,
    textAlign: 'center'
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    marginBottom: 20,
    marginTop: -40
  }
})

export default graphql(payment_MUTATION)(PaymentScreen)
