import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native'
import Header from '../components/header'
class BanScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Header />
        <View>
          <Text style={styles.text}>
            {' '}
            এডমিন আপনার বায়ডাটা চেক করছেন । চেক শেষ হওয়া
            পর্যন্ত অপেক্ষা করুন প্লিজ । | কয়েকদিন লাগে
            সাধারণত ।
          </Text>
          <Image
            source={require('../../assets/icons/reject.png')}
            style={styles.image}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  text: {
    fontWeight: '900',
    fontSize: 20,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginTop: 10,
    textAlign: 'center'
  },
  image: {
    marginTop: 50,
    marginLeft: 30
  }
})
export default BanScreen
