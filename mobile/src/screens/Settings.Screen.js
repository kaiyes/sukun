import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  AsyncStorage,
  ScrollView,
  Dimensions
} from 'react-native'
import { graphql, compose, withApollo } from 'react-apollo'
import { connect } from 'react-redux'
import { Button, Divider } from 'react-native-elements'
import {
  width,
  height,
  totalSize
} from 'react-native-dimension'
import _ from 'lodash'

import Header from '../components/header'
import CREATECHATROOM_MUTATION from '../graphql/mutation/createChatRoom.mutation'
import myChats_QUERY from '../graphql/queries/getMyChats.query'
import { logout } from '../actions/user.action'
import { insertChatId } from '../actions/chat.action'

class SettingsScreen extends Component {
  _onLogoutPress = async () => {
    await this.props.logout()
  }
  _onChatPress = async () => {
    const id = '5b5d4e723db8a4001ffc5fdb'
    const ifExists = _.find(this.props.chatList, [
      'userId',
      id
    ])
    if (ifExists === undefined) {
      const { data } = await this.props.adminChat({
        variables: {
          _id: id
        },
        refetchQueries: [{ query: myChats_QUERY }]
      })
      await this.props.insertChatId(
        id,
        data.createChatRoom._id
      )
      this.props.navigation.navigate('Messages', {
        conversationId: data.createChatRoom._id,
        username: 'Admin'
      })
    } else {
      this.props.navigation.navigate('Messages', {
        conversationId: ifExists.chatRoomId,
        username: 'Admin'
      })
    }
  }

  render() {
    const { navigate } = this.props.navigation

    return (
      <ScrollView style={styles.container}>
        {this.props.userType != 'matchMaker' ? (
          <TouchableOpacity
            onPress={() =>
              navigate('EditProfile', {
                id: this.props.userId
              })
            }
            style={{
              backgroundColor: '#DDDCB6',
              height: 80,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text
              style={{
                fontWeight: '900',
                fontSize: 20,
                color: '#E68D5B',
                fontFamily: 'Helvetica Neue'
              }}
            >
              Edit Profile
            </Text>
          </TouchableOpacity>
        ) : null}

        <TouchableOpacity
          onPress={() => navigate('Faq')}
          style={{
            backgroundColor: '#E96D59',
            height: 80,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text
            style={{
              fontWeight: '900',
              fontSize: 20,
              color: '#6b3f27',
              fontFamily: 'Helvetica Neue'
            }}
          >
            Faq
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={this._onChatPress}
          style={{
            backgroundColor: '#A33451',
            height: 80,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text
            style={{
              fontWeight: '900',
              fontSize: 20,
              color: '#3d2416',
              fontFamily: 'Helvetica Neue'
            }}
          >
            Chat With Admin
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => console.log('logout')}
          style={{
            backgroundColor: '#502F50',
            height: 80,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text
            style={{
              fontWeight: '900',
              fontSize: 20,
              color: '#a78f82',
              fontFamily: 'Helvetica Neue'
            }}
          >
            Deactivate Account
          </Text>
        </TouchableOpacity>

        {this.props.userType != 'matchMaker' ? (
          <TouchableOpacity
            onPress={() => navigate('Payment')}
            style={{
              backgroundColor: '#300730',
              height: 80,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text
              style={{
                fontWeight: '900',
                fontSize: 20,
                color: '#dfccc2',
                fontFamily: 'Helvetica Neue'
              }}
            >
              Payment
            </Text>
          </TouchableOpacity>
        ) : null}

        <TouchableOpacity
          onPress={this._onLogoutPress}
          style={{
            backgroundColor: 'black',
            height: 80,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text
            style={{
              fontWeight: '400',
              fontSize: 20,
              color: 'white',
              fontFamily: 'Helvetica Neue'
            }}
          >
            Logout
          </Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }
}

export default compose(
  connect(
    state => ({
      userId: state.user.userId,
      chatList: state.chat.list,
      userType: state.user.userType
    }),
    { logout, insertChatId }
  ),
  graphql(CREATECHATROOM_MUTATION, { name: 'adminChat' })
)(SettingsScreen)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1'
  },
  text: {
    fontWeight: '900',
    fontSize: 20,
    color: '#E68D5B',
    fontFamily: 'Helvetica Neue'
  }
})
