import React, { Component } from 'react'
import { graphql, compose } from 'react-apollo'
import {
  ActivityIndicator,
  FlatList,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native'
import {
  Card,
  ListItem,
  Button,
  Divider,
  Avatar,
  Badge
} from 'react-native-elements'
import { connect } from 'react-redux'
import { width, height } from 'react-native-dimension'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import DropdownAlert from 'react-native-dropdownalert'

import matches_QUERY from '../graphql/queries/matches.query'
import myMatchers_QUERY from '../graphql/queries/myMatchers.query'
import myPendingMatchers_QUERY from '../graphql/queries/myPendingMatchers.query'
import user_QUERY from '../graphql/queries/currentUser.query'
import approvedTeams_Query from '../graphql/queries/approvedTeams.query'
import GIVE_PERMISSION_MUTATION from '../matchMaker/graphql/mutation/giveMatchPermission.mutation'
import REMOVE_PERMISSION_MUTATION from '../matchMaker/graphql/mutation/removeMatchPermission.mutation'
import MATCH_SEEN_MUTATION from '../graphql/mutation/matchSeen.mutation'
import REQ_TO_MATCH_MUTATION from '../matchMaker/graphql/mutation/createMatchRequest.mutation'

import { subbed, unSubbed } from '../actions/payment.action'

class MatchScreen extends Component {
  componentWillMount() {
    this.props.navigation.addListener('didFocus', () => {
      this.props
        .makeSeen({
          refetchQueries: [{ query: matches_QUERY }]
        })
        .catch(err => {
          console.log(`${err}`)
        })
    })
  }
  _onPaymentPress = () => {
    this.props.navigation.navigate('Payment')
  }

  _onHelpPress = async team => {
    await this.props.reqToMatch({
      variables: {
        _id: this.props.userId,
        team: team,
        gender: this.props.gender
      }
    })
    await this.dropdown.alertWithType(
      'success',
      'Success',
      'Request Sent'
    )
  }

  _refresh = async () => {
    let { data } = await this.props.userQuery.refetch()
    if (data.userInfo.hasPaid) {
      await this.props.dispatch(subbed())
    } else {
      await this.props.dispatch(unSubbed())
    }
  }

  render() {
    const {
      data,
      gender,
      myMatchers,
      pending,
      approvedTeams
    } = this.props
    if (
      data.loading ||
      myMatchers.loading ||
      pending.loading
    ) {
      return (
        <View>
          <ActivityIndicator size="large" />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <ScrollableTabView>
          <ScrollView tabLabel="Matches">
            {data.getMyMatches.map((item, index) => {
              return (
                <View style={styles.card} key={index}>
                  <TouchableOpacity
                    style={styles.avatarContainer}
                    onPress={() =>
                      this.props.navigation.navigate(
                        'Details',
                        {
                          username: `${
                            item.groom.cv.username
                          }`,
                          id: item.groom._id
                        }
                      )
                    }
                  >
                    <Avatar
                      large
                      rounded
                      source={{ uri: item.groom.cv.avatar }}
                      activeOpacity={0.5}
                    />
                    <Text style={styles.name}>
                      {item.groom.cv.username}
                    </Text>
                  </TouchableOpacity>

                  {gender === 'female' ? (
                    <View style={styles.avatarContainer}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.team.sistersAvatar
                        }}
                        activeOpacity={0.5}
                      />
                      <Text style={styles.username}>
                        {item.team.sistersUsername}
                      </Text>
                    </View>
                  ) : (
                    <View style={styles.avatarContainer}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.team.brothersAvatar
                        }}
                        activeOpacity={0.5}
                      />
                      <Text style={styles.username}>
                        {item.team.brothersUsername}
                      </Text>
                    </View>
                  )}
                  <TouchableOpacity
                    style={styles.avatarContainer}
                  >
                    <Avatar
                      large
                      rounded
                      source={{ uri: item.bride.cv.avatar }}
                      activeOpacity={0.5}
                    />
                    <Text style={styles.name}>
                      {item.bride.cv.username}
                    </Text>
                  </TouchableOpacity>
                </View>
              )
            })}
          </ScrollView>

          <ScrollView tabLabel="Match-Makers">
            <ScrollableTabView>
              <ScrollView tabLabel="Approved">
                {myMatchers.myMatchers.map(
                  (item, index) => {
                    return gender === 'female' ? (
                      <View style={styles.card} key={index}>
                        <View
                          style={styles.avatarContainer}
                        >
                          <Avatar
                            large
                            rounded
                            source={{
                              uri: item.team.sistersAvatar
                            }}
                            activeOpacity={0.5}
                          />
                          <Text style={styles.name}>
                            {item.team.sistersUsername}
                          </Text>
                        </View>
                        <Button
                          buttonStyle={styles.button}
                          title="Remove"
                          titleStyle={styles.buttonText}
                          onPress={() => {
                            this.props.removePermission({
                              variables: {
                                _id: item._id
                              },
                              refetchQueries: [
                                {
                                  query: myPendingMatchers_QUERY
                                },
                                { query: myMatchers_QUERY }
                              ]
                            })
                          }}
                        />
                      </View>
                    ) : (
                      <View style={styles.card} key={index}>
                        <View
                          style={styles.avatarContainer}
                        >
                          <Avatar
                            large
                            rounded
                            source={{
                              uri: item.team.brothersAvatar
                            }}
                            activeOpacity={0.5}
                          />
                          <Text style={styles.name}>
                            {item.team.brothersUsername}
                          </Text>
                        </View>
                        <Button
                          buttonStyle={styles.button}
                          title="Remove"
                          titleStyle={styles.buttonText}
                          onPress={() => {
                            this.props.removePermission({
                              variables: {
                                _id: item._id
                              },
                              refetchQueries: [
                                {
                                  query: myPendingMatchers_QUERY
                                },
                                { query: myMatchers_QUERY }
                              ]
                            })
                          }}
                        />
                      </View>
                    )
                  }
                )}
              </ScrollView>
              <ScrollView tabLabel="Requests">
                {pending.myPendingMatchers.map(
                  (item, index) => {
                    return gender === 'female' ? (
                      <View style={styles.card} key={index}>
                        <View
                          style={styles.avatarContainer}
                        >
                          <Avatar
                            large
                            rounded
                            source={{
                              uri: item.team.sistersAvatar
                            }}
                            activeOpacity={0.5}
                          />
                          <Text style={styles.name}>
                            {item.team.sistersUsername}
                          </Text>
                        </View>
                        <Button
                          buttonStyle={styles.button}
                          title="Approve"
                          titleStyle={styles.buttonText}
                          onPress={() => {
                            this.props.mutate({
                              variables: {
                                _id: item._id
                              },
                              refetchQueries: [
                                {
                                  query: myPendingMatchers_QUERY
                                },
                                { query: myMatchers_QUERY }
                              ]
                            })
                          }}
                        />
                      </View>
                    ) : (
                      <View style={styles.card} key={index}>
                        <View
                          style={styles.avatarContainer}
                        >
                          <Avatar
                            large
                            rounded
                            source={{
                              uri: item.team.brothersAvatar
                            }}
                            activeOpacity={0.5}
                          />
                          <Text style={styles.name}>
                            {item.team.brothersUsername}
                          </Text>
                        </View>
                        <Button
                          buttonStyle={styles.button}
                          title="Approve"
                          titleStyle={styles.buttonText}
                          onPress={() => {
                            this.props.mutate({
                              variables: {
                                _id: item._id
                              },
                              refetchQueries: [
                                {
                                  query: myPendingMatchers_QUERY
                                },
                                { query: myMatchers_QUERY }
                              ]
                            })
                          }}
                        />
                      </View>
                    )
                  }
                )}
              </ScrollView>
              <ScrollView tabLabel="List">
                {approvedTeams.approvedTeams.map(
                  (item, index) => {
                    return gender === 'female' ? (
                      <View style={styles.card} key={index}>
                        <View
                          style={styles.avatarContainer}
                        >
                          <Avatar
                            large
                            rounded
                            source={{
                              uri: item.sistersAvatar
                            }}
                            activeOpacity={0.5}
                          />
                          <Text style={styles.name}>
                            {item.sistersUsername}
                          </Text>
                        </View>
                        <Button
                          buttonStyle={styles.button}
                          title="Ask for help"
                          titleStyle={styles.buttonText}
                          onPress={team => {
                            this._onHelpPress(item.team)
                          }}
                        />
                      </View>
                    ) : (
                      <View style={styles.card} key={index}>
                        <View
                          style={styles.avatarContainer}
                        >
                          <Avatar
                            large
                            rounded
                            source={{
                              uri: item.brothersAvatar
                            }}
                            activeOpacity={0.5}
                          />
                          <Text style={styles.name}>
                            {item.brothersUsername}
                          </Text>
                        </View>
                        <Button
                          buttonStyle={styles.button}
                          title="Ask for Help"
                          titleStyle={styles.buttonText}
                          onPress={team => {
                            this._onHelpPress(item.team)
                          }}
                        />
                      </View>
                    )
                  }
                )}
              </ScrollView>
            </ScrollableTabView>
          </ScrollView>
        </ScrollableTabView>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
        {this.props.hasPaid ? null : (
          <View style={styles.absolute}>
            <Text style={styles.blurredText}>
              For subscribers only
            </Text>
            <View style={{ marginTop: 10 }} />
            <Button
              backgroundColor="#314e79"
              rounded={true}
              title="Ask a matchmaker to help you"
              onPress={this._onPaymentPress}
            />
            <Button
              backgroundColor="#314e79"
              title="Check if your payment has gone through"
              rounded={true}
              buttonStyle={{ marginTop: height(2) }}
              onPress={this._refresh}
            />
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  card: {
    backgroundColor: '#E3D5FC',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 5,
    marginTop: 5,
    borderRadius: 7,
    height: height(18)
  },
  avatarContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  textContainer: {
    margin: 20
  },
  username: {
    fontWeight: '900',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue'
  },
  name: {
    fontWeight: '700',
    fontSize: 20,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginTop: 5
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(211, 233, 240, 0.78)'
  },
  blurredText: {
    color: '#314e79',
    fontSize: 26,
    fontWeight: '700'
  },
  button: {
    backgroundColor: '#314e79',
    borderRadius: 7
  },
  buttonText: {
    fontWeight: '500',
    fontSize: 12,
    color: '#142512',
    fontFamily: 'Helvetica Neue'
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(211, 233, 240, 0.78)'
  },
  blurredText: {
    color: '#314e79',
    fontSize: 26,
    fontWeight: '700'
  }
})

export default compose(
  connect(state => ({
    userId: state.user.userId,
    isApproved: state.user.approved,
    gender: state.user.gender,
    hasPaid: state.payment.hasPaid
  })),
  graphql(matches_QUERY),
  graphql(myMatchers_QUERY, {
    name: 'myMatchers'
  }),
  graphql(approvedTeams_Query, {
    name: 'approvedTeams'
  }),
  graphql(myPendingMatchers_QUERY, {
    name: 'pending'
  }),
  graphql(user_QUERY, {
    name: 'userQuery'
  }),
  graphql(GIVE_PERMISSION_MUTATION),
  graphql(REMOVE_PERMISSION_MUTATION, {
    name: 'removePermission'
  }),
  graphql(REQ_TO_MATCH_MUTATION, {
    name: 'reqToMatch'
  }),
  graphql(MATCH_SEEN_MUTATION, {
    name: 'makeSeen'
  })
)(MatchScreen)
