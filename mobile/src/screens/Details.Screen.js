import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  Image,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
  Dimensions,
  Platform
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import { connect } from 'react-redux'
import {
  Button,
  Badge,
  Divider,
  Icon
} from 'react-native-elements'
import ImageCarousel from 'react-native-image-carousel'
import DropdownAlert from 'react-native-dropdownalert'
import ZoomImage from 'react-native-zoom-image'
import _ from 'lodash'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'
import OneSignal from 'react-native-onesignal'

import GETONECV_QUERY from '../graphql/queries/getOneCv.query'
import myChats_QUERY from '../graphql/queries/getMyChats.query'
import photoReq_QUERY from '../graphql/queries/getOnePhotoReq.query'
import LIST_QUERY from '../graphql/queries/list.query'
import whoCanSeeMe_QUERY from '../graphql/queries/whoCanSeeMe.query'
import myFavs_QUERY from '../graphql/queries/myFavs.query'

import CREATECHATROOM_MUTATION from '../graphql/mutation/createChatRoom.mutation'
import CREATEFAV_MUTATION from '../graphql/mutation/createFav.mutation'
import createProfileView_MUTATION from '../graphql/mutation/createProfileView.mutation'
import photoReq_MUTATION from '../graphql/mutation/makePhotoReq.mutation'
import removePhotoReq_MUTATION from '../graphql/mutation/removePhotoReq.mutation'
import accept_MUTATION from '../graphql/mutation/acceptPhotoReq.mutation'

import { insertChatId } from '../actions/chat.action'

import Header from '../components/header'
import FormScreen from '../screens/Form.Screen'
import Loader from '../components/loader'

const WINDOW_WIDTH = Dimensions.get('window').width
const BASE_PADDING = 10

class DetailsScreen extends Component {
  state = {
    me: false,
    refreshing: false
  }

  componentWillMount() {
    this._checkIfCurrentUser()
  }

  componentDidMount() {
    if (
      !this.state.me &&
      this.props.userId != '5b5d4e723db8a4001ffc5fdb'
    ) {
      this._createProfileView()
    }
  }

  _checkIfCurrentUser = async () => {
    if (
      this.props.userId ===
      this.props.navigation.state.params.id
    ) {
      await this.setState({
        me: true
      })
    }
  }

  _createProfileView = async () => {
    await this.props
      .profileView({
        variables: {
          _id: this.props.navigation.state.params.id
        }
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  _onChatPress = async () => {
    const {
      id,
      username
    } = this.props.navigation.state.params
    const ifExists = _.find(this.props.chatList, [
      'userId',
      id
    ])
    if (ifExists === undefined) {
      const { data } = await this.props.createChatRoom({
        variables: {
          _id: id
        },
        refetchQueries: [{ query: myChats_QUERY }]
      })
      await this.props.insertChatId(
        id,
        data.createChatRoom._id
      )
      this.props.navigation.navigate('Messages', {
        conversationId: data.createChatRoom._id,
        username: username
      })
    } else {
      this.props.navigation.navigate('Messages', {
        conversationId: ifExists.chatRoomId,
        username: username
      })
    }
  }

  _onFavPress = async () => {
    await this.props
      .makeFav({
        variables: {
          _id: this.props.navigation.state.params.id
        },
        refetchQueries: [{ query: myFavs_QUERY }]
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  _makePhotoReq = async () => {
    await this.props
      .makePhotoReq({
        variables: {
          _id: this.props.navigation.state.params.id
        },
        refetchQueries: [
          {
            query: photoReq_QUERY,
            variables: {
              _id: this.props.navigation.state.params.id
            }
          }
        ]
      })
      .catch(err => {
        console.log(`${err}`)
      })
    this.dropdown.alertWithType(
      'success',
      'Success',
      'Photo Notification Sent'
    )
  }

  renderAvatar = () => {
    const {
      pic,
      currentLocation,
      avatar
    } = this.props.data.getOneCv
    return (
      <View style={styles.photoContainer}>
        <Image
          style={styles.avatar}
          source={{ uri: avatar }}
        />
        <View style={styles.locationRow}>
          <Icon
            name="dot-single"
            type="entypo"
            color="#42f125"
          />
          <Text style={styles.locationText}>
            {currentLocation}
          </Text>
        </View>
      </View>
    )
  }

  renderImage = () => {
    const {
      pic,
      currentLocation,
      avatar
    } = this.props.data.getOneCv

    return (
      <View style={styles.sliderContainer}>
        <ScrollView horizontal={true}>
          {pic.map((item, index) => {
            return (
              <ZoomImage
                key={index}
                source={{ uri: item }}
                imgStyle={styles.image}
                duration={200}
                enableScaling={false}
              />
            )
          })}
        </ScrollView>
        <View style={styles.locationRow}>
          <Icon
            name="dot-single"
            type="entypo"
            color="#42f125"
          />
          <Text style={styles.locationText}>
            {currentLocation}
          </Text>
        </View>
      </View>
    )
  }

  renderIconContainer = () => {
    const {
      photoPermission,
      navigation,
      userId
    } = this.props

    return (
      <View style={styles.iconsContainer}>
        <TouchableOpacity
          style={styles.icon}
          onPress={this._onChatPress}
        >
          <Image
            source={require('../../assets/icons/Chat.png')}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.icon}
          onPress={this._onFavPress}
        >
          <Image
            source={require('../../assets/icons/Favourite.png')}
          />
        </TouchableOpacity>

        {photoPermission.getOnePhotoReq === null ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._makePhotoReq}
          >
            <Image
              source={require('../../assets/icons/Photo.png')}
            />
          </TouchableOpacity>
        ) : photoPermission.getOnePhotoReq.approved ===
        true ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={async () => {
              await this.props
                .removePhotoReq({
                  variables: {
                    _id: photoPermission.getOnePhotoReq._id
                  },
                  refetchQueries: [
                    {
                      query: GETONECV_QUERY,
                      variables: {
                        _id: navigation.state.params.id
                      }
                    },
                    {
                      query: photReq_QUERY,
                      variables: {
                        _id: navigation.state.params.id
                      }
                    },
                    { query: LIST_QUERY },
                    { query: whoCanSeeMe_QUERY }
                  ]
                })
                .catch(err => {
                  console.log(`${err}`)
                })
              this.dropdown.alertWithType(
                'success',
                'Success',
                'Photo permission removed'
              )
            }}
          >
            <Image
              source={require('../../assets/icons/remove.png')}
            />
          </TouchableOpacity>
        ) : photoPermission.getOnePhotoReq.receiversId ===
        userId ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={async () => {
              await this.props
                .acceptPhotoReq({
                  variables: {
                    _id: photoPermission.getOnePhotoReq._id
                  },
                  refetchQueries: [
                    {
                      query: GETONECV_QUERY,
                      variables: {
                        _id: navigation.state.params.id
                      }
                    },
                    {
                      query: photReq_QUERY,
                      variables: {
                        _id: navigation.state.params.id
                      }
                    },
                    { query: LIST_QUERY },
                    { query: whoCanSeeMe_QUERY }
                  ]
                })
                .catch(err => {
                  console.log(`${err}`)
                })
              this.dropdown.alertWithType(
                'success',
                'Success',
                'Photo Request Accepted'
              )
            }}
          >
            <Image
              source={require('../../assets/icons/acceptReq.png')}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.icon}
            onPress={() => {
              this.dropdown.alertWithType(
                'info',
                'Info',
                'Photo Request Already Sent'
              )
            }}
          >
            <Image
              source={require('../../assets/icons/reqSent.png')}
            />
          </TouchableOpacity>
        )}
      </View>
    )
  }

  renderUnSubbedView = () => {
    return (
      <TouchableOpacity
        style={styles.iconsContainer}
        onPress={this._onPaymentPress}
      >
        <Image
          source={require('../../assets/subscriberOnly.png')}
        />
      </TouchableOpacity>
    )
  }

  _onPaymentPress = () => {
    this.props.navigation.navigate('Payment')
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.props.photoPermission.refetch()
    await this.props.data.refetch()
    this.setState({ refreshing: false })
  }

  render() {
    if (this.state.me) {
      return <FormScreen />
    }
    if (
      this.props.data.loading ||
      this.props.list.loading ||
      this.props.photoPermission.loading ||
      this.props.whoCanSeeMe.loading
    ) {
      return <Loader />
    }

    const {
      age,
      pic,
      profession,
      education,
      homeDistrict,
      currentLocation,
      beard,
      height,
      hifzLevel,
      aboutMe,
      prayer,
      gender,
      practisingSince,
      avatar,
      photoApproved
    } = this.props.data.getOneCv

    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.scrollView}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          {_.includes(photoApproved, this.props.userId)
            ? this.renderImage()
            : this.renderAvatar()}

          {this.props.paid === true
            ? this.renderIconContainer()
            : this.renderUnSubbedView()}

          <View style={styles.badgeContainer}>
            <View style={styles.badgeRow}>
              <Badge containerStyle={styles.badge}>
                <Text style={styles.badgeText}>{age}</Text>
              </Badge>
              <Badge
                containerStyle={styles.badge}
                wrapperStyle={styles.badgeWrapper}
              >
                <Text style={styles.badgeText}>
                  {profession}
                </Text>
              </Badge>
              <Badge
                containerStyle={styles.badge}
                wrapperStyle={styles.badgeWrapper}
              >
                <Text style={styles.badgeText}>
                  {education}
                </Text>
              </Badge>
              <Badge
                containerStyle={styles.badge}
                wrapperStyle={styles.badgeWrapper}
              >
                <Text style={styles.badgeText}>
                  {homeDistrict}
                </Text>
              </Badge>
            </View>

            <View style={styles.badgeRow}>
              <Badge containerStyle={styles.badge}>
                <Text style={styles.badgeText}>
                  {beard}
                </Text>
              </Badge>
              <Badge
                containerStyle={styles.badge}
                wrapperStyle={styles.badgeWrapper}
              >
                <Text style={styles.badgeText}>
                  {practisingSince}
                </Text>
              </Badge>
              <Badge
                containerStyle={styles.badge}
                wrapperStyle={styles.badgeWrapper}
              >
                <Text style={styles.badgeText}>
                  {height}
                </Text>
              </Badge>
            </View>
            <View style={styles.badgeRow}>
              <Badge
                containerStyle={styles.badge}
                wrapperStyle={styles.badgeWrapper}
              >
                <Text style={styles.badgeText}>
                  {hifzLevel}
                </Text>
              </Badge>
              <Badge
                containerStyle={styles.badge}
                wrapperStyle={styles.badgeWrapper}
              >
                <Text style={styles.badgeText}>
                  {prayer}
                </Text>
              </Badge>
            </View>
            <Text style={styles.heading}>About Me</Text>
            <Text style={styles.aboutMe}>{aboutMe}</Text>
          </View>
        </ScrollView>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    )
  }
}

export default compose(
  connect(
    state => ({
      userId: state.user.userId,
      paid: state.payment.hasPaid,
      chatList: state.chat.list
    }),
    { insertChatId }
  ),
  graphql(GETONECV_QUERY, {
    options: ownProps => {
      return {
        variables: {
          _id: ownProps.navigation.state.params.id
        }
      }
    }
  }),
  graphql(CREATECHATROOM_MUTATION, {
    name: 'createChatRoom'
  }),
  graphql(photoReq_QUERY, {
    name: 'photoPermission',
    options: ownProps => {
      return {
        variables: {
          _id: ownProps.navigation.state.params.id
        }
      }
    }
  }),
  graphql(CREATEFAV_MUTATION, { name: 'makeFav' }),
  graphql(photoReq_MUTATION, { name: 'makePhotoReq' }),
  graphql(LIST_QUERY, { name: 'list' }),
  graphql(whoCanSeeMe_QUERY, { name: 'whoCanSeeMe' }),
  graphql(removePhotoReq_MUTATION, {
    name: 'removePhotoReq'
  }),
  graphql(createProfileView_MUTATION, {
    name: 'profileView'
  }),
  graphql(accept_MUTATION, { name: 'acceptPhotoReq' })
)(DetailsScreen)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  scrollView: {
    flex: 1,
    backgroundColor: 'white'
  },
  photoContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  sliderContainer: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  text: {
    color: 'black'
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: wp('5.3%')
  },
  iconsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: hp('2.5%')
  },
  badgeContainer: {
    marginTop: hp('.74%'),
    marginBottom: hp('1.5%')
  },
  badgeRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: hp('.74%')
  },
  aboutMe: {
    color: 'black',
    fontWeight: '300',
    fontSize: hp('3%'),
    fontFamily: 'Helvetica Neue',
    marginTop: hp('.45%'),
    marginHorizontal: wp('4%')
  },
  heading: {
    color: 'black',
    fontWeight: '500',
    fontSize: hp('2.5%'),
    fontFamily: 'Helvetica Neue',
    marginTop: hp('3%'),
    marginHorizontal: wp('4%')
  },
  locationRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatar: {
    width: Platform.OS === 'ios' ? wp('53%') : wp('53%'),
    height: Platform.OS === 'ios' ? hp('30%') : hp('35%'),
    marginTop: hp('2%'),
    padding: hp('4%')
  },
  icon: {
    width: Platform.OS === 'ios' ? wp('19.4%') : wp('22%'),
    height: Platform.OS === 'ios' ? hp('10.5%') : hp('13%'),
    marginHorizontal:
      Platform.OS === 'ios' ? wp('2.5%') : wp('1.5%')
  },
  image: {
    width: wp('53%'),
    height: hp('29%')
  },
  badge: {
    backgroundColor: '#5D4A8F'
  },
  badgeWrapper: {
    marginLeft: wp('1.33%')
  },
  badgeText: {
    color: 'white',
    fontSize: hp('2.5%')
  },
  locationText: {
    fontSize: hp('2.5%'),
    color: '#6d7774'
  }
})
