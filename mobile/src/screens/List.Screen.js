import React, { Component } from 'react'
import { graphql, compose } from 'react-apollo'
import {
  FlatList,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Platform
} from 'react-native'
import {
  Card,
  ListItem,
  Button,
  Divider,
  Avatar,
  Badge
} from 'react-native-elements'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

import LIST_QUERY from '../graphql/queries/list.query'
import USER_QUERY from '../graphql/queries/currentUser.query'
import Loader from '../components/loader'

class ListScreen extends Component {
  async componentWillMount() {
    await this.props.userData.refetch()
    await this.props.data.refetch()
  }

  render() {
    const { data } = this.props

    if (data.loading) {
      return <Loader />
    }
    return (
      <View style={styles.container}>
        <FlatList
          data={data.list}
          keyExtractor={item => item._id}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('Details', {
                  username: `${item.cv.username}`,
                  id: item._id
                })
              }
            >
              <View style={styles.card}>
                <Image
                  style={styles.avatar}
                  source={
                    _.includes(
                      item.cv.photoApproved,
                      this.props.userId
                    )
                      ? { uri: item.cv.pic[0] }
                      : { uri: item.cv.avatar }
                  }
                />

                <View style={styles.textContainer}>
                  <Text style={styles.username}>
                    {item.cv.username}
                  </Text>
                  <Text style={styles.subtitle}>
                    {item.cv.profession}
                  </Text>
                  <Text style={styles.subtitle}>
                    {item.cv.age}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: wp('3.2%'),
    marginVertical: hp('.5%')
  },
  textContainer: {
    marginHorizontal: wp('5.3%'),
    marginVertical: hp('3%')
  },
  username: {
    fontWeight: '900',
    fontSize: hp('2.5%'),
    color: '#4990E2',
    fontFamily: 'Helvetica Neue'
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(211, 233, 240, 0.78)'
  },
  avatar: {
    marginLeft: wp('2.6%'),
    width: Platform.OS === 'ios' ? wp('18%') : wp('18%'),
    height: Platform.OS === 'ios' ? hp('10%') : hp('11%')
  },
  subtitle: {
    color: '#696456',
    fontSize: hp('2%')
  }
})

export default compose(
  connect(state => ({
    userId: state.user.userId
  })),
  graphql(LIST_QUERY),
  graphql(USER_QUERY, { name: 'userData' })
)(ListScreen)
