import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  ScrollView
} from 'react-native'
import { graphql, compose, withApollo } from 'react-apollo'
import {
  Avatar,
  Divider,
  Badge
} from 'react-native-elements'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

import Header from '../components/header'
import Loader from '../components/loader'
import whoViewedMe_QUERY from '../graphql/queries/whoViewedMe.query'
import getViewNotifications_QUERY from '../graphql/queries/getViewNotification.query'
import makeSeen_MUTATION from '../graphql/mutation/makeViewNotiSeen.mutation'
import {
  incViewNotification,
  decViewNotification
} from '../actions/notification.action'

class ProfileViewsScreen extends Component {
  componentWillMount() {
    this.props.navigation.addListener('didFocus', () => {
      this.props.data.refetch()
      this.props
        .mutate({
          variables: {
            _id: this.props.userId
          },
          refetchQueries: [
            {
              query: getViewNotifications_QUERY,
              variables: {
                receiversId: this.props.userId
              }
            }
          ]
        })
        .catch(err => {
          console.log(`${err}`)
        })
    })
  }

  render() {
    if (this.props.data.loading) {
      return <Loader />
    }
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.props.data.whoViewedMe.map(
            (item, index) => {
              return (
                <View key={index}>
                  <View style={styles.avatarContainer}>
                    <Avatar
                      medium
                      rounded
                      onPress={() =>
                        this.props.navigation.navigate(
                          'Details',
                          {
                            username: `${
                              item.visitor.cv.username
                            }`,
                            id: item.visitor._id
                          }
                        )
                      }
                      source={
                        _.includes(
                          item.visitor.cv.photoApproved,
                          this.props.userId
                        )
                          ? { uri: item.visitor.cv.pic[0] }
                          : { uri: item.visitor.cv.avatar }
                      }
                      activeOpacity={0.5}
                    />
                    <Text style={styles.text}>
                      {item.visitor.cv.username} has visited{' '}
                      {'\n'}
                      your profile
                    </Text>
                    <Badge
                      containerStyle={{
                        backgroundColor: '#5D4A8F'
                      }}
                    >
                      <Text style={{ color: 'white' }}>
                        {item.times} times
                      </Text>
                    </Badge>
                  </View>
                </View>
              )
            }
          )}
        </ScrollView>
      </View>
    )
  }
}

export default withApollo(
  compose(
    connect(state => ({
      userId: state.user.userId
    })),
    graphql(whoViewedMe_QUERY),
    graphql(makeSeen_MUTATION)
  )(ProfileViewsScreen)
)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  textInvisible: {
    color: 'white'
  },
  avatarContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginLeft: wp('2.7%'),
    marginVertical: hp('2.5%')
  },
  text: {
    fontWeight: '400',
    fontSize: hp('2.7%'),
    marginBottom: hp('.2%'),
    textAlign: 'left',
    width: wp('50%')
  }
})
