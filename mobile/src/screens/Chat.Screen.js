import React, { Component } from 'react'
import { graphql, compose } from 'react-apollo'
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
  Image,
  Platform
} from 'react-native'
import { connect } from 'react-redux'
import { Avatar, Button } from 'react-native-elements'
import _ from 'lodash'
import moment from 'moment'
import { width, height } from 'react-native-dimension'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

import Loader from '../components/loader'
import Header from '../components/header'

import myChats_QUERY from '../graphql/queries/getMyChats.query'
import user_QUERY from '../graphql/queries/currentUser.query'
import getMsgNoti_QUERY from '../graphql/queries/getMessageNotifications.query'
import msgNoti_subscription from '../graphql/subscriptions/msgNotification.subscription'
import { subbed, unSubbed } from '../actions/payment.action'
import getTime from '../utils/time'

class ChatScreen extends Component {
  state = {
    text: '',
    refreshing: false,
    lastM: ''
  }

  componentWillMount() {
    this.props.navigation.addListener('didFocus', () => {
      this.props.chatRooms.refetch()

      let notificationArray = this.props.msgNoti
        .getMessageNotifications

      let lastMessage = _.chain(notificationArray)
        .groupBy('conversationId')
        .map(function(value) {
          return _.last(value)
        })
        .value()

      this.setState({ lastM: lastMessage })

      this.props.msgNoti.subscribeToMore({
        document: msgNoti_subscription,
        variables: {
          receiverId: this.props.userId
        },
        updateQuery: (prev, { subscriptionData }) => {
          if (!subscriptionData.data) {
            return prev
          }
          const newNotification =
            subscriptionData.data.msgNotiAdded

          // ................own code...............
          let getMessageNotifications = [
            { ...newNotification },
            ...prev.getMessageNotifications
          ]

          let lastMessage = _.chain(getMessageNotifications)
            .groupBy('conversationId')
            .map(function(value) {
              return _.last(value)
            })
            .value()

          this.setState({ lastM: lastMessage })
          // ................own code...............

          return {
            getMessageNotifications: [
              { ...newNotification },
              ...prev.getMessageNotifications
            ]
          }
        }
      })
    })
  }

  _onPaymentPress = () => {
    this.props.navigation.navigate('Payment')
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.props.chatRooms.refetch()
    this.setState({ refreshing: false })
  }

  _refresh = async () => {
    let { data } = await this.props.userQuery.refetch()
    if (data.userInfo.hasPaid) {
      await this.props.dispatch(subbed())
    } else {
      await this.props.dispatch(unSubbed())
    }
  }

  render() {
    if (
      this.props.chatRooms.loading ||
      this.props.msgNoti.loading
    ) {
      return <Loader />
    }

    return (
      <View style={styles.container}>
        <View style={styles.scrollContainer}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            {this.props.chatRooms.getMyChats.map(
              (item, index) => {
                return (
                  <TouchableOpacity
                    style={styles.avatarContainer}
                    key={index}
                    onPress={() => {
                      this.state.lastM.pop(
                        _.find(this.state.lastM, {
                          conversationId: item._id
                        })
                      )
                      this.props.navigation.navigate(
                        'Messages',
                        {
                          conversationId: item._id,
                          username: `${
                            item.inviter._id ===
                            this.props.userId
                              ? item.responder.cv.username
                              : item.inviter.cv.username
                          }`
                        }
                      )
                    }}
                  >
                    <Image
                      style={styles.avatar}
                      source={
                        item.inviter._id ===
                        this.props.userId
                          ? _.includes(
                              item.responder.cv
                                .photoApproved,
                              this.props.userId
                            )
                            ? {
                                uri:
                                  item.responder.cv.pic[0]
                              }
                            : {
                                uri:
                                  item.responder.cv.avatar
                              }
                          : _.includes(
                              item.inviter.cv.photoApproved,
                              this.props.userId
                            )
                            ? {
                                uri: item.inviter.cv.pic[0]
                              }
                            : {
                                uri: item.inviter.cv.avatar
                              }
                      }
                    />
                    <View style={styles.headingContainer}>
                      <Text style={styles.heading}>
                        {item.inviter._id ===
                        this.props.userId
                          ? item.responder.cv.username
                          : item.inviter.cv.username}
                      </Text>

                      <Text style={styles.lastMessage}>
                        {_.find(this.state.lastM, {
                          conversationId: item._id
                        }) === undefined
                          ? 'last message'
                          : _.find(this.state.lastM, {
                              conversationId: item._id
                            }).message.substring(0, 20)}...
                      </Text>
                    </View>

                    <View style={styles.endContainer}>
                      <Text style={styles.timeText}>
                        {_.find(this.state.lastM, {
                          conversationId: item._id
                        }) === undefined
                          ? ''
                          : getTime(
                              _.find(this.state.lastM, {
                                conversationId: item._id
                              }).createdAt
                            )}
                      </Text>
                      <Text style={styles.seenText}>
                        {_.find(this.state.lastM, {
                          conversationId: item._id
                        }) === undefined
                          ? 'seen'
                          : 'Unseen'}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )
              }
            )}
          </ScrollView>
          <Text style={styles.textInvisible}>
            {
              this.props.msgNoti.getMessageNotifications
                .length
            }
          </Text>
        </View>
        {this.props.userType === 'matchMaker' ? null : this
          .props.hasPaid ? null : (
          <View style={styles.absolute}>
            <Text style={styles.blurredText}>
              For subscribers only
            </Text>
            <View style={{ marginTop: 10 }} />
            <Button
              backgroundColor="#314e79"
              rounded={true}
              title="Start Chatting"
              onPress={this._onPaymentPress}
            />
            <Button
              backgroundColor="#314e79"
              title="Check if your payment has gone through"
              rounded={true}
              buttonStyle={{ marginTop: height(2) }}
              onPress={this._refresh}
            />
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  // ...........top horizontal container.........
  scrollContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  textInvisible: {
    color: '#e2f7ff'
  },
  heading: {
    fontWeight: '900',
    fontSize: hp('2.5%'),
    color: '#af39c7',
    fontFamily: 'Helvetica Neue'
  },
  avatarContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: hp('2%'),
    marginLeft: wp('2.6%')
  },
  headingContainer: {
    marginLeft: wp('4%'),
    marginBottom: hp('1.5%'),
    marginTop: hp('1.5%'),
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: wp('40%')
  },
  endContainer: {
    marginLeft: wp('5%'),
    marginBottom: hp('1.5%'),
    marginTop: hp('1.5%'),
    flexDirection: 'column'
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(211, 233, 240, 0.78)'
  },
  blurredText: {
    color: '#314e79',
    fontSize: hp('4%'),
    fontWeight: '700'
  },
  avatar: {
    marginLeft: wp('2.6%'),
    width: Platform.OS === 'ios' ? wp('18%') : wp('18%'),
    height: Platform.OS === 'ios' ? hp('10%') : hp('11%')
  },
  lastMessage: {
    fontWeight: '400',
    fontSize: hp('2%'),
    fontFamily: 'Helvetica Neue'
  },
  timeText: {
    fontWeight: '500',
    fontSize: hp('2%'),
    fontFamily: 'Helvetica Neue',
    color: '#4990E2'
  },
  seenText: {
    fontWeight: '300',
    fontSize: hp('2%'),
    fontFamily: 'Helvetica Neue'
  }
})

// ************************************************
//                     export                     *
// ************************************************

export default compose(
  connect(state => ({
    userId: state.user.userId,
    hasPaid: state.payment.hasPaid,
    userType: state.user.userType
  })),
  graphql(myChats_QUERY, {
    name: 'chatRooms'
  }),
  graphql(getMsgNoti_QUERY, {
    name: 'msgNoti'
  }),
  graphql(user_QUERY, {
    name: 'userQuery'
  })
)(ChatScreen)
