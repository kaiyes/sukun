import { gql } from 'react-apollo'

export default gql`
  subscription photoNotificationAdded($receiverId: ID!) {
    photoNotificationAdded(receiverId: $receiverId) {
      seen
    }
  }
`
