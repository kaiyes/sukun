import { gql } from 'react-apollo'

export default gql`
  subscription OnMessageAdded($conversationId: ID!) {
    messageAdded(conversationId: $conversationId) {
      _id
      body
      conversationId
      createdAt
      sender {
        _id
        cv {
          username
          pic
          gender
          photoApproved
        }
      }
    }
  }
`
