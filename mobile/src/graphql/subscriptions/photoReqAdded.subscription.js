import { gql } from 'react-apollo'

export default gql`
  subscription photoReqAdded($receiversId: ID!) {
    photoReqAdded(receiversId: $receiversId) {
      _id
      requester {
        _id
        cv {
          username
          pic
          gender
          photoApproved
        }
      }
    }
  }
`
