import { gql } from 'react-apollo'

export default gql`
  subscription viewNotificationAdded($receiversId: ID!) {
    viewNotificationAdded(receiverId: $receiversId) {
      seen
    }
  }
`
