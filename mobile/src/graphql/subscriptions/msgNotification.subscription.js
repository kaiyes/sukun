import { gql } from 'react-apollo'

export default gql`
  subscription msgNotiAdded($receiverId: ID!) {
    msgNotiAdded(receiverId: $receiverId) {
      _id
      message
      conversationId
      createdAt
      sender {
        cv {
          username
        }
      }
    }
  }
`
