import { gql } from 'react-apollo'

export default gql`
  subscription viewAdded($ownerId: ID!) {
    viewAdded(ownerId: $ownerId) {
      times
      visitor {
        _id
        cv {
          username
          pic
          photoApproved
          gender
        }
      }
    }
  }
`
