import { gql } from 'react-apollo'

export default gql`
  query {
    getMyMatches {
      brideSeen
      groomSeen
      team {
        sistersUsername
        brothersUsername
        sistersAvatar
        brothersAvatar
      }
      bride {
        _id
        cv {
          username
          avatar
          age
          profession
        }
      }
      groom {
        _id
        cv {
          username
          avatar
          age
          profession
        }
      }
    }
  }
`
