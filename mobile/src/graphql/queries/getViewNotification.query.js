import { gql } from 'react-apollo'

export default gql`
  query getViewNotifications($receiversId: ID!) {
    getViewNotifications(receiverId: $receiversId) {
      seen
    }
  }
`
