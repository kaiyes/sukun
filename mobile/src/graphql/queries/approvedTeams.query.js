import { gql } from 'react-apollo'

export default gql`
  query {
    approvedTeams {
      brothersPic
      brothersAvatar
      brothersUsername
      brothersId
      team
      sistersId
      sistersPic
      sistersAvatar
      sistersUsername
    }
  }
`
