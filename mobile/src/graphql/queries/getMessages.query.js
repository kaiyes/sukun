import { gql } from 'react-apollo'

export default gql`
  query getMessages($conversationId: ID!) {
    getMessages(conversationId: $conversationId) {
      _id
      conversationId
      body
      createdAt
      sender {
        _id
        cv {
          username
          pic
          gender
          photoApproved
          avatar
        }
      }
    }
  }
`
