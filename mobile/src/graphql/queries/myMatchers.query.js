import { gql } from 'react-apollo'

export default gql`
  query {
    myMatchers {
      _id
      team {
        _id
        sistersUsername
        brothersUsername
        brothersAvatar
        sistersAvatar
        approved
      }
    }
  }
`
