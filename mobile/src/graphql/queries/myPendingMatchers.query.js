import { gql } from 'react-apollo'

export default gql`
  query {
    myPendingMatchers {
      _id
      team {
        sistersUsername
        brothersUsername
        brothersAvatar
        sistersAvatar
        approved
      }
    }
  }
`
