import { gql } from 'react-apollo'

export default gql`
  {
    userInfo {
      _id
      userType
      hasPaid
      isBanned
      isApproved
      hasCompletedCv
      isRejected
      gender
    }
  }
`
