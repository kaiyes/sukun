import { gql } from 'react-apollo'

export default gql`
  query getOnePhotoReq($_id: ID!) {
    getOnePhotoReq(_id: $_id) {
      _id
      approved
      receiversId
    }
  }
`
