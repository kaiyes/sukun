import { gql } from 'react-apollo'

export default gql`
  query {
    whoCanSeeMe {
      receiver {
        _id
        cv {
          username
          age
          education
          pic
          photoApproved
          profession
          avatar
        }
      }
      requester {
        _id
        cv {
          username
          age
          education
          pic
          photoApproved
          profession
          avatar
        }
      }
    }
  }
`
