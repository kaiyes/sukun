import { gql } from 'react-apollo'

export default gql`
  query {
    list {
      _id
      cv {
        username
        profession
        age
        beard
        gender
        photoApproved
        pic
        avatar
        avatar
      }
    }
  }
`
