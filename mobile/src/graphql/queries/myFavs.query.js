import { gql } from 'react-apollo'

export default gql`
  query {
    getMyFavs {
      likeReceiver {
        _id
        cv {
          username
          age
          education
          pic
          photoApproved
          profession
          avatar
        }
      }
    }
  }
`
