import { gql } from 'react-apollo'

export default gql`
  query {
    getMyPhotoReqs {
      _id
      requester {
        _id
        cv {
          username
          pic
          gender
          photoApproved
          avatar
        }
      }
    }
  }
`
