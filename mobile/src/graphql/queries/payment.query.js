import { gql } from "react-apollo";

export default gql`
  {
    getPayments {
      _id
      amount
      bikash
      user {
        email
      }
    }
  }
`;
