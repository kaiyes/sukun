import { gql } from 'react-apollo'

export default gql`
  query {
    getUnseenMatches {
      brideSeen
      groomSeen
    }
  }
`
