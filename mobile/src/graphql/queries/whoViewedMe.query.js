import { gql } from 'react-apollo'

export default gql`
  {
    whoViewedMe {
      times
      visitor {
        _id
        cv {
          username
          pic
          photoApproved
          gender
          avatar
        }
      }
    }
  }
`
