import { gql } from 'react-apollo'

export default gql`
  mutation {
    makeApprovalFalse {
      _id
      isApproved
    }
  }
`
