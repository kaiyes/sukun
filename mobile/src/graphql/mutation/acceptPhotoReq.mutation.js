import { gql } from 'react-apollo'

export default gql`
  mutation acceptPhotoReq($_id: ID!) {
    acceptPhotoReq(_id: $_id) {
      approved
    }
  }
`
