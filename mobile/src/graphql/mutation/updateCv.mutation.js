import { gql } from 'react-apollo'

export default gql`
  mutation updateCv(
    $age: String
    $height: String
    $profession: String
    $education: String
    $prayer: String
    $practisingSince: String
    $beard: String
    $homeDistrict: String
    $currentLocation: String
    $username: String
    $hifzLevel: String
    $aboutMe: String
    $pic: [String]
    $nid: String
    $avatar: String
  ) {
    updateCv(
      age: $age
      height: $height
      profession: $profession
      education: $education
      prayer: $prayer
      practisingSince: $practisingSince
      beard: $beard
      homeDistrict: $homeDistrict
      currentLocation: $currentLocation
      hifzLevel: $hifzLevel
      aboutMe: $aboutMe
      username: $username
      pic: $pic
      nid: $nid
      avatar: $avatar
    ) {
      _id
      age
      height
      profession
      education
      prayer
      practisingSince
      beard
      homeDistrict
      currentLocation
      hifzLevel
      aboutMe
      username
      pic
    }
  }
`
