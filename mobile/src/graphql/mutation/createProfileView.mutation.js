import { gql } from 'react-apollo'

export default gql`
  mutation createProfileView($_id: ID!) {
    createProfileView(_id: $_id) {
      visitor {
        cv {
          username
        }
      }
    }
  }
`
