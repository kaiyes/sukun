import { gql } from 'react-apollo'

export default gql`
  mutation seenViewNotifications($_id: ID!) {
    seenViewNotifications(_id: $_id) {
      seen
    }
  }
`
