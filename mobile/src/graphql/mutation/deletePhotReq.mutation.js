import { gql } from 'react-apollo'

export default gql`
  mutation deletePhotoReq($_id: ID!) {
    deletePhotoReq(_id: $_id) {
      _id
    }
  }
`
