import { gql } from 'react-apollo'

export default gql`
  mutation createFav($_id: ID!) {
    createFav(_id: $_id) {
      liker {
        cv {
          username
        }
      }
    }
  }
`
