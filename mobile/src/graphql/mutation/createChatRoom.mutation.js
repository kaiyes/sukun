import { gql } from 'react-apollo'

export default gql`
  mutation createChat($_id: ID!) {
    createChatRoom(_id: $_id) {
      _id
      inviter {
        cv {
          username
        }
      }
      responder {
        cv {
          username
        }
      }
    }
  }
`
