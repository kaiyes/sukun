import { gql } from 'react-apollo'

export default gql`
  mutation createPayment(
    $amount: String!
    $bikash: String!
  ) {
    createPayment(amount: $amount, bikash: $bikash) {
      amount
      bikash
      createdAt
    }
  }
`
