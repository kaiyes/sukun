import { gql } from 'react-apollo'

export default gql`
  mutation makePhotoReq($_id: ID!) {
    makePhotoReq(_id: $_id) {
      approved
      requester {
        _id
      }
    }
  }
`
