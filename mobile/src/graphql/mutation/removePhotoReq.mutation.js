import { gql } from 'react-apollo'

export default gql`
  mutation removePhotoReq($_id: ID!) {
    removePhotoReq(_id: $_id) {
      approved
    }
  }
`
