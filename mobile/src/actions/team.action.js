export default function team(teamNumber, partnerName) {
  return {
    type: 'TEAM',
    teamNumber,
    partnerName
  }
}
