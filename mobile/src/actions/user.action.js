import { AsyncStorage } from 'react-native'

export function logIn() {
  return {
    type: 'LOGIN'
  }
}

export function completedCv() {
  return {
    type: 'CV'
  }
}

export function userId(id, gender, userType, approved) {
  return {
    type: 'USERID',
    id,
    gender,
    userType,
    approved
  }
}

export function logout() {
  return async dispatch => {
    try {
      await AsyncStorage.removeItem('@login:token')
      await AsyncStorage.removeItem('@cv:cv')
      return dispatch({
        type: 'LOGOUT'
      })
    } catch (e) {
      throw e
    }
  }
}
