import { AsyncStorage } from 'react-native'

export function incViewNotification(number) {
  return {
    type: 'INC_VIEW_NOTI',
    number
  }
}

export function decViewNotification() {
  return {
    type: 'DEC_VIEW_NOTI'
  }
}
export function incPhotoNotification(number) {
  return {
    type: 'INC_PHOTO_NOTI',
    number
  }
}

export function decPhotoNotification() {
  return {
    type: 'DEC_PHOTO_NOTI'
  }
}
