export function insertChatId(userId, chatRoomId) {
  return {
    type: 'CHAT_LIST',
    userId,
    chatRoomId
  }
}
