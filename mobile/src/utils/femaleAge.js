let femaleAgeSwitchFunc = age => {
  if (age < 38) {
    return require('../../assets/icons/youngGirl.png')
  } else if (age > 39 && age < 60) {
    return require('../../assets/icons/agedGirl.png')
  }
}
export default femaleAgeSwitchFunc
