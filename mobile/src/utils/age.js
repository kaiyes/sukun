let ageSwitchFunction = age => {
  if (age < 38) {
    return require('../../assets/icons/young.png')
  } else if (age > 39 && age < 60) {
    return require('../../assets/icons/old.png')
  }
}
export default ageSwitchFunction
