import moment from 'moment'

export default (getTimePosted = givenDate => {
  d = moment(givenDate).valueOf()
  const date = new Date(
    typeof d === 'string' ? parseInt(d, 10) : d
  )
  let day
  let year
  const dateMoment = moment(date)
  const isToday = dateMoment.isSame(moment(), 'day')
  const isYesterday = dateMoment.isSame(
    moment().subtract(1, 'day'),
    'day'
  )
  if (isToday) {
    return dateMoment.fromNow()
  } else if (isYesterday) {
    day = 'Yesterday'
  } else {
    day = dateMoment.format('MMMM Do')
  }
  if (!dateMoment.isSame(moment(), 'year'))
    year = `, ${dateMoment.format('YYYY')}`
  return `${day}${year || ''} at ${dateMoment.format(
    'h:mma'
  )}`
})
