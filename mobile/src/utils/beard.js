let beardSwitchFunction = data => {
  switch (data) {
    case 'সুন্নাহ মোতাবেক পূর্ণ দাড়ি':
      return require('../../assets/icons/fullBeard.png')
      break
    case 'এক মুঠোর উপরে কেটে ফেলা':
      return require('../../assets/icons/handful.png')
      break
    case 'দাড়ি ট্রিম করা ট্রিমার দিয়ে':
      return require('../../assets/icons/trimmed.png')
      break
    case 'প্রাকৃতিক ভাবে উঠে না':
      return require('../../assets/icons/noBeard.png')
      break
    case 'শেভ করা':
      return require('../../assets/icons/shaved.png')
      break
    default:
      return require('../../assets/icons/beardLength.png')
      break
  }
}

export default beardSwitchFunction
