let prayerSwitchFunction = data => {
  switch (data) {
    case '৫ ওয়াক্ত সালাহ পড়ি':
      return require('../../assets/icons/5wakt.png')
      break
    case 'মাঝে মাঝে ফজর মিস হয়':
      return require('../../assets/icons/fazrMiss.png')
      break
    case 'শুধু জুমুআ':
      return require('../../assets/icons/jumua.png')
      break
    case 'মাঝে মাঝে সালাহ পড়ি':
      return require('../../assets/icons/rarelyPray.png')
      break
    default:
      return require('../../assets/icons/prayer.png')
      break
  }
}

export default prayerSwitchFunction
