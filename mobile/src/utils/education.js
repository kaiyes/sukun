let eduSwitchFunction = data => {
  switch (data) {
    case 'Ssc':
      return require('../../assets/icons/ssc.png')
      break
    case 'Hsc':
      return require('../../assets/icons/hsc.png')
      break
    case 'Bsc':
      return require('../../assets/icons/bsc.png')
      break
    case 'Hons':
      return require('../../assets/icons/hons.png')
      break
    case 'Beng':
      return require('../../assets/icons/beng.png')
      break
    case 'Meng':
      return require('../../assets/icons/meng.png')
      break
    case 'MBBS':
      return require('../../assets/icons/mbbs.png')
      break
    case 'দাখিল':
      return require('../../assets/icons/dakhil.png')
      break
    case 'ফাজিল':
      return require('../../assets/icons/fajil.png')
      break
    case 'দাওরা':
      return require('../../assets/icons/dawra.png')
      break
    default:
      return require('../../assets/icons/edu.png')
      break
  }
}
export default eduSwitchFunction
