let hijabSwitchFunction = data => {
  switch (data) {
    case 'নিকাব-আবায়া-বোরখা':
      return require('../../assets/icons/nikab.png')
      break
    case 'আবায়া/বোরখা মুখ খোলা':
      return require('../../assets/icons/abaya.png')
      break
    case 'শুধু স্কার্ফ':
      return require('../../assets/icons/scarfOnly.png')
      break
    case 'হিজাব ব্যতীত সাধারণ পোশাক':
      return require('../../assets/icons/noNikab.png')
      break
    default:
      return require('../../assets/icons/abaya.png')
      break
  }
}

export default hijabSwitchFunction
