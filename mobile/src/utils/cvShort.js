export default {
  age,
  height,
  education,
  prayer,
  practisingSince,
  beard,
  homeDistrict,
  currentLocation,
  hifzLevel,
  profession,
  username,
  aboutMe,
  pic,
  nidCopy,
  avatar,
  gender
}
