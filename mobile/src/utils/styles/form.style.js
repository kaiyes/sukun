import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2980b9',
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 5,
    marginRight: 12
  },
  iconContainer2: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 10
  },
  captionContainer2: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginLeft: 10
  },
  iconContainer3: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10
  },
  captionContainer3: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  formContainer: {
    flex: 1,
    marginTop: 5,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonContainer: {
    backgroundColor: 'yellow',
    height: 40
  },
  buttonText: {
    color: 'black',
    fontFamily: 'Helvetica Neue',
    fontSize: 20,
    padding: 8,
    textAlign: 'center',
    fontWeight: '700'
  },
  captionText: {
    color: 'white',
    fontFamily: 'Helvetica Neue',
    fontSize: 14,
    marginTop: 5,
    fontWeight: '700'
  },
  heading: {
    color: 'white',
    fontFamily: 'Helvetica Neue',
    fontSize: 20,
    marginTop: 10,
    fontWeight: '900'
  },
  textInput: {
    borderWidth: 1,
    borderColor: 'white',
    height: 70,
    backgroundColor: 'white',
    borderRadius: 3,
    marginTop: 5,
    fontSize: 17
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#356490'
  },
  icon: {
    width: 90,
    height: 90,
    borderRadius: 45
  },
  emptyIcon: {
    width: 90,
    height: 90,
    borderRadius: 45,
    backgroundColor: '#D7F4F6',
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconText: {
    fontSize: 15,
    fontWeight: '700'
  },
  bigSvg: {
    width: 380,
    height: 500
  }
})
