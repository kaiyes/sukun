import { StyleSheet } from 'react-native'
import { width, height } from 'react-native-dimension'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginLeft: 10,
    marginTop: 10
  },
  iconContainer2: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10
  },
  captionContainer2: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginLeft: 10
  },
  iconContainer3: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10
  },
  captionContainer3: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  formContainer: {
    flex: 1,
    marginTop: 5,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  submitButton: {
    backgroundColor: 'yellow',
    height: height(8)
  },
  buttonText: {
    color: 'black',
    fontFamily: 'Helvetica Neue',
    fontSize: 22,
    padding: height(2),
    textAlign: 'center',
    fontWeight: '700'
  },
  captionText: {
    color: 'white',
    fontFamily: 'Helvetica Neue',
    fontSize: 14,
    marginTop: 5,
    fontWeight: '700'
  },
  captionText: {
    color: 'black',
    fontFamily: 'Helvetica Neue',
    fontSize: 18,
    fontWeight: '400',
    marginLeft: 10
  },
  heading: {
    color: 'black',
    fontSize: 20,
    marginTop: 10,
    fontWeight: '900'
  },
  textInput: {
    borderWidth: 2,
    borderColor: '#356490',
    height: height(30),
    width: width(90),
    backgroundColor: 'white',
    borderRadius: 25,
    marginTop: 5,
    fontSize: 19
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e1eff2'
  },
  icon: {
    width: 90,
    height: 90,
    borderRadius: 45
  },
  form: {
    height: height(8),
    width: width(70),
    borderColor: 'gray',
    borderWidth: 2,
    marginTop: 5,
    borderRadius: 35
  },
  form2: {
    width: width(90),
    marginLeft: width(5),
    marginTop: height(3),
    marginBottom: height(2)
  },
  formContainer2: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginRight: 10
  },
  button: {
    backgroundColor: '#356490',
    width: width(65),
    height: height(8),
    borderColor: 'transparent',
    borderRadius: 35,
    marginTop: 5
  }
})
