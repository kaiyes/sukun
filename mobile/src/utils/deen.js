let deenFunction = data => {
  switch (data) {
    case '৫ বছর এর উপরে':
      return require('../../assets/icons/5year.png')
      break
    case '৩ বছর ধরে':
      return require('../../assets/icons/3year.png')
      break
    case '১ বছর ধরে':
      return require('../../assets/icons/1year.png')
      break
    case 'কয়েক মাস ধরে':
      return require('../../assets/icons/fewMonths.png')
      break
    default:
      return require('../../assets/icons/deen.png')
      break
  }
}

export default deenFunction
