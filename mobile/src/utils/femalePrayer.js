let femalePrayerSwitchFunction = data => {
  switch (data) {
    case '৫ ওয়াক্ত সালাহ পড়ি':
      return require('../../assets/icons/5waktFemale.png')
      break
    case 'মাঝে মাঝে ফজর মিস হয়':
      return require('../../assets/icons/fazrMissFemale.png')
      break
    case 'মাঝে মাঝে সালাহ পড়ি':
      return require('../../assets/icons/rarelyPrayFemale.png')
      break
    default:
      return require('../../assets/icons/5waktFemale.png')
      break
  }
}

export default femalePrayerSwitchFunction
