import React, { Component } from 'react'
import {
  addNavigationHelpers,
  StackNavigator,
  TabNavigator,
  onNavigationStateChange
} from 'react-navigation'
import { withApollo, compose, graphql } from 'react-apollo'
import { AsyncStorage } from 'react-native'

import ApprovalScreen from './screens/approval.screen'
import ChatScreen from './screens/chat.screen'
import MatchScreen from './screens/match.screen'
import PaymentScreen from './screens/payment.screen'
import LogInScreen from './screens/login.screen'
import DashBoardScreen from './screens/dashBoard.screen'
import DetailsScreen from './screens/details.screen'
import MessageScreen from './screens/messages.screen'

import { Icon } from 'react-native-elements'

const Tabs = TabNavigator(
  {
    Approval: {
      screen: ApprovalScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Approval',
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="approval"
            type="material-community"
            color={tintColor}
          />
        )
      })
    },
    Chat: {
      screen: ChatScreen,
      navigationOptions: () => ({
        title: 'Chat',
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="chat"
            type="entypo"
            color={tintColor}
          />
        )
      })
    },
    Payment: {
      screen: PaymentScreen,
      navigationOptions: () => ({
        title: 'Payment',
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="dollar-bill"
            type="foundation"
            color={tintColor}
          />
        )
      })
    },
    Match: {
      screen: MatchScreen,
      navigationOptions: () => ({
        title: 'Match',
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="heart"
            type="entypo"
            color={tintColor}
          />
        )
      })
    }
  },
  {
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    lazy: false,
    tabBarOptions: {
      showIcon: true,
      activeTintColor: 'black',
      inactiveTintColor: '#4990E2',
      style: {
        backgroundColor: 'white',
        paddingVertical: 5
      }
    }
  }
)

const AppMainNav = StackNavigator({
  Home: {
    screen: Tabs
  },
  DashBoard: {
    screen: DashBoardScreen,
    navigationOptions: () => ({
      headerTitle: 'Your DashBoard'
    })
  },
  Details: {
    screen: DetailsScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: `${navigation.state.params.username}`
    })
  },
  Messages: {
    screen: MessageScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: `chat with ${
        navigation.state.params.username
      }`
    })
  }
})

class AppNavigator extends Component {
  state = {
    loggedIn: false
  }
  componentWillMount() {
    this._getUserInfo()
  }

  _getUserInfo = async () => {
    try {
      const token = await AsyncStorage.getItem(
        '@login:token'
      )
      if (token === null) {
        return await this.setState({ loggedIn: false })
      } else {
        return await this.setState({ loggedIn: true })
      }
    } catch (e) {
      console.log(err)
    }
  }

  render() {
    if (!this.state.loggedIn) {
      return <LogInScreen />
    }

    return <AppMainNav />
  }
}

export default AppNavigator
