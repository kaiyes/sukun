import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  Dimensions,
  PixelRatio
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import {
  Avatar,
  Button,
  Badge
} from 'react-native-elements'
import moment from 'moment'
import {
  width,
  height,
  totalSize
} from 'react-native-dimension'
import ScrollableTabView from 'react-native-scrollable-tab-view'

import waitingPayments_QUERY from '../graphql/queries/waitingPayments.query'
import paidUsers_QUERY from '../graphql/queries/paidUsers.query'
import MAKE_PAID_MUTATION from '../graphql/mutation/makePaid.mutation'
import DELETE_RECORD_MUTATION from '../graphql/mutation/deletePaymentRecord.mutation'

class PaymentScreen extends Component {
  state = {
    refreshing: false
  }

  _makePaid = async id => {
    await this.props
      .mutate({
        variables: {
          _id: id
        },
        refetchQueries: [
          { query: waitingPayments_QUERY },
          { query: paidUsers_QUERY }
        ]
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  _deleteRecord = async id => {
    await this.props
      .deleteRecord({
        variables: {
          _id: id
        },
        refetchQueries: [
          { query: waitingPayments_QUERY },
          { query: paidUsers_QUERY }
        ]
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.props.data.refetch()
    await this.props.paidUsers.refetch()
    this.setState({ refreshing: false })
  }

  render() {
    if (
      this.props.data.loading ||
      this.state.refreshing ||
      this.props.paidUsers.loading
    ) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      )
    }
    return (
      <View style={styles.container}>
        <ScrollableTabView>
          <ScrollView
            tabLabel="Waiting"
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            <View style={{ paddingTop: 10 }} />
            {this.props.data.waitingPayments.map(
              (item, index) => {
                return (
                  <View
                    key={index}
                    style={styles.cardContainer}
                  >
                    <TouchableOpacity
                      style={styles.card}
                      onPress={() =>
                        this.props.navigation.navigate(
                          'Details',
                          {
                            username: `${
                              item.user.cv.username
                            }`,
                            id: item.user._id
                          }
                        )
                      }
                    >
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.user.cv.pic[0]
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.user.cv.username}
                      </Text>

                      <Text style={styles.subtitle}>
                        {item.bikash}
                      </Text>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F',
                          marginTop: 5
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.amount} /=
                        </Text>
                      </Badge>
                      <Text style={styles.time}>
                        {moment(item.createdAt).fromNow()}
                      </Text>
                    </TouchableOpacity>
                    <View style={styles.rightCard}>
                      <View style={styles.viewContainer}>
                        <Text style={styles.heading}>
                          hasPaid:{'  '}
                          <Text style={styles.redSubtitle}>
                            {item.user.hasPaid.toString()}
                          </Text>
                        </Text>
                        <Text style={styles.heading}>
                          isBanned:{'  '}
                          <Text style={styles.redSubtitle}>
                            {item.user.isBanned.toString()}
                          </Text>
                        </Text>
                        <Text style={styles.heading}>
                          isApproved:{'  '}
                          <Text style={styles.redSubtitle}>
                            {item.user.isApproved.toString()}
                          </Text>
                        </Text>
                        <Text style={styles.heading}>
                          hasCompletedCv:{'  '}
                          <Text style={styles.redSubtitle}>
                            {item.user.hasCompletedCv.toString()}
                          </Text>
                        </Text>
                      </View>

                      <View style={styles.cardContainer}>
                        <Button
                          buttonStyle={styles.button}
                          title="Make member"
                          titleStyle={styles.buttonText}
                          onPress={() => {
                            this._makePaid(item._id)
                          }}
                        />
                        <Button
                          buttonStyle={styles.redButton}
                          title="Delete"
                          titleStyle={styles.redButtonText}
                          onPress={() => {
                            this._deleteRecord(item._id)
                          }}
                        />
                      </View>
                    </View>
                  </View>
                )
              }
            )}
          </ScrollView>

          <ScrollView
            tabLabel="Approved"
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            <View style={{ paddingTop: 10 }} />
            {this.props.paidUsers.paidUsers.map(
              (item, index) => {
                return (
                  <View
                    key={index}
                    style={styles.cardContainer}
                  >
                    <TouchableOpacity
                      style={styles.card}
                      onPress={() =>
                        this.props.navigation.navigate(
                          'Details',
                          {
                            username: `${
                              item.user.cv.username
                            }`,
                            id: item.user._id
                          }
                        )
                      }
                    >
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.user.cv.pic[0]
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.user.cv.username}
                      </Text>

                      <Text style={styles.subtitle}>
                        {item.bikash}
                      </Text>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F',
                          marginTop: 5
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          {item.amount} /=
                        </Text>
                      </Badge>
                      <Text style={styles.time}>
                        {moment(item.updatedAt).fromNow()}
                      </Text>
                    </TouchableOpacity>
                    <View style={styles.rightCard}>
                      <View style={styles.viewContainer}>
                        <Text style={styles.heading}>
                          hasPaid:{'  '}
                          <Text style={styles.redSubtitle}>
                            {item.user.hasPaid.toString()}
                          </Text>
                        </Text>
                        <Text style={styles.heading}>
                          isBanned:{'  '}
                          <Text style={styles.redSubtitle}>
                            {item.user.isBanned.toString()}
                          </Text>
                        </Text>
                        <Text style={styles.heading}>
                          isApproved:{'  '}
                          <Text style={styles.redSubtitle}>
                            {item.user.isApproved.toString()}
                          </Text>
                        </Text>
                        <Text style={styles.heading}>
                          hasCompletedCv:{'  '}
                          <Text style={styles.redSubtitle}>
                            {item.user.hasCompletedCv.toString()}
                          </Text>
                        </Text>
                      </View>

                      <View style={styles.cardContainer}>
                        <Button
                          buttonStyle={styles.redButton}
                          title="un-member"
                          titleStyle={styles.redButtonText}
                          onPress={() => {
                            this._makePaid(item._id)
                          }}
                        />
                        <Button
                          buttonStyle={styles.redButton}
                          title="Delete"
                          titleStyle={styles.redButtonText}
                          onPress={() => {
                            this._deleteRecord(item._id)
                          }}
                        />
                      </View>
                    </View>
                  </View>
                )
              }
            )}
          </ScrollView>
        </ScrollableTabView>
      </View>
    )
  }
}
export default compose(
  graphql(waitingPayments_QUERY),
  graphql(paidUsers_QUERY, {
    name: 'paidUsers'
  }),
  graphql(MAKE_PAID_MUTATION),
  graphql(DELETE_RECORD_MUTATION, {
    name: 'deleteRecord'
  })
)(PaymentScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  cardContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  card: {
    height: height(23),
    width: width(35),
    backgroundColor: 'white',
    alignItems: 'center',
    marginLeft: 10,
    borderRadius: 5,
    marginBottom: 10
  },
  rightCard: {
    height: height(23),
    width: width(57),
    backgroundColor: 'white',
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    marginLeft: 10,
    borderRadius: 5,
    marginBottom: 10
  },
  heading: {
    fontWeight: '700',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginTop: 5
  },
  heading2: {
    fontWeight: '900',
    fontSize: 17,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginBottom: 10
  },
  subtitle: {
    fontWeight: '300',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginTop: 2
  },
  time: {
    fontWeight: '300',
    fontSize: 12,
    color: '#131110',
    fontFamily: 'Helvetica Neue',
    marginTop: 2
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f1ef08'
  },
  button: {
    backgroundColor: '#79ff87',
    borderRadius: 7,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20
  },
  redButton: {
    backgroundColor: '#e02606',
    borderRadius: 7,
    marginTop: 10,
    marginRight: 7,
    marginLeft: 7,
    marginBottom: 10
  },
  buttonText: {
    fontWeight: '500',
    fontSize: 12,
    color: '#142512',
    fontFamily: 'Helvetica Neue'
  },
  redButtonText: {
    fontWeight: '500',
    fontSize: 12,
    color: 'white',
    fontFamily: 'Helvetica Neue'
  },

  viewContainer: {
    marginLeft: 10
  },
  redSubtitle: {
    fontWeight: '300',
    fontSize: 14,
    color: '#730d8c',
    fontFamily: 'Helvetica Neue',
    marginTop: 2
  }
})
