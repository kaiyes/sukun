import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  RefreshControl
} from 'react-native'
import { Avatar, Button } from 'react-native-elements'
import _ from 'lodash'
import moment from 'moment'

import get_All_Chats_QUERY from '../graphql/queries/getAllChats.query'

class ChatScreen extends Component {
  state = {
    text: '',
    refreshing: false,
    lastM: ''
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.props.data.refetch()
    this.setState({ refreshing: false })
  }

  render() {
    if (this.props.data.loading || this.state.refreshing) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      )
    }

    return (
      <View style={styles.container}>
        <View style={styles.scrollContainer}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            {this.props.data.getAllChats.map(
              (item, index) => {
                return (
                  <TouchableOpacity
                    style={styles.avatarContainer}
                    key={index}
                    onPress={() => {
                      this.props.navigation.navigate(
                        'Messages',
                        {
                          conversationId: item._id,
                          username: `${item.inviter.cv
                            .username +
                            ' & ' +
                            item.responder.cv.username}`
                        }
                      )
                    }}
                  >
                    <View style={styles.leftContainer}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.inviter.cv.avatar
                        }}
                        activeOpacity={0.5}
                      />
                      <Text style={styles.text}>
                        {item.inviter.cv.username}
                      </Text>
                    </View>

                    <View style={styles.endContainer}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.responder.cv.avatar
                        }}
                        activeOpacity={0.5}
                      />
                      <Text style={styles.text}>
                        {item.responder.cv.username}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )
              }
            )}
          </ScrollView>
        </View>
      </View>
    )
  }
}
export default graphql(get_All_Chats_QUERY)(ChatScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scrollContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  avatarContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    margin: 5,
    backgroundColor: '#c5e4f2',
    borderColor: 'red',
    borderRadius: 10,
    height: 80
  },
  leftContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  endContainer: {
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#d0dd41'
  },
  text: {
    fontWeight: '900',
    fontSize: 14,
    color: '#111d1f',
    fontFamily: 'Helvetica Neue'
  }
})
