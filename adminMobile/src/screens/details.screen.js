import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  Image,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
  Dimensions
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import {
  Button,
  Badge,
  Divider,
  Icon
} from 'react-native-elements'
import ImageCarousel from 'react-native-image-carousel'
import DropdownAlert from 'react-native-dropdownalert'
import ZoomImage from 'react-native-zoom-image'
import _ from 'lodash'

import Admin_ApprovalList_QUERY from '../graphql/queries/adminApproval.query'
import Admin_WaitingList_QUERY from '../graphql/queries/adminWaitingList.query'
import Admin_BanList_QUERY from '../graphql/queries/adminBanList.query'
import Get_One_Cv_QUERY from '../graphql/queries/getOneCv.query'
import User_Data_QUERY from '../graphql/queries/adminUserDetails.query'
import Admin_RejectList_QUERY from '../graphql/queries/adminRejectList.query'

import CREATE_CHATROOM_MUTATION from '../graphql/mutation/createChatRoom.mutation'
import ADMIN_APPROVE_MUTATION from '../graphql/mutation/adminApprove.mutation'
import ADMIN_BAN_MUTATION from '../graphql/mutation/adminBan.mutation'
import ADMIN_REJECT_MUTATION from '../graphql/mutation/adminReject.mutation'

class DetailsScreen extends Component {
  renderImage = () => {
    const {
      pic,
      currentLocation,
      avatar
    } = this.props.data.getOneCv

    return (
      <View style={styles.sliderContainer}>
        <ScrollView horizontal={true}>
          {pic.map((item, index) => {
            return (
              <ZoomImage
                key={index}
                source={{ uri: item }}
                imgStyle={{
                  width: 250,
                  height: 200
                }}
                duration={200}
                enableScaling={false}
              />
            )
          })}
        </ScrollView>
        <View style={styles.locationRow}>
          <Icon
            name="dot-single"
            type="entypo"
            color="#42f125"
          />
          <Text>{currentLocation}</Text>
        </View>
      </View>
    )
  }
  _onChatPress = async () => {
    const { data } = await this.props
      .createChatRoom({
        variables: {
          _id: this.props.navigation.state.params.id
        }
      })
      .catch(err => {
        console.log(`${err}`)
      })
    this.props.navigation.navigate('Messages', {
      username: this.props.navigation.state.params.username,
      conversationId: data.createChatRoom._id
    })
  }

  _makeApproved = async () => {
    await this.props
      .adminApprove({
        variables: {
          _id: this.props.navigation.state.params.id
        },
        refetchQueries: [
          { query: Admin_ApprovalList_QUERY },
          { query: Admin_WaitingList_QUERY },
          {
            query: User_Data_QUERY,
            variables: {
              _id: this.props.navigation.state.params.id
            }
          }
        ]
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  _reject = async () => {
    await this.props
      .adminReject({
        variables: {
          _id: this.props.navigation.state.params.id
        },
        refetchQueries: [
          { query: Admin_ApprovalList_QUERY },
          { query: Admin_WaitingList_QUERY },
          { query: Admin_RejectList_QUERY },
          {
            query: User_Data_QUERY,
            variables: {
              _id: this.props.navigation.state.params.id
            }
          }
        ]
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  _ban = async () => {
    await this.props
      .adminBan({
        variables: {
          _id: this.props.navigation.state.params.id
        },
        refetchQueries: [
          { query: Admin_ApprovalList_QUERY },
          { query: Admin_WaitingList_QUERY },
          { query: Admin_BanList_QUERY },
          {
            query: User_Data_QUERY,
            variables: {
              _id: this.props.navigation.state.params.id
            }
          }
        ]
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  renderIconContainer = () => {
    const {
      isBanned,
      isApproved,
      isRejected,
      hasPaid,
      hasCompletedCv
    } = this.props.userData.adminUserDetails

    return (
      <View style={styles.iconsContainer}>
        <TouchableOpacity
          style={styles.icon}
          onPress={this._onChatPress}
        >
          <Image
            style={{ width: 73, height: 70 }}
            source={require('../../assets/icons/Chat.png')}
          />
        </TouchableOpacity>
        {isApproved ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._makeApproved}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../assets/icons/unApprove.png')}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._makeApproved}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../assets/icons/approved.png')}
            />
          </TouchableOpacity>
        )}
        {isBanned ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._ban}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../assets/icons/unBan.png')}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._ban}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../assets/icons/skull.png')}
            />
          </TouchableOpacity>
        )}
        {isRejected ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._reject}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../assets/icons/unRejectIcon.png')}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.icon}
            onPress={this._reject}
          >
            <Image
              style={{ width: 73, height: 70 }}
              source={require('../../assets/icons/rejectIcon.png')}
            />
          </TouchableOpacity>
        )}
      </View>
    )
  }

  render() {
    if (
      this.props.data.loading ||
      this.props.userData.loading
    ) {
      return (
        <ActivityIndicator
          style={styles.loader}
          size="large"
        />
      )
    }

    const {
      age,
      pic,
      profession,
      education,
      homeDistrict,
      currentLocation,
      beard,
      height,
      hifzLevel,
      aboutMe,
      prayer,
      gender,
      practisingSince,
      avatar,
      photoApproved
    } = this.props.data.getOneCv

    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          {this.renderImage()}

          {this.renderIconContainer()}

          <View style={styles.badgeContainer}>
            <View style={styles.badgeRow}>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
              >
                <Text style={{ color: 'white' }}>
                  {age}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {profession}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {education}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {homeDistrict}
                </Text>
              </Badge>
            </View>

            <View style={styles.badgeRow}>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {beard}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {practisingSince}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {height}
                </Text>
              </Badge>
            </View>
            <View style={styles.badgeRow}>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {hifzLevel}
                </Text>
              </Badge>
              <Badge
                containerStyle={{
                  backgroundColor: '#5D4A8F'
                }}
                wrapperStyle={{
                  marginLeft: 5
                }}
              >
                <Text style={{ color: 'white' }}>
                  {prayer}
                </Text>
              </Badge>
            </View>
            <Text style={styles.heading}>About Me</Text>

            <Text style={styles.aboutMe}>{aboutMe}</Text>
          </View>
        </ScrollView>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    )
  }
}

export default compose(
  graphql(User_Data_QUERY, {
    name: 'userData',
    options: ownProps => {
      return {
        variables: {
          _id: ownProps.navigation.state.params.id
        }
      }
    }
  }),
  graphql(Get_One_Cv_QUERY, {
    options: ownProps => {
      return {
        variables: {
          _id: ownProps.navigation.state.params.id
        }
      }
    }
  }),
  graphql(CREATE_CHATROOM_MUTATION, {
    name: 'createChatRoom'
  }),
  graphql(ADMIN_APPROVE_MUTATION, {
    name: 'adminApprove'
  }),
  graphql(ADMIN_BAN_MUTATION, {
    name: 'adminBan'
  }),
  graphql(ADMIN_REJECT_MUTATION, {
    name: 'adminReject'
  })
)(DetailsScreen)

// ************************************************
//                     Styles                     *
// ************************************************
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  scrollView: {
    flex: 1,
    backgroundColor: 'white'
  },
  photoContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  sliderContainer: {
    flexDirection: 'column',
    justifyContent: 'center'
    // alignItems: 'stretch',
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#356490'
  },
  text: {
    color: 'black'
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20
  },
  iconsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  badgeContainer: {
    marginTop: 5,
    marginBottom: 10
  },
  badgeRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 5
  },
  aboutMe: {
    color: 'black',
    fontWeight: '300',
    fontSize: 14,
    fontFamily: 'Helvetica Neue',
    marginTop: 3,
    marginLeft: 5,
    marginLeft: 15,
    marginRight: 15
  },
  heading: {
    color: 'black',
    fontWeight: '500',
    fontSize: 14,
    fontFamily: 'Helvetica Neue',
    marginTop: 15,
    marginLeft: 5,
    marginLeft: 15,
    marginRight: 15
  },
  locationRow: {
    flexDirection: 'row'
  }
})
