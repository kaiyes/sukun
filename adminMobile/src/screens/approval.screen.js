import React, { Component } from 'react'
import {
  ActivityIndicator,
  FlatList,
  View,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import _ from 'lodash'
import {
  Card,
  ListItem,
  Button,
  Divider,
  Avatar,
  Badge
} from 'react-native-elements'
import ScrollableTabView from 'react-native-scrollable-tab-view'

import Admin_ApprovalList_QUERY from '../graphql/queries/adminApproval.query'
import Admin_BanList_QUERY from '../graphql/queries/adminBanList.query'
import Admin_WaitingList_QUERY from '../graphql/queries/adminWaitingList.query'
import ADMIN_REJECTEDLIST_QUERY from '../graphql/queries/adminRejectList.query'

class ApprovalScreen extends Component {
  render() {
    const {
      data,
      banList,
      waitingList,
      rejectList
    } = this.props
    if (
      data.loading ||
      banList.loading ||
      waitingList.loading ||
      rejectList.loading
    ) {
      return (
        <ActivityIndicator
          size="large"
          style={styles.loader}
          size="large"
        />
      )
    }

    return (
      <View style={styles.container}>
        <ScrollableTabView>
          <FlatList
            tabLabel="waiting"
            data={waitingList.adminWaitingList}
            keyExtractor={item => item._id}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate(
                    'Details',
                    {
                      username: `${item.cv.username}`,
                      id: item._id
                    }
                  )
                }
              >
                <View style={styles.card}>
                  <Avatar
                    medium
                    rounded
                    source={{
                      uri: item.cv.pic[0]
                    }}
                    activeOpacity={0.5}
                    containerStyle={{ marginLeft: 10 }}
                  />
                  <View style={styles.textContainer}>
                    <Text style={styles.username}>
                      {item.cv.username}
                    </Text>
                    <Text style={{ color: '#696456' }}>
                      {item.cv.profession}
                    </Text>
                    <Text style={{ color: '#696456' }}>
                      {item.cv.age}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />

          <FlatList
            tabLabel="approved"
            data={data.adminApprovalList}
            keyExtractor={item => item._id}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate(
                    'Details',
                    {
                      username: `${item.cv.username}`,
                      id: item._id
                    }
                  )
                }
              >
                <View style={styles.card}>
                  <Avatar
                    medium
                    rounded
                    source={{
                      uri: item.cv.pic[0]
                    }}
                    activeOpacity={0.5}
                    containerStyle={{ marginLeft: 10 }}
                  />
                  <View style={styles.textContainer}>
                    <Text style={styles.username}>
                      {item.cv.username}
                    </Text>
                    <Text style={{ color: '#696456' }}>
                      {item.cv.profession}
                    </Text>
                    <Text style={{ color: '#696456' }}>
                      {item.cv.age}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />

          <FlatList
            tabLabel="Banned"
            data={banList.adminBanList}
            keyExtractor={item => item._id}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate(
                    'Details',
                    {
                      username: `${item.cv.username}`,
                      id: item._id
                    }
                  )
                }
              >
                <View style={styles.card}>
                  <Avatar
                    medium
                    rounded
                    source={{
                      uri: item.cv.pic[0]
                    }}
                    activeOpacity={0.5}
                    containerStyle={{ marginLeft: 10 }}
                  />
                  <View style={styles.textContainer}>
                    <Text style={styles.username}>
                      {item.cv.username}
                    </Text>
                    <Text style={{ color: '#696456' }}>
                      {item.cv.profession}
                    </Text>
                    <Text style={{ color: '#696456' }}>
                      {item.cv.age}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />

          <FlatList
            tabLabel="Rejected"
            data={rejectList.adminRejectList}
            keyExtractor={item => item._id}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate(
                    'Details',
                    {
                      username: `${item.cv.username}`,
                      id: item._id
                    }
                  )
                }
              >
                <View style={styles.card}>
                  <Avatar
                    medium
                    rounded
                    source={{
                      uri: item.cv.pic[0]
                    }}
                    activeOpacity={0.5}
                    containerStyle={{ marginLeft: 10 }}
                  />
                  <View style={styles.textContainer}>
                    <Text style={styles.username}>
                      {item.cv.username}
                    </Text>
                    <Text style={{ color: '#696456' }}>
                      {item.cv.profession}
                    </Text>
                    <Text style={{ color: '#696456' }}>
                      {item.cv.age}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </ScrollableTabView>
      </View>
    )
  }
}
export default compose(
  graphql(Admin_ApprovalList_QUERY),
  graphql(Admin_BanList_QUERY, {
    name: 'banList'
  }),
  graphql(Admin_WaitingList_QUERY, {
    name: 'waitingList'
  }),
  graphql(ADMIN_REJECTEDLIST_QUERY, {
    name: 'rejectList'
  })
)(ApprovalScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f4d6f6'
  },
  card: {
    // backgroundColor: '#E3D5FC',
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 5,
    marginTop: 5
  },
  textContainer: {
    margin: 20
  },
  username: {
    fontWeight: '900',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue'
  }
})
