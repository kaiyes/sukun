import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  ScrollView,
  FlatList,
  Dimensions
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import {
  Avatar,
  Button,
  Divider
} from 'react-native-elements'
import { connect } from 'react-redux'
import _ from 'lodash'

import getMessages_QUERY from '../graphql/queries/getMessages.query'
import sendMessage_MUTATION from '../graphql/mutation/sendMessage.mutation'
import makeChatSeen_MUTATION from '../graphql/mutation/makeChatNotiSeen.mutation'
import getMsgNoti_QUERY from '../graphql/queries/getMessageNotifications.query'

class MessageScreen extends Component {
  state = {
    text: ''
  }

  async componentWillMount() {
    this.props.data.startPolling(5000)
    await this.props
      .makeChatSeen({
        variables: {
          conversationId: this.props.navigation.state.params
            .conversationId
        },
        refetchQueries: [
          {
            query: getMsgNoti_QUERY
          }
        ]
      })
      .catch(err => {
        console.log(`${err}`)
      })
  }

  componentWillUnMount() {
    this.props.data.stopPolling()
    console.log('stopped')
  }
  _onSendMessage = async () => {
    await this.props
      .mutate({
        variables: {
          conversationId: this.props.navigation.state.params
            .conversationId,
          body: this.state.text
        },
        refetchQueries: [
          {
            query: getMessages_QUERY,
            variables: {
              conversationId: this.props.navigation.state
                .params.conversationId
            }
          }
        ]
      })
      .then(this.setState({ text: '' }))
      .catch(err => {
        console.log(`${err}`)
      })
  }

  render() {
    if (this.props.data.loading) {
      return (
        <View>
          <ActivityIndicator size="large" />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <View style={{ marginTop: 20 }} />
        <ScrollView
          style={styles.container}
          ref={ref => (this.scrollView = ref)}
          onContentSizeChange={(
            contentWidth,
            contentHeight
          ) =>
            this.scrollView.scrollToEnd({ animated: true })
          }
        >
          <FlatList
            contentContainerStyle={{ alignSelf: 'stretch' }}
            data={this.props.data.getMessages}
            keyExtractor={item => item._id}
            renderItem={({ item }) => (
              <View style={styles.card}>
                <Avatar
                  medium
                  rounded
                  onPress={() =>
                    this.props.navigation.navigate(
                      'Details',
                      {
                        username: `${
                          item.sender.cv.username
                        }`,
                        id: item.sender._id
                      }
                    )
                  }
                  activeOpacity={0.5}
                  source={{ uri: item.sender.cv.pic[0] }}
                />
                <View style={styles.chatTextContainer}>
                  <Text style={styles.username}>
                    {item.sender.cv.username}
                  </Text>
                  <Text style={styles.chatBody}>
                    {item.body}
                  </Text>
                </View>
              </View>
            )}
          />
        </ScrollView>
        <TextInput
          style={styles.form}
          placeholder="write here"
          value={this.state.text}
          underlineColorAndroid="transparent"
          onChangeText={text => this.setState({ text })}
          onSubmitEditing={this._onSendMessage}
        />
      </View>
    )
  }
}

export default compose(
  graphql(getMessages_QUERY, {
    options: ownProps => {
      return {
        variables: {
          conversationId:
            ownProps.navigation.state.params.conversationId
        }
      }
    }
  }),
  graphql(sendMessage_MUTATION),
  graphql(makeChatSeen_MUTATION, { name: 'makeChatSeen' })
)(MessageScreen)

// ************************************************
//                     Styles                     *
// ************************************************

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  form: {
    height: 50,
    borderColor: '#959595',
    borderWidth: 3,
    margin: 10,
    textAlign: 'left'
  },
  card: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 10,
    marginTop: 10
  },
  chatTextContainer: {
    marginBottom: 5,
    marginLeft: 15
  },
  chatBody: {
    fontWeight: '400',
    fontSize: 16,
    fontFamily: 'Avenir-Medium',
    marginBottom: 2
  },
  username: {
    fontWeight: '900',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginBottom: 2
  }
})
