import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
  RefreshControl
} from 'react-native'
import { graphql, compose } from 'react-apollo'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import {
  Card,
  ListItem,
  Button,
  Divider,
  Avatar,
  Badge
} from 'react-native-elements'
import { width, height } from 'react-native-dimension'
import DropdownAlert from 'react-native-dropdownalert'

import teams_Awaiting_QUERY from '../graphql/queries/teamsAwaitingApporoval.query'
import ADMIN_APPROVE_MUTATION from '../graphql/mutation/adminApprove.mutation'
import TEAM_APPROVAL_MUTATION from '../graphql/mutation/teamApproval.mutation'
import approved_Teams_QUERY from '../graphql/queries/approvedTeams.query'
import unfinished_Teams_QUERY from '../graphql/queries/unfinishedTeams.query'
// import allMatches_Query from '../graphql/queries/allMatches.query'

class MatchScreen extends Component {
  state = {
    refreshing: false
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.props.data.refetch()
    await this.props.approvedTeams.refetch()
    await this.props.unfinishedTeams.refetch()
    this.setState({ refreshing: false })
  }

  render() {
    const { data } = this.props

    if (data.loading) {
      return (
        <ActivityIndicator
          size="large"
          style={styles.loader}
          size="large"
        />
      )
    }
    return (
      <View style={styles.rootContainer}>
        <ScrollableTabView>
          <ScrollView
            tabLabel="Waiting"
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            <View style={{ paddingTop: 10 }} />
            {this.props.data.teamsAwaitingApproval.map(
              (item, index) => {
                return (
                  <View
                    key={index}
                    style={styles.cardContainer}
                  >
                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.brothersAvatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.brothersUsername}
                      </Text>
                      <Button
                        buttonStyle={styles.button}
                        title="Approve"
                        titleStyle={styles.buttonText}
                        onPress={() => {
                          this.props
                            .mutate({
                              variables: {
                                _id: item.brothersId
                              }
                            })
                            .then(({ data }) => {
                              this.dropdown.alertWithType(
                                'success',
                                'Success',
                                `Approved : ${
                                  data.adminApprove
                                    .isApproved
                                } "\n" hasCompletedCv: ${
                                  data.adminApprove
                                    .hasCompletedCv
                                } `
                              )
                            })
                            .catch(err => {
                              console.log(`${err}`)
                            })
                        }}
                      />
                    </View>

                    <View>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F',
                          marginBottom: 10
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          team {item.team}
                        </Text>
                      </Badge>
                      <Button
                        buttonStyle={styles.redButton}
                        title="Approve"
                        titleStyle={styles.redButtonText}
                        onPress={async () => {
                          await this.props
                            .approveTeam({
                              variables: {
                                _id: item._id
                              },
                              refetchQueries: [
                                {
                                  query: teams_Awaiting_QUERY
                                },
                                {
                                  query: approved_Teams_QUERY
                                }
                              ]
                            })
                            .catch(err => {
                              console.log(`${err}`)
                            })
                        }}
                      />
                    </View>

                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.sistersAvatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.sistersUsername}
                      </Text>
                      <Button
                        buttonStyle={styles.button}
                        title="Approve"
                        titleStyle={styles.buttonText}
                        onPress={() => {
                          this.props
                            .mutate({
                              variables: {
                                _id: item.sistersId
                              }
                            })
                            .then(({ data }) => {
                              console.log(data)
                              this.dropdown.alertWithType(
                                'success',
                                'Success',
                                `Approved : ${
                                  data.adminApprove
                                    .isApproved
                                } "\n" hasCompletedCv: ${
                                  data.adminApprove
                                    .hasCompletedCv
                                } `
                              )
                            })
                            .catch(err => {
                              console.log(`${err}`)
                            })
                        }}
                      />
                    </View>
                  </View>
                )
              }
            )}
          </ScrollView>

          <ScrollView
            tabLabel="Approved"
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            <View style={{ paddingTop: 10 }} />
            {this.props.approvedTeams.approvedTeams.map(
              (item, index) => {
                return (
                  <View
                    key={index}
                    style={styles.cardContainer}
                  >
                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.brothersAvatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.brothersUsername}
                      </Text>
                      <Button
                        buttonStyle={styles.button}
                        title="Approve"
                        titleStyle={styles.buttonText}
                        onPress={() => {
                          this.props
                            .mutate({
                              variables: {
                                _id: item.brothersId
                              }
                            })
                            .then(({ data }) => {
                              console.log(data)
                              this.dropdown.alertWithType(
                                'success',
                                'Success',
                                `Approved : ${
                                  data.adminApprove
                                    .isApproved
                                } "\n" hasCompletedCv: ${
                                  data.adminApprove
                                    .hasCompletedCv
                                }`
                              )
                            })
                            .catch(err => {
                              console.log(`${err}`)
                            })
                        }}
                      />
                    </View>

                    <View>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F',
                          marginBottom: 10
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          team {item.team}
                        </Text>
                      </Badge>
                      <Button
                        buttonStyle={styles.redButton}
                        title="Un-Approve"
                        titleStyle={styles.redButtonText}
                        onPress={() => {
                          this.props
                            .approveTeam({
                              variables: {
                                _id: item._id
                              },
                              refetchQueries: [
                                {
                                  query: teams_Awaiting_QUERY
                                },
                                {
                                  query: approved_Teams_QUERY
                                }
                              ]
                            })
                            .catch(err => {
                              console.log(`${err}`)
                            })
                        }}
                      />
                    </View>

                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.sistersAvatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.sistersUsername}
                      </Text>
                      <Button
                        buttonStyle={styles.button}
                        title="Approve"
                        titleStyle={styles.buttonText}
                        onPress={() => {
                          this.props
                            .mutate({
                              variables: {
                                _id: item.sistersId
                              }
                            })
                            .then(({ data }) => {
                              console.log(data)
                              this.dropdown.alertWithType(
                                'success',
                                'Success',
                                `Approved : ${
                                  data.adminApprove
                                    .isApproved
                                } "\n" hasCompletedCv: ${
                                  data.adminApprove
                                    .hasCompletedCv
                                } `
                              )
                            })
                            .catch(err => {
                              console.log(`${err}`)
                            })
                        }}
                      />
                    </View>
                  </View>
                )
              }
            )}
          </ScrollView>

          <ScrollView
            tabLabel="Half"
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            <View style={{ paddingTop: 10 }} />
            {this.props.unfinishedTeams.getUnfinishedTeamsForAdmin.map(
              (item, index) => {
                return (
                  <View
                    key={index}
                    style={styles.cardContainer}
                  >
                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.brothersAvatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.brothersUsername}
                      </Text>
                    </View>

                    <View>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F',
                          marginBottom: 10
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          team {item.team}
                        </Text>
                      </Badge>
                    </View>

                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.sistersAvatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.sistersUsername}
                      </Text>
                    </View>
                  </View>
                )
              }
            )}
          </ScrollView>

          {/* <ScrollView
            tabLabel="Matches"
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            <View style={{ paddingTop: 10 }} />
            {this.props.allMatches.getAllMatches.map(
              (item, index) => {
                return (
                  <View
                    key={index}
                    style={styles.cardContainer}
                  >
                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.groom.cv.avatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.groom.cv.username}
                      </Text>
                    </View>

                    <View>
                      <Badge
                        containerStyle={{
                          backgroundColor: '#5D4A8F',
                          marginBottom: 10
                        }}
                      >
                        <Text style={{ color: 'white' }}>
                          team {item.team}
                        </Text>
                      </Badge>
                    </View>

                    <View style={styles.card}>
                      <Avatar
                        medium
                        rounded
                        source={{
                          uri: item.groom.cv.avatar
                        }}
                        activeOpacity={0.5}
                        containerStyle={{ marginTop: 10 }}
                      />

                      <Text style={styles.heading}>
                        {item.groom.cv.username}
                      </Text>
                    </View>
                  </View>
                )
              }
            )}
          </ScrollView> */}
        </ScrollableTabView>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    )
  }
}

export default compose(
  graphql(teams_Awaiting_QUERY),
  graphql(ADMIN_APPROVE_MUTATION),
  graphql(TEAM_APPROVAL_MUTATION, { name: 'approveTeam' }),
  graphql(approved_Teams_QUERY, { name: 'approvedTeams' }),
  // graphql(allMatches_Query, { name: 'allMatches' }),
  graphql(unfinished_Teams_QUERY, {
    name: 'unfinishedTeams'
  })
)(MatchScreen)

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column'
  },
  cardContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  card: {
    height: height(25),
    width: width(40),
    backgroundColor: 'yellow',
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 10
  },
  rightCard: {
    height: height(23),
    width: width(57),
    backgroundColor: 'white',
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    marginLeft: 10,
    borderRadius: 5,
    marginBottom: 10
  },
  heading: {
    fontWeight: '700',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginTop: 5
  },
  heading2: {
    fontWeight: '900',
    fontSize: 17,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginBottom: 10
  },
  subtitle: {
    fontWeight: '300',
    fontSize: 14,
    color: '#4990E2',
    fontFamily: 'Helvetica Neue',
    marginTop: 2
  },
  time: {
    fontWeight: '300',
    fontSize: 12,
    color: '#131110',
    fontFamily: 'Helvetica Neue',
    marginTop: 2
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f1ef08'
  },
  button: {
    backgroundColor: '#79ff87',
    borderRadius: 7,
    marginTop: 10
  },
  redButton: {
    backgroundColor: '#c42fcc',
    borderRadius: 7
  },
  buttonText: {
    fontWeight: '500',
    fontSize: 12,
    color: '#142512',
    fontFamily: 'Helvetica Neue'
  },
  redButtonText: {
    fontWeight: '500',
    fontSize: 12,
    color: 'white',
    fontFamily: 'Helvetica Neue'
  },
  viewContainer: {
    marginLeft: 10
  },
  redSubtitle: {
    fontWeight: '300',
    fontSize: 14,
    color: '#730d8c',
    fontFamily: 'Helvetica Neue',
    marginTop: 2
  }
})
