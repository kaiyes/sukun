import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'

class DashBoard extends Component {
  render() {
    return (
      <View style={styles.rootContainer}>
        <Text>DashBoard</Text>
      </View>
    )
  }
}
export default DashBoard

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column'
  }
})
