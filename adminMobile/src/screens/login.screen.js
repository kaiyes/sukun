import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  AsyncStorage,
  ActivityIndicator,
  KeyboardAvoidingView,
  Keyboard
} from 'react-native'
import { graphql, withApollo, compose } from 'react-apollo'
import { connect } from 'react-redux'
import DropdownAlert from 'react-native-dropdownalert'
import {
  width,
  height,
  totalSize
} from 'react-native-dimension'

import LOGIN_MUTATION from '../graphql/mutation/login.mutaion'

class LogInScreen extends Component {
  state = {
    email: '',
    password: '',
    loading: false
  }

  _onChangeText = (text, type) =>
    this.setState({
      [type]: text
    })

  _onLoginPress = async () => {
    this.setState({ loading: true })
    const { email, password } = this.state

    await this.props
      .mutate({
        variables: { email, password }
      })
      .then(({ data }) => {
        AsyncStorage.setItem(
          '@login:token',
          data.logIn.token
        )
      })
      .catch(e => {
        this.setState({ loading: false })
        this.dropdown.alertWithType(
          'error',
          'Error',
          `${e.graphQLErrors[0].message}`
        )
      })
    this.setState({ loading: false })
  }

  render() {
    if (this.state.loading) {
      return <ActivityIndicator size="large" />
    }

    return (
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <TextInput
            style={styles.form}
            placeholder="Email"
            value={this.state.email}
            keyBoardType="email-address"
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            onChangeText={text =>
              this._onChangeText(text, 'email')
            }
          />
          <TextInput
            style={styles.form}
            placeholder="Password"
            value={this.state.password}
            secureTextEntry
            underlineColorAndroid="transparent"
            onChangeText={text =>
              this._onChangeText(text, 'password')
            }
          />
        </View>

        <TouchableOpacity onPress={this._onLoginPress}>
          <View style={styles.signUpContainer}>
            <Text style={styles.signUpText}>LogIn</Text>
          </View>
        </TouchableOpacity>

        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={2000}
        />
      </View>
    )
  }
}

export default compose(graphql(LOGIN_MUTATION))(LogInScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column'
  },
  formContainer: {
    flex: 2,
    justifyContent: 'center',
    paddingHorizontal: 20
  },
  closeButton: {
    position: 'absolute',
    top: 30,
    left: 10
  },
  signUpContainer: {
    justifyContent: 'center',
    backgroundColor: '#2980b9'
  },
  form: {
    height: 50,
    borderColor: '#2980b9',
    borderWidth: 1,
    borderRadius: 22,
    margin: 10,
    textAlign: 'center',
    color: 'black'
  },
  signUpText: {
    color: 'black',
    fontFamily: 'Helvetica Neue',
    fontSize: 22,
    padding: 16,
    textAlign: 'center',
    fontWeight: '600'
  }
})
