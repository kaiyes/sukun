import { createStore, applyMiddleware } from 'redux'
import { AsyncStorage, Platform } from 'react-native'
import { composeWithDevTools } from 'redux-devtools-extension'
import ApolloClient, {
  createNetworkInterface
} from 'apollo-client'
import thunk from 'redux-thunk'
import reducers from './redux/reducer/index'

const networkInterface = createNetworkInterface({
  uri: 'https://sukun.herokuapp.com/graphql'
})

networkInterface.use([
  {
    async applyMiddleware(req, next) {
      if (!req.options.headers) {
        req.options.headers = {}
      }
      try {
        const token = await AsyncStorage.getItem(
          '@login:token'
        )
        if (token !== null) {
          req.options.headers.authorization = token || null
        }
      } catch (error) {
        throw error
      }
      next()
    }
  }
])

export const client = new ApolloClient({
  networkInterface
})

const middlewares = [client.middleware(), thunk]

export const store = createStore(
  reducers(client),
  undefined,
  composeWithDevTools(applyMiddleware(...middlewares))
)
