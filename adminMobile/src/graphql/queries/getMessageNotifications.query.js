import { gql } from 'react-apollo'

export default gql`
  query {
    getMessageNotifications {
      _id
      message
      conversationId
      createdAt
      sender {
        cv {
          username
        }
      }
    }
  }
`
