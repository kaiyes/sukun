import { gql } from 'react-apollo'

export default gql`
  query {
    teamsAwaitingApproval {
      _id
      team
      brothersId
      brothersPic
      brothersAvatar
      brothersUsername

      sistersId
      sistersPic
      sistersAvatar
      sistersUsername
    }
  }
`
