import { gql } from 'react-apollo'

export default gql`
  query adminUserDetails($_id: ID!) {
    adminUserDetails(_id: $_id) {
      hasPaid
      isBanned
      isApproved
      isRejected
      hasCompletedCv
    }
  }
`
