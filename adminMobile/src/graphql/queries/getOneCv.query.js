import { gql } from 'react-apollo'

export default gql`
  query getOneCv($_id: ID!) {
    getOneCv(_id: $_id) {
      username
      age
      profession
      homeDistrict
      currentLocation
      practisingSince
      beard
      prayer
      education
      aboutMe
      height
      hifzLevel
      pic
      gender
      avatar
      photoApproved
    }
  }
`
