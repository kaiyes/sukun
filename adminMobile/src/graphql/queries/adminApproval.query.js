import { gql } from 'react-apollo'

export default gql`
  query {
    adminApprovalList {
      _id
      cv {
        username
        profession
        age
        beard
        gender
        photoApproved
        pic
        avatar
        avatar
      }
    }
  }
`
