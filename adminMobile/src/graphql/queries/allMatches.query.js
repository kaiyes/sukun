import { gql } from 'react-apollo'

export default gql`
  query {
    getAllMatches {
      bride {
        cv {
          username
          avatar
          age
          profession
          practisingSince
        }
      }
      groom {
        cv {
          username
          avatar
          age
          profession
          practisingSince
        }
      }
    }
  }
`
