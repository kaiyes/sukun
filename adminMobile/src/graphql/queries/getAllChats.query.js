import { gql } from 'react-apollo'

export default gql`
  {
    getAllChats {
      _id
      inviter {
        _id
        gender
        cv {
          username
          avatar
        }
      }
      responder {
        _id
        gender
        cv {
          username
          avatar
        }
      }
    }
  }
`
