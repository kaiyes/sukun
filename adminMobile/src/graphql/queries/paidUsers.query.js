import { gql } from 'react-apollo'

export default gql`
  {
    paidUsers {
      _id
      amount
      bikash
      createdAt
      approved
      updatedAt
      user {
        _id
        isApproved
        isBanned
        hasCompletedCv
        isRejected
        hasPaid
        cv {
          username
          avatar
          pic
        }
      }
    }
  }
`
