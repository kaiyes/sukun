import { gql } from 'react-apollo'

export default gql`
  {
    waitingPayments {
      _id
      amount
      bikash
      createdAt
      approved
      user {
        _id
        isApproved
        isBanned
        hasCompletedCv
        isRejected
        hasPaid
        cv {
          username
          avatar
          pic
        }
      }
    }
  }
`
