import { gql } from 'react-apollo'

export default gql`
  {
    getMyChats {
      _id
      inviter {
        _id
        cv {
          username
          pic
          gender
          photoApproved
          avatar
        }
      }
      responder {
        _id
        cv {
          username
          pic
          gender
          photoApproved
          avatar
        }
      }
    }
  }
`
