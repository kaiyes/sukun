import { gql } from 'react-apollo'

export default gql`
  mutation adminBan($_id: ID!) {
    adminBan(_id: $_id) {
      isBanned
    }
  }
`
