import { gql } from 'react-apollo'

export default gql`
  mutation makePaid($_id: ID!) {
    makePaid(_id: $_id) {
      approved
    }
  }
`
