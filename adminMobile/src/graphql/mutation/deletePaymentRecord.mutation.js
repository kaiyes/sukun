import { gql } from 'react-apollo'

export default gql`
  mutation deletePaymentRecord($_id: ID!) {
    deletePaymentRecord(_id: $_id) {
      _id
    }
  }
`
