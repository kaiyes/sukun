import { gql } from 'react-apollo'

export default gql`
  mutation insertMessage(
    $body: String!
    $conversationId: ID!
  ) {
    insertMessage(
      body: $body
      conversationId: $conversationId
    ) {
      body
    }
  }
`
