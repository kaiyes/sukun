import { gql } from 'react-apollo';

export default gql`
  mutation signUp($email: String!, $password: String!, $gender: String!) {
    signUp(email: $email, password: $password, gender: $gender) {
      token
    }
  }
`;
