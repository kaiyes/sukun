import { gql } from 'react-apollo'

export default gql`
  mutation makeChatNotiSeen($conversationId: ID!) {
    makeChatNotiSeen(conversationId: $conversationId) {
      seen
    }
  }
`
