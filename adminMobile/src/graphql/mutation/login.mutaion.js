import { gql } from 'react-apollo'

export default gql`
  mutation logIn($email: String!, $password: String!) {
    logIn(email: $email, password: $password) {
      token
      user {
        hasCompletedCv
        hasPaid
        isBanned
        isApproved
        isRejected
        gender
        _id
      }
    }
  }
`
