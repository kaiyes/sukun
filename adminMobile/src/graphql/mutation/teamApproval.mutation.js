import { gql } from 'react-apollo'

export default gql`
  mutation teamApproval($_id: String) {
    teamApproval(_id: $_id) {
      team
      approved
    }
  }
`
