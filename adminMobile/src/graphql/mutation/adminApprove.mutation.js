import { gql } from 'react-apollo'

export default gql`
  mutation adminApprove($_id: ID!) {
    adminApprove(_id: $_id) {
      isApproved
      hasCompletedCv
    }
  }
`
