import { gql } from 'react-apollo'

export default gql`
  mutation adminReject($_id: ID!) {
    adminReject(_id: $_id) {
      isRejected
    }
  }
`
