import React, { Component } from 'react'
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native'
import { ApolloProvider } from 'react-apollo'
import { client } from './store'

import AppNavigator from './navigation'
console.disableYellowBox = true

export default class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <AppNavigator />
      </ApolloProvider>
    )
  }
}
